/****************************************************/
/* Quelques primitives de dessin                    */
/* image_draw_func.h                                */
/*                                                  */
/* Ecrit par : Daniel Lacroix (all rights reserved) */
/*                                                  */
/****************************************************/

#ifndef __IMAGE_DRAW_FUNC_H__
#define __IMAGE_DRAW_FUNC_H__

#include "image.h"

/* les definitions des differentes utilisation du canal      */
/* alpha qui suivent NE DOIVENT PAS etre utilisees ailleurs  */
/* elle en interne a ne pas ecrirent 3 fonctions en fonction */
/* de l'utilisation de alpha mais une seul parametree.       */
typedef enum {
  ALPHA_REPLACE,
  ALPHA_KEEP,
  ALPHA_USE
} AlphaType;

/* dessine un point aux coordonnees (x,y) de couleur col  */
/* la transparence resultante dans pimage de celle de col */
void put_pix_alpha_replace(image *pimage, int32 x, int32 y, pix col);
/* dessine un point aux coordonnees (x,y) de couleur col     */
/* la transparence resultante dans pimage de celle de pimage */
void put_pix_alpha_keep   (image *pimage, int32 x, int32 y, pix col);
/* dessine un point aux coordonnees (x,y) de couleur col     */
/* la transparence resultante dans pimage de celle de pimage */
/* la transparence de col est utilise pour calculer le       */
/* nouveau point resultant de la superposition de col et du  */
/* point de pimage.                                          */
void put_pix_alpha_use    (image *pimage, int32 x, int32 y, pix col);

/* renvoi le point aux coordonnees (x,y) */
pix get_pix(image *pimage, int32 x, int32 y);

/* dessine une ligne entre (x1,y1) et (x2,y2) de couleur col */
/* utilisation en interne seulement, utilisez plutot les     */
/* fonctions qui suivent.                                    */
void line(image *pimage, int32 x1, int32 y1, int32 x2, int32 y2, pix col,
  AlphaType alpha_type);
/* dessine une ligne entre (x1,y1) et (x2,y2) de couleur col */
/* la transparence resultante dans pimage de celle de col    */
/* void line_alpha_replace(image *pimage, int32 x1, int32 y1,*/
/*   int32 x2, int32 y2, pix col);                           */
#define line_alpha_replace(pimage,x1,y1,x2,y2,col) \
 line(pimage,x1,y1,x2,y2,col,ALPHA_REPLACE)
/* dessine une ligne entre (x1,y1) et (x2,y2) de couleur col */
/* la transparence resultante dans pimage de celle de pimage */
/* void line_alpha_keep   (image *pimage, int32 x1, int32 y1,*/
/*   int32 x2, int32 y2, pix col);                           */
#define line_alpha_keep(pimage,x1,y1,x2,y2,col) \
 line(pimage,x1,y1,x2,y2,col,ALPHA_KEEP)
/* dessine une ligne entre (x1,y1) et (x2,y2) de couleur col */
/* la transparence resultante dans pimage de celle de pimage */
/* la transparence de col est utilise pour calculer le       */
/* nouveau point resultant de la superposition de col et du  */
/* point de pimage.                                          */
/* void line_alpha_use    (image *pimage, int32 x1, int32 y1,*/
/*   int32 x2, int32 y2, pix col);                           */
#define line_alpha_use(pimage,x1,y1,x2,y2,col) \
 line(pimage,x1,y1,x2,y2,col,ALPHA_USE)

/* dessine une ligne avec antialiasee entre */
/* (x1,y1) et (x2,y2) de couleur col        */
void line_aliased(image *pimage, int32 x1, int32 y1, int32 x2, int32 y2,
  pix col);

/* dessine une ligne horizontale entre (x1,y) et (x2,y) de couleur col */
void hline(image *pimage, int32 x1, int32 x2, int32 y, pix col,
  AlphaType alpha_type);
/* dessine une ligne horizontale entre (x1,y) et (x2,y) de couleur col */
/* la transparence resultante dans pimage de celle de col              */
/* void hline_alpha_replace(image *pimage, int32 x1, int32 x2, int32 y,*/
/* pix col);                                                           */
#define hline_alpha_replace(pimage,x1,x2,y,col) \
 hline(pimage,x1,x2,y,col,ALPHA_REPLACE)
/* dessine une ligne horizontale entre (x1,y) et (x2,y) de couleur col */
/* la transparence resultante dans pimage de celle de pimage           */
/* void hline_alpha_keep(image *pimage, int32 x1, int32 x2, int32 y,   */
/*   pix col);                                                         */
#define hline_alpha_keep(pimage,x1,x2,y,col) \
 hline(pimage,x1,x2,y,col,ALPHA_KEEP)
/* dessine une ligne horizontale entre (x1,y) et (x2,y) de couleur col */
/* la transparence resultante dans pimage de celle de pimage           */
/* la transparence de col est utilise pour calculer le                 */
/* nouveau point resultant de la superposition de col et du            */
/* point de pimage.                                                    */
/* void hline_alpha_use(image *pimage, int32 x1, int32 x2, int32 y,    */
/*   pix col);                                                         */
#define hline_alpha_use(pimage,x1,x2,y,col) \
 hline(pimage,x1,x2,y,col,ALPHA_USE)

/* dessine une ligne verticale entre (x,y1) et (x,y2) de couleur col */
void vline(image *pimage, int32 x, int32 y1, int32 y2, pix col,
  AlphaType alpha_type);
/* dessine une ligne verticale entre (x,y1) et (x,y2) de couleur col */
/* la transparence resultante dans pimage de celle de col            */
/* void vline_alpha_replace(image *pimage, int32 x, int32 y1,        */
/*   int32 y2, pix col);                                             */
#define vline_alpha_replace(pimage,x,y1,y2,col) \
 vline(pimage,x,y1,y2,col,ALPHA_REPLACE)
/* dessine une ligne verticale entre (x,y1) et (x,y2) de couleur col */
/* la transparence resultante dans pimage de celle de pimage         */
/* void vline_alpha_keep(image *pimage, int32 x, int32 y1, int32 y2, */
/*   pix col);                                                       */
#define vline_alpha_keep(pimage,x,y1,y2,col) \
 vline(pimage,x,y1,y2,col,ALPHA_KEEP)
/* dessine une ligne verticale entre (x,y1) et (x,y2) de couleur col */
/* la transparence resultante dans pimage de celle de pimage         */
/* la transparence de col est utilise pour calculer le               */
/* nouveau point resultant de la superposition de col et du          */
/* point de pimage.                                                  */
/* void vline_alpha_use(image *pimage, int32 x, int32 y1, int32 y2,  */
/*   pix col);                                                       */
#define vline_alpha_use(pimage,x,y1,y2,col) \
 vline(pimage,x,y1,y2,col,ALPHA_USE)

/* dessine un rectangle vide entre (x1,y1) et (x2,y2) de couleur col */
void rectangle(image *pimage, int32 x1, int32 y1, int32 x2, int32 y2,
  pix col, AlphaType alpha_type);
/* dessine un rectangle vide entre (x1,y1) et (x2,y2) de couleur col */
/* la transparence resultante dans pimage de celle de col            */
/* void rectangle_alpha_replace(image *pimage, int32 x1, int32 y1,   */
/*   int32 x2, int32 y2, pix col);                                   */
#define rectangle_alpha_replace(pimage,x1,y1,x2,y2,col) \
 rectangle(pimage,x1,y1,x2,y2,col,ALPHA_REPLACE)
/* dessine un rectangle vide entre (x1,y1) et (x2,y2) de couleur col */
/* la transparence resultante dans pimage de celle de pimage         */
/* void rectangle_alpha_keep(image *pimage, int32 x1, int32 y1,      */
/*   int32 x2, int32 y2, pix col);                                   */
#define rectangle_alpha_keep(pimage,x1,y1,x2,y2,col) \
 rectangle(pimage,x1,y1,x2,y2,col,ALPHA_KEEP)
/* dessine un rectangle vide entre (x1,y1) et (x2,y2) de couleur col */
/* la transparence resultante dans pimage de celle de pimage         */
/* la transparence de col est utilise pour calculer le               */
/* nouveau point resultant de la superposition de col et du          */
/* point de pimage.                                                  */
/* void rectangle_alpha_use(image *pimage, int32 x1, int32 y1,       */
/*   int32 x2, int32 y2, pix col);                                   */
#define rectangle_alpha_use(pimage,x1,y1,x2,y2,col) \
 rectangle(pimage,x1,y1,x2,y2,col,ALPHA_USE)

/* dessine un rectangle rempli */
void rectangle_fill(image *pimage, int32 x1, int32 y1, int32 x2, int32 y2,
  pix col, AlphaType alpha_type);
/* dessine un rectangle rempli                            */
/* la transparence resultante dans pimage de celle de col */
/* void rectangle_fill_alpha_replace(image *pimage,       */
/*   int32 x1, int32 y1, int32 x2, int32 y2, pix col);    */
#define rectangle_fill_alpha_replace(pimage,x1,y1,x2,y2,col) \
 rectangle_fill(pimage,x1,y1,x2,y2,col,ALPHA_REPLACE)
/* dessine un rectangle rempli                               */
/* la transparence resultante dans pimage de celle de pimage */
/* void rectangle_fill_alpha_keep(image *pimage,             */
/*   int32 x1, int32 y1, int32 x2, int32 y2, pix col);       */
#define rectangle_fill_alpha_keep(pimage,x1,y1,x2,y2,col) \
 rectangle_fill(pimage,x1,y1,x2,y2,col,ALPHA_KEEP)
/* dessine un rectangle rempli                               */
/* la transparence resultante dans pimage de celle de pimage */
/* la transparence de col est utilise pour calculer le       */
/* nouveau point resultant de la superposition de col et du  */
/* point de pimage.                                          */
/* void rectangle_fill_alpha_use(image *pimage,              */
/*   int32 x1, int32 y1, int32 x2, int32 y2, pix col);       */
#define rectangle_fill_alpha_use(pimage,x1,y1,x2,y2,col) \
 rectangle_fill(pimage,x1,y1,x2,y2,col,ALPHA_USE)

/* repaint le fond avec col. meme le canal alpha est remplace */
void clear_img(image *pimage,pix col);

/* dessine une ellipse de centre (cx,cy) et de rayon */
/* horizontal rx, vertical ry                        */
void ellipse(image *pimage, int32 cx, int32 cy, uint32 rx, uint32 ry,
  pix col, AlphaType alpha_type);
/* dessine une ellipse de centre (cx,cy) et de rayon horizontal  */
/* rx, vertical ry                                               */
/* la transparence resultante dans pimage de celle de col        */
/* void ellipse_alpha_replace(image *pimage, int32 cx, int32 cy, */
/*   uint32 rx, uint32 ry, pix col);                             */
#define ellipse_alpha_replace(pimage,cx,cy,rx,ry,col) \
 ellipse(pimage,cx,cy,rx,ry,col,ALPHA_REPLACE)
/* dessine une ellipse de centre (cx,cy) et de rayon         */
/* horizontal rx, vertical ry                                */
/* la transparence resultante dans pimage de celle de pimage */
/* void ellipse_alpha_keep(image *pimage, int32 cx, int32 cy,*/
/*   uint32 rx, uint32 ry, pix col);                         */
#define ellipse_alpha_keep(pimage,cx,cy,rx,ry,col) \
 ellipse(pimage,cx,cy,rx,ry,col,ALPHA_KEEP)
/* dessine une ellipse de centre (cx,cy) et de rayon         */
/* horizontal rx, vertical ry                                */
/* la transparence resultante dans pimage de celle de pimage */
/* la transparence de col est utilise pour calculer le       */
/* nouveau point resultant de la superposition de col et du  */
/* point de pimage.                                          */
/* void ellipse_alpha_use(image *pimage, int32 cx, int32 cy, */
/*   uint32 rx, uint32 ry, pix col);                         */
#define ellipse_alpha_use(pimage,cx,cy,rx,ry,col) \
 ellipse(pimage,cx,cy,rx,ry,col,ALPHA_USE)

/* Dessine une ellipse rempli de centre (cx,cy) et de */
/* rayon horizontal rx, vertical ry                   */
void ellipse_fill(image *pimage, int32 cx, int32 cy,
  uint32 rx, uint32 ry, pix col, AlphaType alpha_type);
/* Dessine une ellipse rempli de centre (cx,cy) et de     */
/* rayon horizontal rx, vertical ry                       */
/* la transparence resultante dans pimage de celle de col */
/* void ellipse_fill_alpha_replace(image *pimage,         */
/*   int32 cx, int32 cy, uint32 rx, uint32 ry, pix col);  */
#define ellipse_fill_alpha_replace(pimage,cx,cy,rx,ry,col) \
 ellipse_fill(pimage,cx,cy,rx,ry,col,ALPHA_REPLACE)
/* Dessine une ellipse rempli de centre (cx,cy) et de        */
/* rayon horizontal rx, vertical ry                          */
/* la transparence resultante dans pimage de celle de pimage */
/* void ellipse_fill_alpha_keep(image *pimage,               */
/*   int32 cx, int32 cy, uint32 rx, uint32 ry, pix col);     */
#define ellipse_fill_alpha_keep(pimage,cx,cy,rx,ry,col) \
 ellipse_fill(pimage,cx,cy,rx,ry,col,ALPHA_KEEP)
/* Dessine une ellipse rempli de centre (cx,cy) et de        */
/* rayon horizontal rx, vertical ry                          */
/* la transparence resultante dans pimage de celle de pimage */
/* la transparence de col est utilise pour calculer le       */
/* nouveau point resultant de la superposition de col et du  */
/* point de pimage.                                          */
/* void ellipse_fill_alpha_use(image *pimage, int32 cx,      */
/*   int32 cy, uint32 rx, uint32 ry, pix col);               */
#define ellipse_fill_alpha_use(pimage,cx,cy,rx,ry,col) \
 ellipse_fill(pimage,cx,cy,rx,ry,col,ALPHA_USE)

/* dessine une ligne horizontale entre (x1,y) de couleur col1    */
/* et (x2,y) de couleur col2 avec un degrade lineaire de couleur */
void hline_shade(image *pimage, int32 x1, int32 x2, int32 y,
  pix col1, pix col2, AlphaType alpha_type);
/* dessine une ligne horizontale entre (x1,y) de couleur col1    */
/* et (x2,y) de couleur col2 avec un degrade lineaire de couleur */
/* la transparence resultante dans pimage de celle du degrade    */
/* de col1 et col2                                               */
/* void hline_shade_alpha_replace(image *pimage,                 */
/*   int32 x1, int32 x2, int32 y, pix col1, pix col2);           */
#define hline_shade_alpha_replace(pimage,x1,x2,y,col1,col2) \
 hline_shade(pimage,x1,x2,y,col1,col2,ALPHA_REPLACE)
/* dessine une ligne horizontale entre (x1,y) de couleur col1    */
/* et (x2,y) de couleur col2 avec un degrade lineaire de couleur */
/* la transparence resultante dans pimage de celle de pimage     */
/* void hline_shade_alpha_keep(image *pimage, int32 x1, int32 x2,*/
/*   int32 y, pix col1, pix col2);                               */
#define hline_shade_alpha_keep(pimage,x1,x2,y,col1,col2) \
 hline_shade(pimage,x1,x2,y,col1,col2,ALPHA_KEEP)
/* dessine une ligne horizontale entre (x1,y) de couleur col1    */
/* et (x2,y) de couleur col2 avec un degrade lineaire de couleur */
/* la transparence resultante dans pimage de celle de pimage     */
/* le degrade de transparence de col1 et col2 est utilise pour   */
/* calculer le nouveau point resultant de la superposition de    */
/* col1 col2 et du point de pimage.                              */
/* void hline_shade_alpha_use(image *pimage,                     */
/*   int32 x1, int32 x2, int32 y, pix col1, pix col2);           */
#define hline_shade_alpha_use(pimage,x1,x2,y,col1,col2) \
 hline_shade(pimage,x1,x2,y,col1,col2,ALPHA_USE)

/* dessine une ligne verticale entre (x,y1) de couleur col1      */
/* et (x,y2) de couleur col2 avec un degrade lineaire de couleur */
void vline_shade(image *pimage, int32 x, int32 y1, int32 y2,
  pix col1, pix col2, AlphaType alpha_type);
/* dessine une ligne verticale entre (x,y1) de couleur col1      */
/* et (x,y2) de couleur col2 avec un degrade lineaire de couleur */
/* la transparence resultante dans pimage de celle du degrade de */
/* col1 vers col2                                                */
/* void vline_shade_alpha_replace(image *pimage, int32 x,        */
/*   int32 y1, int32 y2, pix col1, pix col2);                    */
#define vline_shade_alpha_replace(pimage,x,y1,y2,col1,col2) \
 vline_shade(pimage,x,y1,y2,col1,col2,ALPHA_REPLACE)
/* dessine une ligne verticale entre (x,y1) de couleur col1      */
/* et (x,y2) de couleur col2 avec un degrade lineaire de couleur */
/* la transparence resultante dans pimage de celle de pimage     */
/* void vline_shade_alpha_keep(image *pimage, int32 x, int32 y1, */
/*   int32 y2, pix col1, pix col2);                              */
#define vline_shade_alpha_keep(pimage,x,y1,y2,col1,col2) \
 vline_shade(pimage,x,y1,y2,col1,col2,ALPHA_KEEP)
/* dessine une ligne verticale entre (x,y1) de couleur col1      */
/* et (x,y2) de couleur col2 avec un degrade lineaire de couleur */
/* la transparence resultante dans pimage de celle de pimage     */
/* le degrade de transparence de col1 et col2 est utilise pour   */
/* calculer le nouveau point resultant de la superposition de    */
/* col1 col2 et du point de pimage.                              */
/* void vline_shade_alpha_use(image *pimage, int32 x,            */
/*   int32 y1, int32 y2, pix col1, pix col2);                    */
#define vline_shade_alpha_use(pimage,x,y1,y2,col1,col2) \
 vline_shade(pimage,x,y1,y2,col1,col2,ALPHA_USE)

/* Dessine un rectangle rempli entre les points (x1,y1) et (x2,y2) */
/* ce rectangle est rempli par un degrade lineaire de couleur      */
/*  definie comme sur le schema ci-dessous                         */
/*  col1 ---- col2                                                 */
/*   |          |                                                  */
/*  col3 ---- col4                                                 */
void rectangle_fill_shade(image *pimage,
  int32 x1, int32 y1, int32 x2, int32 y2,
  pix col1, pix col2, pix col3, pix col4, AlphaType alpha_type);
/* Dessine un rectangle rempli entre les points (x1,y1) et (x2,y2) */
/* ce rectangle est rempli par un degrade lineaire de couleur      */
/*  definie comme sur le schema ci-dessous                         */
/*  col1 ---- col2                                                 */
/*   |          |                                                  */
/*  col3 ---- col4                                                 */
/* la transparence resultante dans pimage de celle du degrade de   */
/* col1, col2, col3 et col4                                        */
/* void rectangle_fill_shade_alpha_replace(image *pimage,          */
/*   int32 x1, int32 y1, int32 x2, int32 y2,                       */
/*   pix col1, pix col2, pix col3, pix col4);                      */
#define rectangle_fill_shade_alpha_replace(pimage,x1,y1,x2,y2,\
col1,col2,col3,col4) \
 rectangle_fill_shade(pimage,x1,y1,x2,y2,col1,col2,col3,col4,ALPHA_REPLACE)
/* Dessine un rectangle rempli entre les points (x1,y1) et (x2,y2) */
/* ce rectangle est rempli par un degrade lineaire de couleur      */
/*  definie comme sur le schema ci-dessous                         */
/*  col1 ---- col2                                                 */
/*   |          |                                                  */
/*  col3 ---- col4                                                 */
/* la transparence resultante dans pimage de celle de pimage       */
/* void rectangle_fill_shade_alpha_keep(image *pimage,             */
/*   int32 x1, int32 y1, int32 x2, int32 y2,                       */
/*   pix col1, pix col2, pix col3, pix col4);                      */
#define rectangle_fill_shade_alpha_keep(pimage,x1,y1,x2,y2,\
col1,col2,col3,col4) \
 rectangle_fill_shade(pimage,x1,y1,x2,y2,col1,col2,col3,col4,ALPHA_KEEP)
/* Dessine un rectangle rempli entre les points (x1,y1) et (x2,y2) */
/* ce rectangle est rempli par un degrade lineaire de couleur      */
/*  definie comme sur le schema ci-dessous                         */
/*  col1 ---- col2                                                 */
/*   |          |                                                  */
/*  col3 ---- col4                                                 */
/* la transparence resultante dans pimage de celle de pimage       */
/* le degrade de transparence de col1, col2, col3 et col4 est      */
/* utilise pour calculer le nouveau point resultant de la          */
/* superposition du degrade et du point de pimage.                 */
/* void rectangle_fill_shade_alpha_use(image *pimage,              */
/*   int32 x1, int32 y1, int32 x2, int32 y2,                       */
/*   pix col1, pix col2, pix col3, pix col4);                      */
#define rectangle_fill_shade_alpha_use(pimage,x1,y1,x2,y2,\
col1,col2,col3,col4) \
 rectangle_fill_shade(pimage,x1,y1,x2,y2,col1,col2,col3,col4,ALPHA_USE)

/* remplace la couleur src_col dans pimage par dst_col */
void image_replace_color(image *pimage, pix dst_col, pix src_col);

#endif /* __IMAGE_DRAW_FUNC_H__ */
