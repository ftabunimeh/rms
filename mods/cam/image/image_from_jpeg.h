/****************************************************/
/* Pour cree une image a partir d'un fichier JPEG   */
/* Utilise la libjpeg.                              */
/* image_from_jpeg.h                                */
/*                                                  */
/* Ecrit par : Daniel Lacroix (all rights reserved) */
/*                                                  */
/****************************************************/

#ifndef __IMAGE_FROM_JPEG_H__
#define __IMAGE_FROM_JPEG_H__

#include <sys/types.h>
#include <unistd.h>
#include "image.h"
       
#define MAX_JPEG_FILE_SIZE 10000000


/* Renvoi l'image ou NULL si impossible */
image *image_new_from_jpeg(char *file_name);

/* Renvoi l'image ou NULL si impossible */
image *image_new_from_jpeg_stream(FILE *file);

#define JPEG_QUALITY 255

/* Sauvegarde l'image pimage dans le fichier file      */
/* sous le format JPEG. Return -1 = si ERREUR, 0 sinon */
int image_save_to_jpeg(image *pimage, char *file,int out_jpeg_quality);

/* Sauvegarde l'image pimage dans le flux stream       */
/* sous le format JPEG. Return -1 = si ERREUR, 0 sinon */
int image_save_to_jpeg_stream(image *pimage, FILE *stream,int out_jpeg_quality);

#endif /* __IMAGE_FROM_JPEG_H__ */
