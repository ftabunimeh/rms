/*******************************************************************************
Copyright (c) 2004, MSU ECE480 F04 Team01, Faisal Abu-Nimeh
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list
of conditions and the following disclaimer. Redistributions in binary form must
reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the
distribution. Neither the name of the Michigan State University nor the names
of its contributors may be used to endorse or promote products derived from this
software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************/

/*
  Changes:
  10/24/04 [Faisal]: Removed Win32 port support.
  10/28/04 [Faisal]: Save PID in log.
  12/06/04 [Faisal]: STDOUT_FILENO is now closed after spawning.
*/

#include "main.h"
#include "external.h"

/*******************************************************************************
   Function: runservice moves process image to background
   Inputs: VOID
   Outputs: pid file
   Returns: 0
*******************************************************************************/
void runservice()
{
	FILE *pidfile;
	pid_t pid;
	
	pid = fork(); /* spawn a new process */

	if (-1 == pid)
	{
		exitrobot("unable to fork()",-1);
	}

	if ( 0 == pid ) /* child */
	{
	   if(access(RPID,F_OK)==0)
	   {
	      logit("ERROR: [%s] exists! previous unexpected exited?",RPID);
	   }
	   
	   pidfile = fopen(RPID, "w");

	   if (NULL == pidfile)
	   {
	      exitrobot("RMS.pid failed!",-1);
	   }

      fprintf(pidfile, "%d", getpid()); /* write PID to file */
      fclose(pidfile);
      
      logit("%s is in background. PID[%d]",VERSION, getpid());
	   /* clean unused file descriptors 
      close(STDIN_FILENO);
      close(STDERR_FILENO); */
      close(STDOUT_FILENO);
	}

	else /* parent */
	{
	   exit(1);
	}
}
