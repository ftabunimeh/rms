/*******************************************************************************
Copyright (c) 2004, Abu-Nimeh, Faisal
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list
of conditions and the following disclaimer. Redistributions in binary form must
reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the
distribution. Neither the name of the Michigan State University nor the names
of its contributors may be used to endorse or promote products derived from this
software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

   strlib.c : Custom string library version 3.1
              each function has a brief comment above it to explain what it does
              
              * This file should be compiled and linked with main program
              * strlib.h should be included where you have main()
              * you might need to free the memory allocated when you're done
              * Please report bugs to asd1815@msu.edu
              * Last modified: November 2, 2004
   
*******************************************************************************/

#include <stdlib.h>
#define EMPTYSTR ""

int charocc( char *, char , int );
int charcount( char *, char );
int strlength( char *);
char *rmchar(char *, char );
char *substr(char *, int , int );
char *gettok(char *, char , int );
char **arrstr(char *, char );

/* Find the nth occurance postion of a character in a string */
int charocc( char *str, char c, int n)
{
	int i, j = 0;
	
	if (n == 0)
	{ return(0); }
	
	for (i = 0; i<=strlength(str); i++)
	{
		if (str[i] == c )
		{
			if( ++j == n )
			{ return (i); }
		}
	}
	
	return -1; /* -1 not found */
}

/* Count number of character repeatition in string */
int charcount( char *str, char c)
{
	int i, j = 0;
	
	for (i = 0; i<=strlength(str); i++)
	{
		if (str[i] == c )
		{ j++; }
	}
	
	return(j);
}

/* Calculate string length */
int strlength( char *str)
{
	char *s = str;
 	
 	for (s=s; *s != 0; ++s)
	{ /* count it */ }     
     return(s - str);
}

/* Remove a character from a string */
char *rmchar(char *str, char c)
{
	int i,j;
	
	for (i = 0; str[i] != 0; i++)
	{
		if (str[i] == c)
		{
			for(j = i; str[j] != 0; j++)
			{
				str[j] = str[j+1];
			}
		}
	}
	return(str);
}
/* create a substring from string */
char *substr(char *str, int I, int II)
{
	int i, j = 0;
	char *output;

	if ( (I>II) || (I<0) || (II<0) )
	{ return(EMPTYSTR); }
	
	output = (char *) malloc( sizeof(char)*(II-I+2) );
	
	if( output == NULL )
	{ return(EMPTYSTR); }

	for (i = 0; str[i] != 0; i++)
	{
		if ( (i >= I) && (i <= II) )
		output[j++] = str[i];
	}
	output[j] = '\0';
	
	return(output);
}

/* Get a specific token from a string seperated by c */
char *gettok(char *str, char c, int I)
{
	int n = charcount(str,c); /* number of delimiters */
	
	if (I > n+1)
	{ return(EMPTYSTR); }
	
	else if (n == 0 )
	{ return(str); }
	
	else if ( I == 1 ) /* first token */
	{ return(substr(str,0,charocc(str,c,I)-1)); }
	
	else if ( I == n+1) /* last token */
	{ return(substr(str,charocc(str,c,I-1)+1,charocc(str,'\0',1)-1)); }
	
	/* else */
	return(substr(str,charocc(str,c,I-1)+1,charocc(str,c,I)-1));
	
}
/* tokenizes string seperated by c and stores tokens in an array of strings */
char **arrstr(char *str, char c)
{
	int n,i;
	char **output;
	
	n = charcount(str,c); /* number of delimiters */
	
	output = calloc(n+2, sizeof(char *) );
	if (output == NULL)
	{ return(output); }
	
	for(i=0; i<n+1; i++)
	{
		output[i] = gettok(str,c,i+1);
	}
	output[i] = NULL;
	
	return(output);
}


