/*******************************************************************************
Copyright (c) 2004, MSU ECE480 F04 Team01, Faisal Abu-Nimeh
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list
of conditions and the following disclaimer. Redistributions in binary form must
reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the
distribution. Neither the name of the Michigan State University nor the names
of its contributors may be used to endorse or promote products derived from this
software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************/

/*
  Changes:
  10/30/04 [Faisal]: Instead of initializing structs globaly
                     now they will be passed to specific functions only.
  11/18/04 [Faisal]: Added uptime counter.
*/

#include "main.h"
#include "external.h"

/*******************************************************************************
   Function: main initializes all needed structures for launch
   Inputs: Command line parameters
   Outputs: VOID
   Returns: 0
*******************************************************************************/
int main(int argc, char *argv[])
{
   struct bot robo;
   
   time(&(robo.startupt)); /* start up time */
   
   chg2RMSpath(argv[0]); /* fix cwd */
   logit("# # #");
   logit("Starting %s",VERSION);
   readcmdconf(CMDS); /* assign robot commands */
   parsenloadconf(&robo,CONF); /* load main config */
   runservice(); /* run in background as a service */
   
   startnet(&robo); /* waits for connections forever */
   
   return 0;
}
