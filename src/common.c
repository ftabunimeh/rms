/*******************************************************************************
Copyright (c) 2004, MSU ECE480 F04 Team01, Faisal Abu-Nimeh
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list
of conditions and the following disclaimer. Redistributions in binary form must
reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the
distribution. Neither the name of the Michigan State University nor the names
of its contributors may be used to endorse or promote products derived from this
software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************/

/*
  Changes:
  10/26/04 [Faisal]: Added chg2RMSpath() to fix path problems
  11/14/04 [Faisal]: Commented out printconf()
  11/22/04 [Faisal]: - PID file is deleted on exit.
                     - completed rehashrobot()
  12/06/04 [Faisal]: Added new values to resetrobo()
  
*/

#include "main.h"
#include "external.h"

/*******************************************************************************
   Function: exitrobot tracks all exits here
   Inputs: exit event track message, exit return vaule
   Outputs: writes track message to log if available
   Returns: VOID

   TODO: Add memory, sockets, etc clean up.
*******************************************************************************/
void exitrobot(char *rtrack,int num)
{
   if (strlen(rtrack) > 0)
   {
      logit("[NUM %d] RMS exiting: %s",num, rtrack);
   }
   if(num<0) { fprintf(stderr, "ERROR: check log file!\n"); }
   
   unlink(RPID);
	exit(num);
}

/*******************************************************************************
   Function: rehashrobot reloads config file
   Inputs:
   Outputs:
   Returns: VOID

*******************************************************************************/
void rehashrobot()
{
   logit("Reloading RMS in 10 seconds");
   
   /* system("cd ..; ./rms restart"); */
}

/*******************************************************************************
   Function: chg2RMSpath find path to RMS and chdir to target
   Inputs: command line argument used to execute RMS
   Outputs: chdir to RMS bin folder
   Returns: VOID
*******************************************************************************/
void chg2RMSpath(char *rmsf)
{
   int n = charcount(rmsf,'/');
   char *path; /* path to RMS executable */
   if(n == 0) { return; } /* called directly */
   
   else
   {
      path = substr(rmsf,0,charocc(rmsf,'/',n));
      if(chdir(path) == -1)
      {
         fprintf(stderr, "ERROR: cannot change to path [%s]\n", path);
         exitrobot("",-1);
      }
      
      /* in directory */
   }
}

/*******************************************************************************
   Function: printconf displays current vaules of robot struct
   Inputs: robot struct
   Outputs: print out to stdout values
   Returns: VOID
*******************************************************************************/
void printconf(struct bot *robot)
{
#ifdef DEBUG
   int i;
   
   if (NULL == robot->login)
   {
      fprintf(stderr, "ERROR: username is not SET\n");
   }
   else
   {
      fprintf(stdout, "username is [%s]\n",robot->login);
   }
   
   if (NULL == robot->pass)
   {
      fprintf(stderr, "ERROR: password is not SET\n");
   }
   else
   {
      fprintf(stdout, "password is [%s]\n",robot->pass);
   }
   if (-1 == robot->port)
   {
      fprintf(stderr, "ERROR: port is not SET\n");
   }
   else
   {
      fprintf(stdout, "port is [%d]\n",robot->port);
   }
   
   if (NULL == robot->modules)
   {
      fprintf(stderr, "ERROR: no modules are currently loaded\n");
   }
   else
   {
      /*
      mods = curloadedmods();
      for(i=0; mods != NULL; i++)
      {
         printf("[%s]\n",mods[i]);
      }*/
	}
   
   if (-1 == robot->timeout)
   {
      fprintf(stderr, "ERROR: timeout is not SET\n");
   }
   else
   {
      fprintf(stdout, "timeout is [%d]\n",robot->timeout);
   }
   if (-1 == robot->maxcon)
   {
      fprintf(stderr, "ERROR: maxcon is not SET\n");
   }
   else
   {
      fprintf(stdout, "maxcon is [%d]\n",robot->maxcon);
   }
   if (NULL == robot->arm_com)
   {
      fprintf(stderr, "ERROR: arm_com is not SET\n");
   }
   else
   {
      fprintf(stdout, "arm_com is [%s]\n",robot->arm_com);
   }
   if (NULL == robot->motor_com)
   {
      fprintf(stderr, "ERROR: motor_com is not SET\n");
   }
   else
   {
      fprintf(stdout, "motor_com is [%s]\n",robot->motor_com);
   }
   if (NULL == robot->teles_com)
   {
      fprintf(stderr, "ERROR: teles_com is not SET\n");
   }
   else
   {
      fprintf(stdout, "teles_com is [%s]\n",robot->teles_com);
   }
   if (NULL == robot->spec_com)
   {
      fprintf(stderr, "ERROR: spec_com is not SET\n");
   }
   else
   {
      fprintf(stdout, "spec_com is [%s]\n",robot->spec_com);
   }
#endif
}

/*******************************************************************************
   Function: initializes robot structure
   Inputs: robot struct
   Outputs: sets robot struct elements to corresponding values
   Returns: VOID
*******************************************************************************/
void resetrobo(struct bot *robot)
{
   robot->login = NULL;
   robot->pass = NULL;
   robot->port = -1;
   
   robot->timeout = -1;
   robot->maxcon = -1;
   robot->arm_com = NULL;
   robot->motor_com = NULL;
   robot->teles_com = NULL;
   robot->sense_com = NULL;
}

