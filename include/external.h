/*******************************************************************************
Copyright (c) 2004, MSU ECE480 F04 Team01, Faisal Abu-Nimeh
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list
of conditions and the following disclaimer. Redistributions in binary form must 
reproduce the above copyright notice, this list of conditions and the following 
disclaimer in the documentation and/or other materials provided with the 
distribution. Neither the name of the Michigan State University nor the names 
of its contributors may be used to endorse or promote products derived from this 
software without specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR 
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************/

/* log.c external functions */
extern void logit(char *, ...); /* add entries to log file */

/* common.c external functions */
extern void exitrobot(char *,int );
extern void rehashrobot();
extern void chg2RMSpath(char *);
extern void printconf(struct bot *);
extern void resetrobo(struct bot *);

/* service.c external functions */
extern void runservice();

/* service.c external functions */
extern void parsenloadconf(struct bot *,char *);

/* modules.c external functions */
extern int loadmod(struct bot *,char *);
extern int unloadmod(struct bot *,char *);
extern void unloadallmods(struct bot *);

/* network.c external functions */
extern void startnet(struct bot *);
void sendtoclient(RCLIENT *, char *, ...);

/* cmds.c external functions */
extern void readcmdconf(char *);
extern int addcmd(int, int (*)() ,char *);
extern int runcmd(RCLIENT *, int , char *, ...);
extern void delmodcmds(char *);

/* interncmds.c external functions */
int intern_null(RCLIENT *, char *);
int intern_uptime(RCLIENT *, char *);
int intern_rehash(RCLIENT *, char *);
int intern_settings(RCLIENT *, char *);
int intern_exit(RCLIENT *, char *);
int intern_loadmod(RCLIENT *, char *);
int intern_uloadmod(RCLIENT *, char *);
int intern_uloadallmods(RCLIENT *, char *);

/* netprocess.c external function */
extern void processinput (RCLIENT*, char*);

