/*******************************************************************************
Copyright (c) 2004, MSU ECE480 F04 Team01, Faisal Abu-Nimeh
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list
of conditions and the following disclaimer. Redistributions in binary form must
reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the
distribution. Neither the name of the Michigan State University nor the names
of its contributors may be used to endorse or promote products derived from this
software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************/

/*
  Changes:
  10/26/04 [Faisal]: Implemented timeout and maxcon using global struct.
  10/28/04 [Faisal]: Added new sendtoclient() function.
                     Fixed a flaw in delclient() causing sync problems
  11/03/04 [Faisal]: packet and their time sync is now more accurate.
  11/13/04 [Faisal]: Changes as follows:
                     - startnet() only accepts one argument
                     - startnet() will initialize socket structure not main()
                     - removed remote client array.
                     - remote client list is a dynamic linked list.
                     - new type called RONET check main.h
                     - completely changed remote client struct and main socket
                       struct as a result many things will look different now.
                     - net_listen() will initialize server own listening socket
                       this struct won't be global.
                     - checksockets() now uses the remote client linked list
                       instead of using the array.
                     - removed and rewrote all client handling functions
                       addclient() , findclient(), and delclient()
                     - removed resetclient() it is no longer needed.
                     - sendtoclient() uses the new remote client linked list.
  11/14/04 [Faisal]: Removed previous remote command test.
  11/17/04 [Faisal]: Fixed bug reported by Steve from Team03 when using recv()
  11/18/04 [Faisal]: - recv()ed buf is zeroed out before used using memset().
                     - recv()ed buf is parsed before calling runcmd()
                     - sendtoclient() now accepts formatted arguments
  12/05/04 [Faisal]: - Added netprocess.c
                     - Added authorize() to check for valid login/password
                     - Added back setsockopt() to net_listen()
  12/09/04 [Faisal]: - Last minute change to client protocol. Now commands are 
                       not stored in the first byte of the received data.
                       however they are the first argument of it.
*/

#include "main.h"
#include "external.h"

void net_listen(struct bot *, RONET *);
void waitforconnections(struct bot * , RONET *);
void checksockets(struct bot *, RONET *);
void checktimedout(struct bot *, RONET *);
void addclient(struct bot *,int , char *, RONET *);
RCLIENT *findclient(int );
void delclient(int , RONET *);
void sendtoclient(RCLIENT *, char *, ...);
void sendtoclient(RCLIENT *, char *, ...);
int authorize(struct bot *,RCLIENT *,char *);

static RCLIENT *clnt_head = NULL;
static RCLIENT *clnt_tail = NULL;
static RCLIENT *clnt_curr = NULL;
/*******************************************************************************
   Function: startnet start network protocol
   Inputs: robot socket struct
   Outputs: sets number of users to zero, starts listen socket, 
            wait for connections
   Returns: VOID
*******************************************************************************/
void startnet(struct bot *robot)
{
   RONET robosock;
   
   robosock.users = 0;
   net_listen(robot,&robosock); /* starts listening socket */
   waitforconnections(robot,&robosock); /* wait for connections */
   
}

/*******************************************************************************
   Function: net_listen starts listening on a socket defined in struct
   Inputs: robot socket struct
   Outputs: listening fd
   Returns: VOID
   
   TODO: Add IPv6 support
*******************************************************************************/
void net_listen(struct bot *robo, RONET *rsock)
{
   struct sockaddr_in myaddr;
   int ok=1;
   
   rsock->lsnsock = socket(AF_INET, SOCK_STREAM, 0);
   if ( -1 == rsock->lsnsock)
   {
      logit("[socket] %s",strerror(errno));
      exitrobot("socket() failed",-1);
   }
   
   if (setsockopt(rsock->lsnsock,SOL_SOCKET,SO_REUSEADDR,&ok,sizeof(int)) == -1)
   {
      logit("[setsockopt] %s",strerror(errno));
      exitrobot("setsockopt() failed",-1);
   }
   
   
   /* TODO: add IPv6 support */
   myaddr.sin_family = AF_INET;
   myaddr.sin_addr.s_addr = htonl(INADDR_ANY);
   myaddr.sin_port = htons(robo->port);
   memset(&(myaddr.sin_zero), '\0', 8);
   
   if( -1 == bind( rsock->lsnsock,
                  (struct sockaddr *)&myaddr,
                  sizeof(struct sockaddr))
     )
   {
      logit("[bind] %s",strerror(errno));
      exitrobot("bind() failed!",-1);
   }
   
   if (-1 == listen(rsock->lsnsock, robo->maxcon))
   {
      logit("[listen] %s",strerror(errno));
      exitrobot("listen() failed!",-1);
   }
}

/*******************************************************************************
   Function: waitforconnections uses select() to multiplex between fd's
             it loops forever lookin for a change in status
   Inputs: robot socket struct
   Outputs: checks for socket change or socket timeout if defined
   Returns: VOID
*******************************************************************************/
void waitforconnections(struct bot *robo, RONET *rsock)
{
   int rsel;
   struct timeval tv;
   
   signal( SIGPIPE, SIG_IGN );
      
   FD_ZERO( &(rsock->master) );
   FD_ZERO( &(rsock->read_fds) );
   
   FD_SET(rsock->lsnsock, &(rsock->master));
   
   rsock->fdmax = rsock->lsnsock;
   if (robo->timeout > 0)
   {
      tv.tv_sec = robo->timeout;
      tv.tv_usec = 1000; /* 1ms sec after to catch timeout */
   }
   
   for(;;)
   {
      rsock->read_fds = rsock->master;
      
      /* timeout enabled */
      if (robo->timeout > 0)
      {
         rsel = select(rsock->fdmax+1, &(rsock->read_fds), NULL, NULL, &tv);
         checktimedout(robo,rsock);
      }
      
      /* timeout disabled */
      if (robo->timeout == 0)
      {
         rsel = select(rsock->fdmax+1, &(rsock->read_fds), NULL, NULL, NULL);
      }
      
      if ( rsel == -1)
      {
         logit("[select] %s",strerror(errno));
         exitrobot("select failed",-1);
      }
      /* scan for new packets */
      checksockets(robo,rsock);
   }
}

/*******************************************************************************
   Function: checksockets will go thru FD_SET lookin for a change in status
   Inputs: robot socket struct
   Outputs: should handle new connections and data received from them
            checks for maximum number of connections if defined
   Returns: VOID
*******************************************************************************/
void checksockets(struct bot *robo, RONET *rsock)
{
   int i;
   int addrlen;
   int newfd;
   int rbytes;
   char *buf;
   char *raddr;
   time_t now;
   RCLIENT *client;
   struct sockaddr_in remoteaddr;
   
   /* why start from 0 ? */
   for(i = 0; i <= rsock->fdmax; i++)
   {
      time(&now);
      if ( FD_ISSET(i, &(rsock->read_fds)) )
      {
         /* listen socket changed */
         if(i == rsock->lsnsock)
         {
            addrlen = sizeof(remoteaddr);
            newfd = accept( rsock->lsnsock,
                            (struct sockaddr *)&remoteaddr,
                            &addrlen);
            
            if (-1 == newfd)
            {
               logit("[accept] %s",strerror(errno));
            }
            
            else
            {
               FD_SET(newfd, &(rsock->master));
               
               if (newfd > rsock->fdmax) { rsock->fdmax = newfd; }
               /* check if maxcon reached */
               if ((robo->maxcon > 0) && (rsock->users >= robo->maxcon))
               {
                  close(newfd);
                  FD_CLR(newfd, &(rsock->master) );
                  FD_CLR(newfd, &(rsock->read_fds) );
                  
               }
               
               /* new connection received */
               else
               {
                  raddr = inet_ntoa(remoteaddr.sin_addr);                  
                  addclient(robo,newfd,raddr,rsock);
                  logit("new connection from [%s] on socket [%d]",raddr,newfd);
               }
            }
         }
         
         /* received socket changed */
         else
         {
            client = (RCLIENT *)findclient(i);
            /* receive buffer */
            
            /* check if we received anything */
            buf = malloc(MIDBUF * sizeof(char)); /* allocate memory for buffer */
	         if (NULL == buf) /* check sufficient memory */
	         {
	            logit("Unable to allocate memroy received on socket [%d]",i);
	            return;
	         }
	         memset(buf,0,MIDBUF);
	         
            if ( (rbytes = recv(i,buf,MIDBUF, 0)) <= 0)
            {
               /* nothing received */
               if (0 == rbytes)
               {
                  logit("Client [%s] on socket [%d] disconnected",
                        client->address,i);
               }
               
               /* dead */
               else
               {
                  logit("[recv] %s",strerror(errno));
               }
               
               delclient(i,rsock);
            }
            
            /* buffer got something */
            else
            {
               /* update last command received */
               client->lastrecv = now;
               
               /* login */
               if(buf[0] == '9')
               {
                  if (authorize(robo,client,&buf[2]) < 0)
                  {
                     logit("client [%s] on socket [%d] login/pass failed!",
                            client->address,i);
                     delclient(i,rsock);
                  }
                  
                  else
                  {
                     logit("Authrozied client [%s] on socket [%d]",
                           client->address,i);
                  }
               }
               
               else
               {
                  if(client->auth == 1)
                  {
                     processinput(client, buf);
                  }
                  else
                  {
                     logit("Disconnecting unauthorized client [%s] on socket"
                           " [%d]! execute commands.",client->address,i);
                     delclient(i,rsock);
                  }
               }
            }
            free(buf);
         }
      }
   }
}

/*******************************************************************************
   Function: Goes thru a set of FDs checking if they passed timeout limit
   Inputs: robot socket struct
   Outputs: disconnect timedout connections
   Returns: VOID
*******************************************************************************/
void checktimedout(struct bot *robo, RONET *rsock)
{
   RCLIENT *clnt_tmp = clnt_head;
   time_t now;
   
   time(&now);

   while(clnt_tmp != NULL)
   {
      if(now >= ((clnt_tmp->lastrecv)+(robo->timeout)))
      {
         logit("Client [%s], socket [%d] timedout",clnt_tmp->address
                                                  ,clnt_tmp->fd);
         delclient(clnt_tmp->fd,rsock);
      }
      clnt_tmp = clnt_tmp->next;
   }
}

/*******************************************************************************
   Function: Will add a client to connection array
   Inputs: robot socket struct, file descriptor, time connected, address struct
   Outputs: sets values for struct
   Returns: VOID
   
   TODO: Set flags for different users like access level may be ?
*******************************************************************************/
void addclient(struct bot *robo,int fd, char *address, RONET *rsock)
{
   time_t now;
   
   time(&now);
   clnt_curr = (RCLIENT *)malloc(sizeof(RCLIENT));
   
	if (clnt_curr == NULL)
	{
		logit("ERROR: Not enough memory. Unable to add Client %s",address);
		return;
	}
	
	clnt_curr->fd = fd;
	clnt_curr->flags = 0;
	clnt_curr->auth = 0;
	clnt_curr->address = address;
   clnt_curr->lastrecv = now;
   clnt_curr->robotstk = robo;
	
	clnt_curr->next = clnt_head;
	clnt_curr->prev = NULL;
	
	if (clnt_tail == NULL)
	{ clnt_tail = clnt_curr; }
	
	else
	{ clnt_curr->next->prev = clnt_curr; }
	
	clnt_head = clnt_curr;
	rsock->users = rsock->users + 1;
}

/*******************************************************************************
   Function: Will look for a specific FD in client list
   Inputs: file descriptort to look for
   Outputs: VOID
   Returns: pointer to client if found, NULL if not found
*******************************************************************************/
RCLIENT *findclient(int fd)
{
   RCLIENT *clnt_tmp = clnt_head;
   
   while(clnt_tmp != NULL)
   {
      if(clnt_tmp->fd == fd)
      {
         return(clnt_tmp);
      }
      clnt_tmp = clnt_tmp->next;
   }
   return(NULL);
}

/*******************************************************************************
   Function: deletes a client from array with cleaning
   Inputs: file descriptor
   Outputs: VOID
   Returns: VOID
*******************************************************************************/
void delclient(int fd, RONET *rsock)
{
   RCLIENT *clnt_tmp = (RCLIENT *)findclient(fd);
   
   if(clnt_tmp != NULL) /* found */
   {
      close(fd);
      FD_CLR(fd, &rsock->master);
      FD_CLR(fd, &rsock->read_fds);
      
      /* Make sure its not a head or a tail */
      if( (clnt_tmp->next != NULL) && (clnt_tmp->prev != NULL) )
      {
         clnt_tmp->prev->next = clnt_tmp->next;
         clnt_tmp->next->prev = clnt_tmp->prev;
      }
      
      /* either head or tail or both */
      else
      {
         /* Both head and tail == one element in list */
         
         if( (clnt_tmp == clnt_head) && (clnt_tmp == clnt_tail) )
         {
            clnt_head = NULL;
            /* keep it up2date */
            clnt_tail = NULL;
         }
         
         /* either one */
         else
         {
            /* head only */
            if(clnt_tmp == clnt_head)
            {
               clnt_tmp->next->prev = NULL;
               clnt_head = clnt_tmp->next;
               /* keep it up2date */
            }
            
            /* tail only */
            else if(clnt_tmp == clnt_tail)
            {
               clnt_tmp->prev->next = NULL;
               clnt_tail = clnt_tmp->prev;
            }
         }
      }
      free(clnt_tmp);
      rsock->users = rsock->users -1;
   }
}

/*******************************************************************************
   Function: universal socket send that will be used for all modules
   Inputs: -
   Outputs: -
   Returns: VOID
*******************************************************************************/
void sendtoclient(RCLIENT *client, char *format, ...)
{
   int len;
   char *buf;
   FILE *tempf;
   va_list args;
   
   /* check if passed arguments are NULL */
   if (NULL == format)
   {
      logit("ERROR: sendtoclient() cannot send NULL to remote host");
      return;
   }

	va_start(args, format);
	if((tempf = tmpfile()) != NULL)
	{
	   len = vfprintf(tempf, format, args) + 1;
	   fclose(tempf);
	   if (len < 1)
	   {
	      logit("ERROR: sendtoclient() writing to temp file failed!");
	      va_end(args);
	      return;
	   }
	   
	   buf = malloc(len * sizeof(char)); /* allocate memory for buffer */
	   if (NULL == buf) /* check sufficient memory */
	   {
	      logit("ERROR: sendtoclient() Not enough memory!");
	      va_end(args);
	      return;
	   }
	   
	   vsprintf(buf, format, args);
	    
	   if(send(client->fd,buf,strlen(buf),0) == -1)
	   {
	      logit("[send] %s",strerror(errno));
	      logit("ERROR: Sending data to Client [%s] on socket [%d] failed.",
            client->address,
            client->fd);
      }
	   
	   free(buf);
	   va_end(args);
	   return;
	}
	
	logit("ERROR: sendtoclient() temp file failed!");
	va_end(args);
}

/*******************************************************************************
   Function: check if login and password are correct
   Inputs: Client , Login and password
   Outputs: -
   Returns: VOID
   
   TODO: Multi Client Auth
*******************************************************************************/
int authorize(struct bot *robo,RCLIENT *client,char *buffer)
{
   char *username;
   char *password;
   
   rmchar(buffer,'\n');
	rmchar(buffer,'\r');
   
   username = gettok(buffer, ':', 1);
   password = gettok(buffer, ':', 2);
   
   
   if ( (strncmp(username,robo->login,strlen(robo->login)) == 0) &&
        (strncmp(password,robo->pass,strlen(robo->pass)) == 0) )
   {
      client->auth = 1;
      return 1;
   }
   
   client->auth = 0;
   return -1;
}

