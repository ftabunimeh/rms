/*******************************************************************************
Copyright (c) 2004, MSU ECE480 F04 Team01, Faisal Abu-Nimeh
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list
of conditions and the following disclaimer. Redistributions in binary form must
reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the
distribution. Neither the name of the Michigan State University nor the names
of its contributors may be used to endorse or promote products derived from this
software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************/

/*
  Changes:
  11/14/04 [Faisal]: - FILE *conffile is no longer global.
                     - added more logging to parsenloadconf().
  11/15/04 [Faisal]: parsenloadconf() now uses malloc instead of an array
  12/06/04 [Faisal]: Added chg_telescom() and chg_sensecom()
*/

#include "main.h"
#include "external.h"

void configbot(struct bot *, char *, char *);
void chg_user(struct bot *, char *);
void chg_pass(struct bot *, char *);
void chg_port(struct bot *, char *);
void chg_timeout(struct bot *, char *);
void chg_maxcon(struct bot *, char *);
void chg_module(struct bot *, char *);
void chg_armcom(struct bot *, char *);
void chg_motorcom(struct bot *, char *);
void chg_telescom(struct bot *, char *);
void chg_sensecom(struct bot *, char *);

/*******************************************************************************
   Function: opens configuration file for reading and parses its tags
   Inputs: VOID
   Outputs: calls configbot() to saves info in struct
   Returns: VOID
*******************************************************************************/
void parsenloadconf(struct bot *robo,char *conff)
{
   char *buf;
   char *tag, *value;
   FILE *conffile = NULL;
   
   logit("Reading RMS main configuration file [%s]",conff);
   conffile = fopen(conff, "r");

   if (NULL == conffile)
   {
		exitrobot("ERROR: Conf file could not be openned!", -1);
	}
	resetrobo(robo);
	
	buf = malloc(MIDBUF * sizeof(char)); /* allocate memory for buffer */
	if (NULL == buf) /* check sufficient memory */
	{
	   exitrobot("ERROR: Unable to parseconf() Not enough memory!",-1);
	}
	
	while ( (!feof(conffile)) && (NULL != (fgets(buf, MIDBUF, conffile))) )
	{
	   
	   /* eleminate comments/blank/false lines */
	   if(isalpha(buf[0]))
	   {
	      /* trim fgets left overs */
	      rmchar(buf,'\n');
	      rmchar(buf,'\r');
	      
	      tag = gettok(buf, CONFDEL, 1);
	      value = gettok(buf, CONFDEL, 2); /* allocated seperately */
	      
	      /* each parsed value will have its own pointer */
	      configbot(robo,tag,value); /* duplicates will be reported */
	   }

	}
	fclose(conffile);
	logit("Done reading [%s]",conff);
	free(buf);
}

/*******************************************************************************
   Function: receives tags and values then passes them to handle function
   Inputs: robot struct, tag and its value
   Outputs: calls corresponding handle function for verification
   Returns: VOID
   
   To Add new Configuration file tags. Add another else if and a new handle
      function that would check its validation.
*******************************************************************************/
void configbot(struct bot *robot, char *tag, char *value)
{
   /* To expand configuration file tags. Append new tags to the end */
   
   if( strncmp("USER",tag,strlen("USER")) == 0 ) 
   { chg_user(robot,value); }
   
   else if( strncmp("PASS",tag,strlen("PASS")) == 0 )
   { chg_pass(robot,value); }
   
   else if( strncmp("PORT",tag,strlen("PORT")) == 0 )
   { chg_port(robot,value); }
   
   else if( strncmp("MAXCONNECTIONS",tag,strlen("MAXCONNECTIONS")) == 0 )
   { chg_maxcon(robot,value); }
   
   else if( strncmp("TIMEOUT",tag,strlen("USER")) == 0 )
   { chg_timeout(robot,value); }
   
   else if( strncmp("MODULE",tag,strlen("MODULE")) == 0 )
   { chg_module(robot,value); }
   
   else if( strncmp("ARMCOM",tag,strlen("ARMCOM")) == 0 )
   { chg_armcom(robot,value); }
   
   else if( strncmp("WHEELSCOM",tag,strlen("WHEELSCOM")) == 0 )
   { chg_motorcom(robot,value); }
   
   else if( strncmp("TELESCOM",tag,strlen("TELESCOM")) == 0 )
   { chg_telescom(robot,value); }
   
   else if( strncmp("SENSECOM",tag,strlen("SENSECOM")) == 0 )
   { chg_sensecom(robot,value); }
   
   /* Unknown */
   else
   {
      logit("ERROR: configuration file contains an unknown tag [%s]",tag);
   }
}

/*******************************************************************************
   Function: Sets a valid username in robot struct if empty
   Inputs: robot struct, username
   Outputs: saves username in struct. or log if duplicated
   Returns: VOID
   
   TODO: Add length limit
*******************************************************************************/
void chg_user(struct bot *robot, char *value)
{
   if(robot->login == NULL) { robot->login = value; }
   else { logit("[USER] tag in configuration file is duplicated!"); }
}

/*******************************************************************************
   Function: Sets a valid password in robot struct if empty
   Inputs: robot struct, password
   Outputs: saves password in struct. or log if duplicated
   Returns: VOID
   
   TODO: Add length limit
*******************************************************************************/
void chg_pass(struct bot *robot, char *value)
{
   if(robot->pass == NULL) { robot->pass = value; }
   else { logit("[PASS] tag in configuration file is duplicated!"); }
}

/*******************************************************************************
   Function: Sets a valid port in robot struct if empty
   Inputs: robot struct, port
   Outputs: saves port in struct. or log if duplicated
            program will exit if port is invalid !
            
   Returns: VOID
*******************************************************************************/
void chg_port(struct bot *robot, char *value)
{
   if(robot->port == -1)
   {
      robot->port = (int)strtol(value,NULL,10); /* base 10 */
      
      if ( (robot->port <= 0) || (robot->port >= 65536) )
      {
         exitrobot(
           "ERROR: Invalid value for [PORT] in configuration file!", -1);
      }
   }
   else { logit("[PORT] tag in configuration file is duplicated!"); }
}

/*******************************************************************************
   Function: Sets the number of maximum connections allowed
   Inputs: robot struct, maximum number of connections
   Outputs: saves port in struct. or log if duplicated
            program will not fail if maxcon is invalid it will use 0 instead
            
   Returns: VOID
*******************************************************************************/
void chg_maxcon(struct bot *robot, char *value)
{
   if(robot->maxcon == -1)
   {
      /* strtol returns 0 on failure so check if its really 0 */
      if (strncmp("0",value,strlen("0")) == 0) { robot->maxcon = 0; }
      else
      {
         robot->maxcon = (int)strtol(value,NULL,10); /* base 10 */
         if ( (robot->maxcon <= 0) || (robot->maxcon >= MAXNETFD) )
         {
            logit("ERROR: Invalid value for [MAXCONNECTIONS]"
                  " in configuration file! using 0 instead.");
            robot->maxcon = 0;
         }
      }
   }
   else
   { logit("[MAXCONNECTIONS] tag in configuration file is duplicated!"); }
}

/*******************************************************************************
   Function: Sets connection timeout in seconds
   Inputs: robot struct, timeout in seconds
   Outputs: saves timeout in struct. or log if duplicated
            program will not fail if timeout is invalid it will use 0 instead
            
   Returns: VOID
*******************************************************************************/
void chg_timeout(struct bot *robot, char *value)
{
   if(robot->timeout == -1)
   {
      /* strtol returns 0 on failure so check if its really 0 */
      if (strncmp("0",value,strlen("0")) == 0) { robot->timeout = 0; }
      else
      {
         robot->timeout = (int)strtol(value,NULL,10); /* base 10 */
         if (robot->timeout <= 2) /* strtol failed or very low Time Out */
         {
            logit("ERROR: Invalid value for [TIMEOUT]"
                  " in configuration file! using 120 instead.");
            robot->timeout = 120;
         }
      }
   }
   else { logit("[TIMEOUT] tag in configuration file is duplicated!"); }
}

/*******************************************************************************
   Function: Adds Module to modules list
   Inputs: robot struct, module name
   Outputs: add modules to modlist in struct.
            if module file was not found in module directory it will be logged
            
   Returns: VOID
*******************************************************************************/
void chg_module(struct bot *robot, char *value)
{
   /* modules.c will handle everything even error msgs */
   loadmod(robot,value);
}

/*******************************************************************************
   Function: sets COM port for robotic arm
   Inputs: robot struct, com port path
   Outputs: saves com port in robot struct
   Returns: VOID
*******************************************************************************/
void chg_armcom(struct bot *robot, char *value)
{
   if(robot->arm_com == NULL) { robot->arm_com = value; }
   else { logit("[ARMCOM] tag in configuration file is duplicated!"); }
}

/*******************************************************************************
   Function: sets COM port for robotic motor (wheels)
   Inputs: robot struct, com port path
   Outputs: saves com port in robot struct
   Returns: VOID
*******************************************************************************/
void chg_motorcom(struct bot *robot, char *value)
{
   if(robot->motor_com == NULL) { robot->motor_com = value; }
   else { logit("[WHEELSCOM] tag in configuration file is duplicated!"); }
}

/*******************************************************************************
   Function: sets COM port for robotic Telescope
   Inputs: robot struct, com port path
   Outputs: saves com port in robot struct
   Returns: VOID
*******************************************************************************/
void chg_telescom(struct bot *robot, char *value)
{
   if(robot->teles_com == NULL) { robot->teles_com = value; }
   else { logit("[TELESCOM] tag in configuration file is duplicated!"); }
}

/*******************************************************************************
   Function: sets COM port for robotic Sensors
   Inputs: robot struct, com port path
   Outputs: saves com port in robot struct
   Returns: VOID
*******************************************************************************/
void chg_sensecom(struct bot *robot, char *value)
{
   if(robot->sense_com == NULL) { robot->sense_com = value; }
   else { logit("[SENSECOM] tag in configuration file is duplicated!"); }
}


