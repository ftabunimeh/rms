#include "arm.h"


void register_mod(char *myname)
{
   addcmd(ARMCMD,InitializeMain, myname);

}

/* This function will be called when the module is unloaded */
void unregister_mod(char *myname)
{
   
}
	
/***********************************************************
Function Name: InitializeMain
Function Objective:  Basically the "main" function for the arm control module
Arguements:  RCLIENT * remote_client, char * buf:  the socket, and input string
Return:  0
***********************************************************/
int InitializeMain(RCLIENT * remote_client, char * buf)
{
	struct ArmPos * ArmRef=&CurrentPos;  //make a pointer to the CurrentPos Struct
	struct Angles * AngleRef=&CurrentAngles; //make a pointer to the CurrentAngles Struct
	struct ArmPos InputPos;  //a struct to store the imput
	struct ArmPos * InputRef=&InputPos; //pointer to the input struct
	
	
	if(Load_Unload==0)
	{	
		
		InitAngles(AngleRef,ArmRef,0); //Initialize the AngleRef Struct
		sleep(2);
		InitAngles(AngleRef,InputRef,0);//need to call twice so that serial port autodetects,then puts in correct position
		sleep(2);
		Load_Unload=1;
	}
	
	{
	COMPRT = remote_client->robotstk->arm_com;//set the COMPRT for the arm
	MyClient=remote_client;
		if(ParseInput(buf,InputRef,AngleRef,ArmRef)==0)//if it equals 1 then we are autodocking and do not want to use resolution
		{
			errorcode=resolution(InputRef,AngleRef,ArmRef);//Function Does most of the main work to Calculate the new angles for the servo, also makes sure that position is valid (returnvalue=1), it will only change the currentAngles if the arm will be in a safe state.  Error code is also set for autodock in the ParseInput Function
		}
		returnstring(errorcode,ArmRef);
//		sendtoclient(remote_client,returnstring(errorcode, ArmRef));//return feedback string
		
	}
	
	return 0;
}



/***********************************************************
Function Name: InitAngles
Function Objective:  Initialize the Angle Struct, and the ArmPos struct to the Default State
Arguements:  struct ArmPos * CurrentAngles, RCLIENT * remote_client, char * buf
Return:  None
***********************************************************/
void InitAngles(struct Angles *AngleRef, struct ArmPos * ArmRef, int start)//This Function Initializes the Angles and Position for the struct
{
	ArmRef->Pos_x=2.0+Link3;
	ArmRef->Pos_y=14;
	if(start==0)//if it is the first time (since we need to have the arm power down in a different position, then the docking procedure
	{
		ArmRef->Pos_z=0;
	}
	else//if we are powering down the arm, change the z pos
	{
		ArmRef->Pos_z=-1*(2.0+Link3);
	}
	ArmRef->Pos_pitch=5;//center
	ArmRef->Pos_yaw=0;//center
	ArmRef->Pos_roll=-3;//center
	
	//puts the arm in a default state
	if(start==0)
	{
		AngleRef->servo0=0;//base
	}
	else
	{
		AngleRef->servo0=100;//base
	}
	AngleRef->servo1=-60.0*REVERSE_SERVO;//shoulder 45 degrees back, but the servos represent it as +45
	AngleRef->servo2=-70.0*REVERSE_SERVO;//elbow 50 degrees down, but the servos represent it as +50
	AngleRef->servo3=5.0;//pitch
	AngleRef->servo4=0;//yaw
	AngleRef->servo5=-3;//roll
	

	SendOutput(AngleRef,ArmRef);//use null just because we will change the sendoutput function later
	
}


/***********************************************************
Function Name: ParseInput
Function Objective:  Takes in the input string passed to the program and parses the string into the correct format for the desired command
Arguements:  char * buf, struct ArmPos * InputRef
Return:  
***********************************************************/
int ParseInput(char * buf, struct ArmPos * InputRef, struct Angles * AngleRef, struct ArmPos * ArmRef)
{
	int i=0;//counting variable
	
	float auto_increment=0;
	int multifunction=-1;
	
	//need to initialize the variables to their current position incase if a change does not occur
	InputRef->Pos_x=ArmRef->Pos_x;
	InputRef->Pos_y=ArmRef->Pos_y;
	InputRef->Pos_z=ArmRef->Pos_z;
	InputRef->Pos_pitch=ArmRef->Pos_pitch;
	InputRef->Pos_yaw=ArmRef->Pos_yaw;
	InputRef->Pos_roll=ArmRef->Pos_roll;

	while(buf[i]!='\0')
	{
		switch (buf[i])
		{
			case 'A':
				i++;
				InputRef->Pos_x=NumVal(buf,&i);
			break;
			case 'B':
				i++;
				InputRef->Pos_y=NumVal(buf,&i);
			break;
			case 'C':
				i++;
				InputRef->Pos_z=NumVal(buf,&i);
			break;
			case 'D':
				i++;
				InputRef->Pos_pitch=NumVal(buf,&i);
			break;
			case 'E':
				i++;
				InputRef->Pos_yaw=NumVal(buf,&i);
			break;
			case 'F':
				i++;
				InputRef->Pos_roll=NumVal(buf,&i);
			break;
			case 'G':
				i++;
				num_steps=NumVal(buf,&i);
				if(num_steps<1)
				{
					num_steps=1;
				}
				break;
			case 'H':
				i++;
				multifunction=(int) NumVal(buf,&i);
				if(multifunction==0)//autodock
				{
					errorcode=autodock();//not fully implemented yet!
					return 1;
				}
				else if(multifunction==1)//reset
				{
					errorcode=1;
					InitAngles(AngleRef,ArmRef,0);
					return 1;
				}
				else if(multifunction==2)//reset and exit
				{
					errorcode=1;
					InitAngles(AngleRef,ArmRef,1);
					Load_Unload=0;
					return 1;
				}
				
			break;
			case 'I':
				i++;
				auto_increment=(int) NumVal(buf,&i);
				if(auto_increment!=0)
				{
					IncPos(ArmRef, InputRef, auto_increment);
					return 0;
				}
			break;
			
			default:
				i++;
			break;
		}
		
		i++;
	}
	if(DEBUG==1)
	{
		printf("\nstring is [%s]",buf);
		printf("\nPos_x=%f",InputRef->Pos_x);
		printf("\nPos_y=%f",InputRef->Pos_y);
		printf("\nPos_z=%f",InputRef->Pos_z);
		printf("\nPos_pitch=%f",InputRef->Pos_pitch);
		printf("\nPos_yaw=%f",InputRef->Pos_yaw);
		printf("\nPos_roll=%f",InputRef->Pos_roll);
		printf("\nauto-increment=%f",auto_increment);
		printf("\nmultifunction=%d",multifunction);
	
	}
	return 0;
	
	
}	

/***********************************************************
Function Name: NumVal
Function Objective:  Called by the ParseInput function to return the value of the Parsed string as a float
Arguements:  char * buf, int * pos
Return:  float value of the parsed text
***********************************************************/
float NumVal(char * buf, int *pos)
{
	float returnVal;
	char *strVal;
	int strPos;
	int i=*pos;

	strVal=malloc(10*sizeof(char));
	
	strPos=0;
	while(buf[i]!=',' && buf[i]!='\0' && strPos<10)
	{	
		strVal[strPos]=buf[i];
		strPos++;
		i++;
	}
	*pos=i;
	
	returnVal=(float) atof(strVal);
	
	free(strVal);
	return returnVal;
}


/***********************************************************
Function Name: NewPos
Function Objective:  This function performs the inverse kinematics for the arm.  In addition it calls the ValidPos function to make sure 	that a safe state will be reached.  If it will result in a safe state, then the SendOutput function is called to put the arm 		into the new position.  If a non safestate is the result of the ValidPos then no position change will occur.
Arguements:  struct ArmPos * InputRef, struct Angles * AngleRef, struct ArmPos * ArmRef
Return:  Int 1 if the operation was successful (results in a safe-state), or 0 if the operation would result in a bad state, in which 		case no change in position occurs
***********************************************************/
int NewPos(struct ArmPos *InputRef, struct Angles *AngleRef, struct ArmPos *ArmRef, int execute)
{
	float Xb=0.0;//X-Coordinate at the end of L2
	float Yb=0.0;//Y-Coordinate at the end of L2
	float Q=0.0;//storage variable for a calculation
	float P1=0.0;//angle L1 from the ground
	float P2=0.0;//angle of L2 from the ground
	float T0=0.0;//servo0 angle
	float T1=0.0;//servo1 angle
	float T2=0.0;//servo2 angle
	float T3=0.0;//servo3 angle
	float T4=0.0;//servo4 angle
	float T5=0.0;//servo5 angle
	
	
	
	InputRef->Pos_pitch*=rad;//Convert To radians for the calculations
	
	
	//below are the calculations to determine the angles
	Xb=( (InputRef->Pos_x - (Link3*cos(InputRef->Pos_pitch))) / (2*Link1) );//calculates the Xcoordinate at the end of L2
	Yb=( (InputRef->Pos_y - baseheight - (Link3*sin(InputRef->Pos_pitch))) / (2*Link1));//calculates the Ycoordinate at the end of L2
	Q=sqrt(1/(pow(Xb,2)+pow(Yb,2))-1);
	P1=(atan( ((Yb+Q*Xb)/(Xb-Q*Yb)) )*deg+180);//the +180 is to compensate for the atan function behavior depending on the quadrant
	if(P1>180)//this is necessary to compensate for the atan functions behavior depending on what quadrant the angle is in, since we are using floating point variables you cannot use % operator, and this is much faster than using the fmod function
	{
		P1-=180;
	}
	P2=atan( ((Yb-Q*Xb)/(Xb+Q*Yb)) )*deg;
	
	T0=atan((InputRef->Pos_z/InputRef->Pos_x) )*deg;//this calculates the base angle
	T1=P1-90-servo_offset1;//servo1 angle
	T2=(P2-T1+servo_offset2)*REVERSE_SERVO;//servo2 angle
	T3=InputRef->Pos_pitch*deg-P2+servo_offset3; //servo3 angle
	T4=InputRef->Pos_yaw;//servo angle4
	T5=InputRef->Pos_roll*REVERSE_SERVO;//directly input the servo angle from input
	

	if(ValidPos(T0,T1,T2,T3,T4,T5,Xb,Yb)==1)
	{
		//the arm will be in a safe state, proceed, and update all values
		
		//update the anglestruct to the new angles
		if(execute==1)//used to command the arm to move, if 0 then we just want to check if the position can be obtained, used for resolution func.
		{
			AngleRef->servo0=T0;
			AngleRef->servo1=T1;
			AngleRef->servo2=T2;
			AngleRef->servo3=T3;
			AngleRef->servo4=T4;
			AngleRef->servo5=T5;
			
			//update the armstruct to the new coordinates
			ArmRef->Pos_x=InputRef->Pos_x;
			ArmRef->Pos_y=InputRef->Pos_y;
			ArmRef->Pos_z=InputRef->Pos_z;
			ArmRef->Pos_pitch=InputRef->Pos_pitch*deg;
			ArmRef->Pos_yaw=InputRef->Pos_yaw;
			ArmRef->Pos_roll=InputRef->Pos_roll;
			
			
			SendOutput(AngleRef,ArmRef);//output the new values of the arm position & angles	
		}
		InputRef->Pos_pitch*=deg;//need to change this back to deg
		return 1;
	}
	else
	{
		//arm will not be in a safe state!!! do not update the values as the arm has not changed position
		InputRef->Pos_pitch*=deg;//need to change this back to deg
		return 0;
	}
}

/***********************************************************
Function Name: ValidPos
Function Objective:  This function checks safe state will be reached.  It will return a 1 if it will result in a safe state, otherwise it wil output which servo is over/under extending and specify the degree; and finally it will return a 0.
Arguements:  float servo0, float servo1, float servo2 ,float servo3, float servo4, float servo5, float Xb, float Yb
Return:  Int 1 if the operation was successful (results in a safe-state), or 0 if the operation would result in a bad state
***********************************************************/
int ValidPos(float servo0, float servo1, float servo2 ,float servo3, float servo4, float servo5, float Xb, float Yb)//this function makes sure that the arm cannot enter a non-safe state
{

	if((pow(Xb,2)+pow(Yb,2))>1)
	{
		printf("\nInput would cause a non-safe state!  Impossible due to arm length - Arm Position not changed, please input valid coordinates!\n");
		return 0;
	}
	else if(servo0>servo_max_range0 || servo0<servo_min_range0)
	{
		printf("\nInput would cause a non-safe state!  Servo0 Range, Attempted Angle=%f - Arm Position not changed, please input valid coordinates!\n",servo0);
		return 0;
	}
	else if(servo1>servo_max_range1 || servo1<servo_min_range1)
	{
		printf("\nInput would cause a non-safe state!  Servo1 Range, Attempted Angle=%f - Arm Position not changed, please input valid coordinates!\n",servo1);
		return 0;
	}
	else if(servo2>servo_max_range2 || servo2<servo_min_range2)
	{
		printf("\nInput would cause a non-safe state!  Servo2 Range, Attempted Angle=%f - Arm Position not changed, please input valid coordinates!\n",servo2);
		return 0;
	}
	else if(servo3>servo_max_range3 || servo3<servo_min_range3)
	{
		printf("\nInput would cause a non-safe state!  Servo3 Range, Attempted Angle=%f - Arm Position not changed, please input valid coordinates!\n",servo3);
		return 0;
	}
	else if(servo4>servo_max_range4 || servo4<servo_min_range4)
	{
		printf("\nInput would cause a non-safe state!  Servo4 Range, Attempted Angle=%f - Arm Position not changed, please input valid coordinates!\n",servo4);
		return 0;
	}
	else if(servo5>servo_max_range5 || servo5<servo_min_range5)
	{
		printf("\nInput would cause a non-safe state!  Servo5 Range, Attempted Angle=%f - Arm Position not changed, please input valid coordinates!\n",servo5);
		return 0;
	}
	else
	{
		return 1;
	}
		
}

/***********************************************************
Function Name: SendOutput
Function Objective:  This function takes the new servo angles from the NewPos function and translates them into pulses which the servocontroller can read.  These pulse lengths are then writen out the serial port which results in the movement of the arm.
Arguements:  struct Angles * AngleRef, struct ArmPos * ArmRef
Return:  None
***********************************************************/
void SendOutput(struct Angles *AngleRef, struct ArmPos *ArmRef)
{
	unsigned char *servo_pos;
	float *ptr=&AngleRef->servo0;

	int c;//counting variable
	int speed=1; //default speed, to be used for servos 0-1, speed will be 2 for servos 2-5
	int fd;
	int bytes=0;
	
	//this is the actual calculation to determine the servopulse
	//servo_pulse calculates the pulse_width from the desired angle
	float servo_pulseF;//float format
	int servo_pulseI;//integer format

	
	servo_pos=malloc(6*sizeof(unsigned char));//for each of the 6 servos
	for(c=0;c<6;c++)
	{
		if(c>1)//since the different servos have longer links to move, the servos 3-5 need a faster speed to compensate
		{
			speed=2;
		}

		servo_pos[0]= (unsigned char) 0x80;
		servo_pos[1]= (unsigned char) 0x01;
		servo_pos[2]= (unsigned char) 0x01;
		servo_pos[3]= (unsigned char) c;//the servo
		servo_pos[4]= (unsigned char) speed;//the speed
		
		fd = open(COMPRT,O_RDWR | O_NOCTTY | O_NDELAY);
		if (fd == -1)
		{
		if(DEBUG==1){
			printf("couldnt write to ports!\n");
			}
		}
		else
		{
			fcntl(fd,F_SETFL,0); /* set up blocking */
			
			bytes = write(fd,servo_pos,5);
			if (bytes < 0)
			{
				if(DEBUG==1){
				printf("writting data to port failed!");
				}
			}
			
		}

		servo_pulseF=((*ptr)*EQCONST + EQOFFSET);
		servo_pulseI=(int) servo_pulseF;

		servo_pos[0]= (unsigned char) 0x80;
		servo_pos[1]= (unsigned char) 0x01;
		servo_pos[2]= (unsigned char) 0x04;
		servo_pos[3]= (unsigned char) c;//the servo number 
		servo_pos[4]= (unsigned char) (servo_pulseI>>7);
		servo_pos[5]= (unsigned char) (servo_pulseI & 0x7f);
		
		bytes = write(fd,servo_pos,6);
		if (bytes < 0)
		{
			if(DEBUG==1){
			printf("writting data to port failed!");
			}
		}
		ptr++;//increment point to point ot the next double of the struct for the next servo
	}


	if(DEBUG==1)//print debug statements
	{	//printf("\nNew Arm Coordinates as Follows\nArmTip X-Coordinate=%f\nArmTip Y-Coordinate=%f\nArmTip Z-Coordinate=%f\nArmTip Pitch=%f\nArmTip Yaw=%f\nArmTip Roll=%f\n",ArmRef->Pos_x,ArmRef->Pos_y,ArmRef->Pos_z,ArmRef->Pos_pitch,ArmRef->Pos_yaw,ArmRef->Pos_roll);
		printf("\nNew Servo Angles are As Follows\nServo0 Angle (base)=%f\nServo1 Angle (shoulder)=%f\nServo2 Angle (elbow)=%f\nServo3 Angle (pitch)=%f\nServo4 Angle (Yaw)=%f\nServo5 Angle (roll)=%f\n",AngleRef->servo0,AngleRef->servo1,AngleRef->servo2,AngleRef->servo3,AngleRef->servo4,AngleRef->servo5);
	}
	
	close(fd); /* close COM port */
	free(servo_pos);

}

/***********************************************************
Function Name: IncPos
Function Objective:  This function increments the box's position based upon its orientation.  You specify an amount to increment and it 	calculates what the new coordinates will be.  This is useful for docking procedures where you would want to move the box forward 	in its current direction
Arguements:  struct ArmPos * CurrentPos, struct ArmPos * InputRef, float inc_val
Return:  None
***********************************************************/
void IncPos(struct ArmPos * ArmRef, struct ArmPos * InputRef, float inc_val)
{
	float PitchYaw_X;
	float Pitch_Y;
	float Yaw_Z;

	//new calculations
	PitchYaw_X=(inc_val*cos(ArmRef->Pos_pitch*rad)*cos(ArmRef->Pos_yaw*rad));
	Pitch_Y=(inc_val*sin(ArmRef->Pos_pitch*rad));
	Yaw_Z=(inc_val*sin(ArmRef->Pos_yaw*rad));

	

	//update the values for the coordinate positions with the new increments
	InputRef->Pos_x=ArmRef->Pos_x + PitchYaw_X;  //this is necessary since there are two x values to contribute to the sum	

	InputRef->Pos_y=ArmRef->Pos_y + Pitch_Y;
	InputRef->Pos_z=ArmRef->Pos_z + Yaw_Z;
	InputRef->Pos_pitch=ArmRef->Pos_pitch;
	InputRef->Pos_yaw=ArmRef->Pos_yaw;
	InputRef->Pos_roll=ArmRef->Pos_roll;

}

/***********************************************************
Function Name: resolution
Function Objective:  This function splits up a movement from point a to point b into x steps defined by num_steps (above.)  It will then pass each of these incremental steps to NewPos which will execute them.  This fucntion is useful for making smoother movements when trying to go between 2 point.  I decided to develope it since the arm was a little bit jerky and not very elegant when moving between two positions.  The higher the resolution the better the movement.  If num_steps is 1 then it will make a direct movement 
Arguements:  struct ArmPos * InputRef, struct Angles * AngleRef, struct ArmPos * ArmRef
Return:  None
***********************************************************/
int resolution(struct ArmPos * InputRef, struct Angles * AngleRef, struct ArmPos * ArmRef)
{
  	float change_x, change_y, change_z, sumchange;  //used to measure the change to determine how many steps should be taken
	int aveVal=0;  //this is used to determine what the 3 changes should be divided by
		int i=1;//counting variable
	struct ArmPos StartPos;//this keeps track of the position where the arm was origionally
	struct ArmPos Increment;//struct used to store the increment value
	struct ArmPos OutputRef;//send the incremented position to the NewPos function using this struct
	int sleeptime;
	
			
	if(NewPos(InputRef,AngleRef,ArmRef,0)==0)//check to see if there could be a position that would cause it to result in a bad state before doing the function
	{
		return 0;
	}

	sleeptime=1;
	change_x=InputRef->Pos_x-ArmRef->Pos_x;
	change_y=InputRef->Pos_y-ArmRef->Pos_y;
	change_z=InputRef->Pos_z-ArmRef->Pos_z;
	
	//determine what you should divide the sumchange by, used to help get more resolution for longer motions, if change is less than 5 the don't consider it in the resolution
	if(abs(change_x)>5)
	{
		aveVal++;
	}
	if(abs(change_y)>5)
	{
		aveVal++;
	}
	if(abs(change_z)>5)
	{
		aveVal++;
	}
	if(aveVal==0)
	{
		aveVal++;//make sure that we do not divide sumchange by zero if the change is small
	}
	
	sumchange=(int)(abs(change_x)+abs(change_y)+abs(change_z))/(aveVal); //used to calculate what the resolution should be based upon the total change
	
	if(sumchange==0)
	{
		sumchange=5;//no divide by zero! choose 5 just to be safe
	}
	
	
	//find out the change in coordinate/angle positions and divide it into the number of steps specified to find the increment
	Increment.Pos_x=change_x/num_steps;
	Increment.Pos_y=change_y/num_steps;
	Increment.Pos_z=change_z/num_steps;
	Increment.Pos_pitch=(InputRef->Pos_pitch-ArmRef->Pos_pitch)/num_steps;
	Increment.Pos_yaw=(InputRef->Pos_yaw-ArmRef->Pos_yaw)/num_steps;
	Increment.Pos_roll=(InputRef->Pos_roll-ArmRef->Pos_roll)/num_steps;
	if(DEBUG==1)
	{
		printf("\nchange_x=%f\nchange_y=%f\nchange_z=%f\nsumchange=[%f]\nIncrement X=%f\nIncrement Y=%f\nIncrement Z=%f\nIncrement Pitch=%f\nIncrement Yaw=%f\nIncrement Roll=%f\n",change_x,change_y,change_z,sumchange,Increment.Pos_x,Increment.Pos_y,Increment.Pos_z,Increment.Pos_pitch,Increment.Pos_yaw,Increment.Pos_roll);
	}
	
	//initialize the StartPos struct to the origional position of the arm before this new input
	StartPos.Pos_x=ArmRef->Pos_x;
	StartPos.Pos_y=ArmRef->Pos_y;
	StartPos.Pos_z=ArmRef->Pos_z;
	StartPos.Pos_pitch=ArmRef->Pos_pitch;
	StartPos.Pos_yaw=ArmRef->Pos_yaw;
	StartPos.Pos_roll=ArmRef->Pos_roll;
	

	//dynamically determine the sleeptime (delay between steps) dependent on the number of steps
	if(num_steps<10)
	{
		sleeptime=2000000;
	}
	else if(num_steps > 10 && num_steps < 30)
	{
		sleeptime=1250000;
	}
	else if ( num_steps > 30 )
	{
		sleeptime=750000;
	}
	
	for(i=1;i<(num_steps+1);i++)
	{
		//basically this sets the new position for each step, then sends it to the function to calculate the angles and command the arm
		
		OutputRef.Pos_x=StartPos.Pos_x + i*Increment.Pos_x;
		OutputRef.Pos_y=StartPos.Pos_y + i*Increment.Pos_y;
		OutputRef.Pos_z=StartPos.Pos_z + i*Increment.Pos_z;
		OutputRef.Pos_pitch=StartPos.Pos_pitch + i*Increment.Pos_pitch;
		OutputRef.Pos_yaw=StartPos.Pos_yaw + i*Increment.Pos_yaw;
		OutputRef.Pos_roll=StartPos.Pos_roll + i*Increment.Pos_roll;
		
		if(DEBUG==1)
		{
			printf("\nOutputRef values\nPos_x=%f\nPos_y=%f\nPos_z=%f\nPos_pitch=%f\nPos_yaw=%f\nPos_roll=%f",OutputRef.Pos_x,OutputRef.Pos_y,OutputRef.Pos_z,OutputRef.Pos_pitch,OutputRef.Pos_yaw,OutputRef.Pos_roll);
		}
		
		if(NewPos(&OutputRef,AngleRef,ArmRef,1)==0)//if coordinates would result in a non-safe state break, do not increment more
		{
			return 0;  
		}

		usleep(sleeptime);//pause for a sleeptime between outputing  may need to adjust this
	}
	return 1;
}
/***********************************************************
Function Name: AutoDock
Function Objective:  This function automatically docks the arm based upon capaciflector input.  NOT COMPLETED
Arguements:  struct ArmPos * InputRef, struct Angles * AngleRef, struct ArmPos * ArmRef
Return:  Success/Failure
***********************************************************/
int autodock(struct ArmPos * InputRef,struct Angles * AngleRef, struct ArmPos * ArmRef)
{
	
	
	/***************This is the incomplete autodocking function, needs to be modified to work
	float poschange0=0;
	float poschange1=0;
	float poschange2=0;
	const float Margin_Width = 1.0; //this is the width between he box and the receptacle for each side

	struct ArmPos InputRef;
	struct ArmPos ArmRef;
	struct Angles AngleRef;

	//GetSensorValues(&CurrentSensor);
	CurrentSensor.cap_0=4;
	CurrentSensor.cap_1=4;
	CurrentSensor.cap_2=4;
	CurrentSensor.cap_3=4;
	CurrentSensor.cap_4=2;
	CurrentSensor.cap_5=2;
	CurrentSensor.cap_6=3;
	CurrentSensor.cap_7=3;
	
	//used to keep track of the change in position

	
	
	printf("\nAutoDock!");
	
	while(CurrentSensor.cap_0 > Margin_Width || CurrentSensor.cap_1 > Margin_Width || CurrentSensor.cap_2 > Margin_Width || CurrentSensor.cap_3 > Margin_Width || CurrentSensor.cap_4 > Margin_Width || CurrentSensor.cap_5 > Margin_Width || CurrentSensor.cap_6 > Margin_Width || CurrentSensor.cap_7 > Margin_Width)//loop until autodocking is complete
	{
		printf("\n\nentered loop");
		if(CurrentSensor.cap_0>5 || CurrentSensor.cap_1>5 || CurrentSensor.cap_2>5 || CurrentSensor.cap_3>5 || CurrentSensor.cap_4>5|| CurrentSensor.cap_5>5 || CurrentSensor.cap_6>5 || CurrentSensor.cap_7>5)//if distance is too large to detect
		{
			printf("\nCapaciflector ranges exceeded, cannot autodock");
			return 2;
		}
		if(CurrentSensor.cap_0>Margin_Width && CurrentSensor.cap_1>Margin_Width && CurrentSensor.cap_2>Margin_Width && CurrentSensor.cap_3>Margin_Width)//in/out, 5 is for the range of the capaciflector sensor
		{
			poschange0=((CurrentSensor.cap_0+CurrentSensor.cap_1+CurrentSensor.cap_2+CurrentSensor.cap_3)/4)/3;
			InputRef.Pos_x=CurrentPos.Pos_x + poschange0;//this takes the distance in/out of the receptacle measured by the capaciflectors, averages the 4 sensor distances, and then divides the distance into the receptacle by 3 so that we increment slowly into the receptacle.
			printf("\nIncrementing X to %f",InputRef.Pos_x);
		}
		if(CurrentSensor.cap_4>Margin_Width || CurrentSensor.cap_6>Margin_Width)//up/down
		{
			poschange1=((CurrentSensor.cap_4-CurrentSensor.cap_6)/2)/3;
			InputRef.Pos_y=CurrentPos.Pos_y + poschange1;//moves the y direction up/down
			printf("\nIncrementing Y to %f",InputRef.Pos_y);
		}
		if(CurrentSensor.cap_5>Margin_Width && CurrentSensor.cap_7>Margin_Width)//left/right
		{
			poschange2=((CurrentSensor.cap_5-CurrentSensor.cap_7)/2)/3;
			InputRef.Pos_z=CurrentPos.Pos_z + poschange2;//moves the z direction
			printf("\nIncrementing Z to %f",InputRef.Pos_z);
		}
		if(abs(CurrentSensor.cap_0-CurrentSensor.cap_2)>1 && abs(CurrentSensor.cap_1-CurrentSensor.cap_3)>1)//pitch
		{
			
			InputRef.Pos_pitch=asin((((CurrentSensor.cap_0-CurrentSensor.cap_2)+(CurrentSensor.cap_1-CurrentSensor.cap_3))/2)/17.14)*deg/3;  //this increase/decreases the pitch the atan calculation is based on the average of the difference between the upper/lower sensors, which would be the opposite side, while the 17.14 is the height of the box in cm, the /3 is to increment to the desired position in 3 times
			printf("\nIncrementing Pitch to %f",InputRef.Pos_pitch);
		}
		if(abs(CurrentSensor.cap_0-CurrentSensor.cap_1)>1 && abs(CurrentSensor.cap_2-CurrentSensor.cap_3)>1)//yaw
		{
			
			InputRef.Pos_yaw=asin((((CurrentSensor.cap_0-CurrentSensor.cap_1)+(CurrentSensor.cap_2-CurrentSensor.cap_3))/2)/17.14)*deg/3;
			printf("\nIncrementing Yaw to %f",InputRef.Pos_yaw);
		}
		if((abs(CurrentSensor.cap_4-CurrentSensor.cap_6)>1) && (abs(CurrentSensor.cap_5-CurrentSensor.cap_7)>1))//roll
		{
			
			InputRef.Pos_roll=asin((((CurrentSensor.cap_4-CurrentSensor.cap_6)+(CurrentSensor.cap_5-CurrentSensor.cap_7))/2)/17.14)*deg/3;
			printf("\nIncrementing Roll to %f",InputRef.Pos_roll);
		}

		if(NewPos(&InputRef,&AngleRef,&ArmRef,1)==0)
		{
			printf("\nDangerous Servo position");
			
			break;
		}
		CurrentSensor.cap_0-=poschange0;
		CurrentSensor.cap_1-=poschange0;
		CurrentSensor.cap_2-=poschange0;
		CurrentSensor.cap_3-=poschange0;
		CurrentSensor.cap_4-=poschange1;
		CurrentSensor.cap_5-=poschange2;
		CurrentSensor.cap_6-=poschange1;
		CurrentSensor.cap_7-=poschange2;
		
	}
	*/
	return 1;
	
}
/***********************************************************
Function Name: returnstring
Function Objective:  This function creates the returnstring for the robot, including the position and success/failure code
Arguements: int returncode, struct ArmPos * InputRef
Return:  char *, the returned string
***********************************************************/
void returnstring(int returncode, struct ArmPos * InputRef)
{
	char *feedback;  //this string is used to send feedback back to the GUI about the current position and error status
	feedback=calloc(128,sizeof(char));

	/*sprintf(feedback,"%cA%f,B%f,C%f,D%f,E%f,F%f,G%d\n",37,
		InputRef->Pos_x,
		InputRef->Pos_y,
		InputRef->Pos_z,
		InputRef->Pos_pitch,
		InputRef->Pos_yaw,
		InputRef->Pos_roll,
		returncode);
	//printf("feedback is [%s]\n",feedback);
	//return(feedback);*/

	sendtoclient(MyClient,"%d A%f,B%f,C%f,D%f,E%f,F%f,G%d\n",ARMCMD,
		InputRef->Pos_x,
		InputRef->Pos_y,
		InputRef->Pos_z,
		InputRef->Pos_pitch,
		InputRef->Pos_yaw,
		InputRef->Pos_roll,
		returncode);//return feedback string

	free(feedback);
}

