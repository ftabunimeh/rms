#include "main.h"
#include "external.h"
#include <termios.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <strings.h>
#include <sys/signal.h>
#include <sys/types.h>

#define BAUDRATE      B9600     // Baudrate at which data is transmitted
#define DATABITS      CS8       // 8-bits transmitted at one time
#define STOPBITS      CSTOPB    // Stop bit value
#define PARITYON      0         // Parity bit On/Off
#define PARITY        0         // Parity bit value

/* should use RMS.conf com port instead -- change this */
#define DEVICE "/dev/ttyS7"     // Com port number

#define buf_read      40        // Number of bytes to read from stream
#define buf_size      19        // Number of bytes in packet
#define num_sens      8         // Number of sensors
#define HALF_SECOND   0x9896    // Number defined in PIC
#define CAPSCMD       55        // Reference number for CMD module
#define FALSE         0
#define TRUE          1
#define _POSIX_SOURCE 1

int fd;
const int num_readings = 10;    // Number of sensor_readings in array
const float filter_slow=0.2;    // 1.0 for fastest filter on sensor data
const float filter_fast=0.8;    // 1.0 for fastest filter on sensor data
unsigned char *buf;             // Buffer for where data is read into
float *sensor;                  // Raw frequency reading from PIC
float *sensor_slow;             // For smoothing the readings
float *sensor_fast;             // For smoothing the readings
struct termios oldtio, newtio;  // Place for old and new serial port settings
struct sigaction saio;          // Definition of signal action

// freq,distance.  Update 'num_readings' below
// must be in order of increasing frequency and decreasing distance
const float sensor_readings[][2] = {
{    7, 5    },
{   10, 4    },
{   75, 3    },
{  300, 2    },
{  600, 1    },
{ 4000, 0.5  },
{10000, 0.25 },
{20000, 0.125},
{35000, 0.01 },
{50000, 0.001}
};

// Normalize maximum frequency deviation
const float sensor_gains[] = {
1.0,1.0,1.0,10000.0,1.0,1000.0,1000.0,1.0
};

int InitMain();
int InitSens();
int InitCOM();
int ReadCaps(RCLIENT *, char *);
int ProcessPacket();
void UpdateSensors();
float Linearize(float);
void UnLoadCaps();
void signal_handler_IO (){};
