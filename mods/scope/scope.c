#include <stdio.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h> 
#include <unistd.h> //Comment out if working on Windows
#include "main.h"
#include "external.h"

#define SCOPECMD 38


double getAltitude();
double parseLatitude( char * );
double getMeanLong( double );
double getMeanAnom( double );
double getEpsilon( double );
double getLambda( double, double );
double getRA( double, double );
double getDec( double, double );
double getLST( double, double );
double getha( double, double );
double getAlt( double, double, double );
double getAz( double, double, double, double );
double getJD();
double minutes( double );
void convertOneDigit( char *, int, short int, short int );
void convertTwoDigit( char *, int, short int, short int );
void sendToScope( char * );
double parseLongitude( char * );
int ScopeControl( RCLIENT *, char *);

char *COMPRT; /* uses RMS config file */

const double radToDeg = 360.0 / (2.0 * 3.141592654);//Constants for radian and
const double degToRad = (2.0 * 3.141592654) / 360;	//degree conversions

void register_mod(char *module_name)
{ 
   addcmd (SCOPECMD, ScopeControl, module_name);
}
void unregister_mod(char *module_name)
{
   /* say good bye */
   
   /* you might wanna add clean up functions for example restore
      arm position to a safe state
   */
}


int ScopeControl( RCLIENT* remote_client, char * argv )
{
	double julianDate, meanLong, meanAnom, lambda, epsilon, rightA, declination,
		LST, ha, altitude, azimuth, longitude, latitude, timeDiff;
	time_t start, end;
	int altDeg, azDeg, altMin, azMin, dayCount = 0;
	char * tempValue, * command, * inputString;
	int i = 0;
				//Allocate memory for the 
	COMPRT = remote_client->robotstk->teles_com; /* use COM PORT */
	
	tempValue = (char*)malloc( 4 * sizeof( char ) );
	inputString = (char*)malloc( 16 * sizeof( char ) );
			//Copy the passed in input to another string
	strncpy( inputString, argv, 16);

	//Test code to output the inputted string
	//for( i= 0; i < 16; i++ )
	//	printf("%c", inputString[i] );

			//Parse the latitude and longitude from the input string
	latitude = parseLatitude( inputString );
	//printf("Latitude: %lf", latitude );
	longitude = parseLongitude( inputString );
	//printf("\nLongitude: %lf", longitude );
	printf("\n");

	/***********
	Initialize the command string
	**********/
	command = (char*)malloc( 25 * sizeof( char ) );
	command[0] = '#';
	command[1] = ':';
	command[2] = 'S';
	command[3] = 'a';
	command[7] = '*';
	command[10] = '#';
	command[11] = ':';
	command[12] = 'S';
	command[13] = 'z';
	command[17] = '*';
	command[20] = '#';
	command[21] = ':';
	command[22] = 'M';
	command[23] = 'A';
	command[24] = '#';

		/***********
		Start the main loop.  There are 288 5-minute periods in one day.
		Set the conditional to match the number of movements you want each day
		*************/
	while( dayCount < 1 )
	{

		julianDate = getJD();	//Calculate julian date
		///Calculate necesary parameters for coordinate calculation
		meanLong = getMeanLong( julianDate );
		meanAnom = getMeanAnom( julianDate );
		epsilon = getEpsilon( julianDate );
		lambda = getLambda( meanLong, meanAnom );
		rightA = getRA( epsilon, lambda );
		declination = getDec( epsilon, lambda );
		LST = getLST( julianDate, longitude );
		ha = getha( LST, rightA );
		////
		//get the decimal altitude and elevation
		altitude = getAlt( declination, latitude, ha );
		azimuth = getAz( declination, latitude, ha, altitude );
		//Calculate alt and az in degrees, minutes
		azDeg = floor(azimuth);
		altDeg = floor(altitude);
		azMin = minutes(azimuth);
		altMin = minutes(altitude);
		//Test outputs
		//printf( "\nAltitude: %d", altDeg );
		//printf( " %d", altMin );
		//printf( "\nAzimuth: %d", azDeg );
		//printf( " %d", azMin );

		/***************************
		Command String Format
		Altitude:     :SasDD*MM#
		Azimuth:	  :SzDDD*MM#
		Move to:	  :MA#
		A # should precede all commands ie., #:SasDD*MM#:SzDDD*MM#:MA#
		Pos: 0123456789012345678901234
		****************************/
	
		//altDeg = 45;	//Set alt/az for command string testing
		//altMin = 0;
		//azDeg = 180;
		//azMin = 0;

				//Format the altitude degrees as needed for the command string
		if ( altDeg >= -9 && altDeg <= 9 ) convertOneDigit( tempValue, altDeg, 0, 0 );
		else convertTwoDigit( tempValue, altDeg, 0, 0 );
		command[4] = tempValue[0];	//Set the command values to the numbers received from the functions
		command[5] = tempValue[1];
		command[6] = tempValue[2];

				//Format the altitude minutes as needed for the command string
		if ( altMin >= -9 && altMin <= 9 ) convertOneDigit( tempValue, altMin, 0, 1 );
		else convertTwoDigit( tempValue, altMin, 0, 1 );
		command[8] = tempValue[0];	//Set the command values to the numbers recieved
		command[9] = tempValue[1];

					//Format the azimuth degrees as needed for the command string
		if ( azDeg >= -9 && azDeg <= 9 ) convertOneDigit( tempValue, azDeg, 1, 0 );
		else if ( abs( azDeg ) >= 10 && abs( azDeg ) <= 99 ) convertTwoDigit( tempValue, azDeg, 1, 0 );
		else sprintf( tempValue, "%d", azDeg );
		command[14] = tempValue[0];	//Set the command values to the numbers recieved
		command[15] = tempValue[1];
		command[16] = tempValue[2];

				//Format the azimuth minutes as needed for the command string
		if ( azMin >= -9 && azMin <= 9 ) convertOneDigit( tempValue, azMin, 0, 1 );
		else convertTwoDigit( tempValue, azMin, 0, 1 );
		command[18] = tempValue[0];		//Set the command values
		command[19] = tempValue[1];

		sendToScope( command );	//Send the command to the telescope over RS-232

		printf( "Julian Date: %lf", julianDate );
		printf( "\nAltitude (D): %d", altDeg );
		printf( "\nAltitude (M): %d", altMin );
		printf( "\nAzimuth (D): %d", azDeg );
		printf( "\nAzimuth (M): %d", azMin );
		printf( "\nCommand: " );
		for( i = 0; i < 25; i++ )
            printf( "%c", command[i] );
		printf( "\n\n\n\n" );


		/***********
			Delay the program the specified time
		**************/

		time(&start);	//Get the current time
		do
		{
			time(&end);	//Get the second time
			timeDiff = difftime( end, start );	//Compare the two times
		}
		while( timeDiff < 2 );	//Wait for timeDiff seconds.  5m = 300s

		dayCount++;		//increment the movement counter
	}

	sendtoclient(remote_client,"%d %d\n",11,SCOPECMD);

	free(command);		//delete the allocated memory for the program
	free(tempValue);
	free(inputString);

	return 0;
}
/*************
	Function: getJD()
	This function calculates and returns the current Julian Date
************/
double getJD()
{
	double year, month, day, timeS, a, y, m, julian;
	time_t now;
	struct tm *currTime;

	time( &now );	//get the current system time
	currTime = gmtime( &now );	//Convert the system time to UTC
	//Test outputs
	//printf( "\nYear: %d", currTime->tm_year + 1900 );
	//printf( "\nMonth: %d", currTime->tm_mon + 1 );
	//printf( "\nDay: %d", currTime->tm_mday );
	//printf( "\nHour: %d", currTime->tm_hour );
	//printf( "\nMinutes: %d", currTime->tm_min );
	//printf( "\nSeconds: %d", currTime->tm_sec );
	//
	//Calculate parameters need for JD calculation
	year = currTime->tm_year + 1900;
	month = currTime->tm_mon + 1;
	day = currTime->tm_mday;
	timeS = currTime->tm_hour + ( currTime->tm_min / 60.0 ) + ( currTime->tm_sec / 3600.0 );
	/********
		timeS note:  On the Linux computer used for testing, we had to add 5 to this variable.
		I am not sure if the error was due to the time settings on the Linux machine or a difference
		between the Windows and Linux OSs.
	*************/
	
	//Test Outputs
	//printf( "\nThe date is: %lf", year );
	//printf( "\nThe month is: %lf", month );
	//printf( "\nThe day is: %lf", day );
	//printf( "\nThe time is: %lf", timeS );
	//
	//calculate julian date
	a = (14.0-month)/12.0;
	y = year - a;
	m = month + 12.0 * a - 3;
		//Calculate the JD
	julian = day + floor( ((306.0 * m + 5.0) / 10.0)) + floor( y * 365.0 ) + floor(y/4.0) - 
		floor(y/100.0) + floor(y/400.0) + 1721119.0 - 2451545.0 + (timeS - 12.0)/24.0;

	//printf( "\nJulian Date: %lf", julian );	//test output

	return julian;
}

/**********
	Function:  getAz(...)
	This function calculates and returns the decimal azimuth
*************/

double getAz( double dec, double lat, double ha, double alt )
{
	double tempY, tempX, tempZ, az;
	//Calculate preliminary values needed to get azimuth
	tempY = -cos(dec*degToRad)*cos(lat*degToRad)*sin(ha*degToRad);
	tempX = sin(dec * degToRad) - ( sin(lat * degToRad) * sin(alt * degToRad) );
	tempZ = atan(tempY / tempX) * radToDeg;

	if ( tempX < 0 )
		az = tempZ + 180;
	else if ( tempY < 0 && tempX > 0 )
		az = tempZ + 360;
	else
		az = tempZ;

	//printf( "\nDecimal Azimuth: %lf" , az );//test output
	return az;
}

/***********
	Function:  getAlt(...)
	This function calculates and returns the decimal altitude
**************/

double getAlt( double dec, double lat, double ha )
{		
	double alt;
	//Calculate altitude
	alt = asin( sin(dec*degToRad) * sin(lat*degToRad) + cos(dec*degToRad) * 
		cos(lat*degToRad) * cos(ha*degToRad)) * radToDeg;

	//printf( "\nDecimal Altitude: %lf" , alt );	//test output
	return alt;
}

/***********
	Function:  getha(...)
	This function calculates a preliminary variable used in the Az/Alt calculation
**********/

double getha( double lst, double ra )
{
	double ha;
	//Calculate ha
	ha = lst - ra;
	while( ha < 0 || ha > 360 )
	{
		if ( ha < 0 )
			ha += 360;
		else
			ha -= 360;
	}

	//printf( "\nha: %lf", ha );	//test output
	return ha;
}

/****************
	Function:  getLST(...)
	This function calculates LST, a variable used in the Az/Alt equation
******************/

double getLST( double julian, double ltude )
{
	double LST;
	//Calculate LST
	LST = 280.46061837 + (360.98564736629*julian) + ltude;
	while( LST < 0 || LST > 360 )
	{
		if ( LST < 0 )
			LST += 360;
		else
			LST -= 360;
	}

	//printf( "\nLST: %lf", LST );	//test output
	return LST;
}

/***************
	Function:  getDec(..)
	This function calculates the declination which is needed in the Alt/Az equations
*****************/

double getDec( double ep, double lambda )
{
	double dec;
	//Calculate declination
	dec = asin( sin(ep*degToRad)*sin(lambda*degToRad) )*radToDeg;
	//printf( "\nDeclination: %lf", dec );	//test output
	return dec;
}

/****************
	Function:  getRA(...)
	This function calculates the Right Ascension which is used in the Alt/Az calculations
******************/

double getRA( double ep, double lambda )
{
	double tempY, tempX, tempA, rightA;
	//Calculate Right Ascension
	tempY = cos(ep*degToRad) * sin(lambda*degToRad);
	tempX = cos(lambda*degToRad);
	tempA = atan(tempY / tempX) * radToDeg;

	if (tempX < 0)
		rightA = tempA + 180;
	else if ( tempY < 0 && tempX > 0 )
		rightA = tempA + 360;
	else
		rightA = tempA;

	//printf( "\nRight Ascension: %lf", rightA );	//test output
	return rightA;
}

/****************
	Function:  getLambda
	This function calculates lambda, which is used in the Alt/Az calculations
******************/

double getLambda( double mLong, double mAnom )
{
	double tempLambda;
	//Calculate lambda
	tempLambda = mLong + (1.915*sin(mAnom*degToRad)) + (0.02*sin(2*mAnom*degToRad));
	//printf( "\nLambda : %lf", tempLambda );	//test output
	return tempLambda;
}

/************
	Function:  getEpsilon(..)
	This function calculates epsilon, a variable used in the alt/az calculations
***************/

double getEpsilon( double julian )
{
	double tempEp;
	//Calculate epsilon
	tempEp = 23.439 - (0.0000004 * julian);
	//printf( "\nEpsilon : %lf", tempEp );	//test output
	return tempEp;
}

/*************
	Function:  getMeanAnom(..)
	This function calculates the Mean Anomaly
******************/

double getMeanAnom( double julian )
{
	double tempAnom;
	//Calculate mean anomaly
	tempAnom = 357.528 + (0.9856474 * julian);
	//printf( "\nMean Anomaly: %lf", tempAnom );	//test output
	return tempAnom;
}

/******************
	Function:  getMeanLong
	This function calculates the Mean Longitude
**************************/

double getMeanLong( double julian )
{
	double tempMean;
	//Calculate mean longitude
	tempMean = ( 280.461 + (0.9856474 * julian) );
	while( tempMean < 0 || tempMean > 360 )
	{
		if ( tempMean < 0 )
			tempMean += 360;
		else
			tempMean -= 360;
	}
	//printf( "Mean Longitude: %lf", tempMean );	//test output
	return tempMean;
}

/**********
	Function:  minutes(..)
	This function converts a value to minutes.
	Example:  54.34, the 54 is subtracted out leaving .34.  Then .34 is multiplied by 60 and returned.
**************/

double minutes( double value )
{
	if ( value < 0 ) value = (-1*value);//Change to positive if negative
	while( value > 1 ) value--; //Decrease value until between 0 and 1
	return ( floor( value * 60 ) ); //Calculate and return the minutes
}

/*************
	Function:  convertOneDigit(....)
	This function takes in a one digit input and converts it to the format needed for the command string
*********************/

void convertOneDigit( char * digit, int value, short int isAz, short int isMin )
{
	sprintf( digit, "%d", value );	//Convert the digit to a character string

	if ( isAz )	//If converting an azimuth value...
	{
		digit[2] = digit[0];		//change to format 00x
		digit[1] = digit[0] = '0';	
	}
	else
	{
		if ( value < 0 )
		{
			digit[2] = digit[1];	//if neg. change to -0x
			digit[1] = '0';
		}
		else
		{
			if ( !isMin )
			{
				digit[2] = digit[0];	//if positive change to +0x
				digit[0] = '+';
				digit[1] = '0';
			}
			else
			{			//A format conversion for minutes does not include a + or - sign
				digit[1] = digit[0];
				digit[0] = '0';
			}
		}
	}
}

/*************
	Function:  convertTwoDigit
	This function converts a number over 9 or under -9 to the format needed for the command string
************************/

void convertTwoDigit(  char * digit, int value, short int isAz, short int isMin )
{
	sprintf( digit, "%d", value );	//Convert the number to a character string
	
	if ( isAz )	//If an azimuth value...
	{		//convert xx to 0xx
		digit[2] = digit[1];
		digit[1] = digit[0];
		digit[0] = '0';
	}
	else
	{			//If an altitude value...
		if ( value >= 0 )
		{
			if ( !isMin )	//Leave the string the same if it's for a minutes conversion
			{
				digit[2] = digit[1];
				digit[1] = digit[0];
				digit[0] = '+';
			}
		}
	}
}

/*********************
	Function:  sendToScope
	This function send a command string to the telescope
*************************************/

void sendToScope( char * command )
{
	//The computer must send unsigned char over the serial connection
	unsigned char * uCommand;
	int i = 0;
	uCommand = (unsigned char*)malloc( 25 * sizeof( unsigned char* ) );
		//Convert the command to unsigned char format
	for( i = 0; i < 25; i++ )
	{
		uCommand[i] = (unsigned char) command[i];
	}
	//Test output
	//printf("\n");
	//for( i = 0; i < 25; i++ )
	//{
	//	printf( "%c", uCommand[i] );
	//}
	//printf("\n");

	
	//This section of code should be commented if working on a Windows system
	//This code sends the command over the serial port.
	
	int fd;
	int bytes = 0;

	fd = open (COMPRT, O_RDWR | O_NOCTTY | O_NDELAY );
	if ( fd == -1 )
		printf( "Couldn't write to ports!\n" );
	else
	{
		fcntl( fd, F_SETFL, 0 );
		bytes = write( fd, uCommand, 25 );
		if ( bytes < 0 )
			printf( "Writing data failed!" );
		else
			printf( "Success!" );
	}

	close(fd);
	
	/////////////////////////////////////
}

/*******************
	Function:  parseLatitude(..)
	This function parses the latitude from the input string
**************************/

double parseLatitude( char * input )
{
	int i = 0;
	double lat;
	char * tempLat;
	tempLat = (char*)malloc( sizeof( char ) * 7 );
			//Save the part of the string needed for conversion
	for( i= 0 ; i < 6; i++ )
	{
		tempLat[i] = input[i+1];
	}

	sscanf( tempLat, "%lf", &lat );	//Convert to a double

	free(tempLat);
	return lat;
}

/**********************
	Function:  parseLongitude
	This function parses the longitude from the string input
******************************/

double parseLongitude( char * input )
{
	char * tempLong;
	int i = 0, j = 0, comma;
	double longitude;
	tempLong = (char*)malloc( sizeof(char) * 7 );
	comma = strcspn( input, "," );	//Find the comma in the input string
	i = comma+1;		//set i to the spot after the comma

	while ( j < 7 )
	{			//Parse the input
		switch ( input[i] )
		{
		case ',':
			i++;
			break;
		case 'B':
			i++;
			break;
		default:
			tempLong[j] = input[i];	//add the necessary part of the string to the temp. longitude string
			j++;
			i++;
        }
	}

	sscanf( tempLong, "%lf", &longitude );	//Convert the string longitude to a double
	
	free(tempLong);
	return longitude;
}
