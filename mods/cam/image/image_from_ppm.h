/***********************************************************/
/* Pour cree une image a partir d'un fichier PPM           */
/* image_from_ppm.h                                        */
/*                                                         */
/* Ecrit par : Daniel Lacroix (all rights reserved)        */
/*                                                         */
/***********************************************************/

#ifndef __IMAGE_FROM_PPM_H__
#define __IMAGE_FROM_PPM_H__

#include "image.h"

#define MAX_PPM_FILE_SIZE 10000000

/* Renvoi l'image ou NULL si impossible */
image *image_new_from_ppm(char *file_name);

/* Sauvegarde l'image pimage dans le fichier file     */
/* dans le format PPM. Return -1 = si ERREUR, 0 sinon */
int image_save_to_ppm(image *pimage, char *file);

#endif /* __IMAGE_FROM_PPM_H__ */
