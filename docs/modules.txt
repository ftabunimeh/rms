### HOWTO create new modules
1- since each module is compiled independently then each module will have its
   own Makefile. if you do not want to start from scratch. go to mods/example/ 
   then copy Makefile to your new module directory. Now change variable MOD in 
   the Makefile to something else. if you need to include mode libraries please
   do so.
2- when RMS loads any module it will call a function called register_mod()
   this function has to be included in your code. Usually in this function the
   module will let RMS know what kind of commands it has. a module can add new
   commands by calling addcmd(). This function needs accepts three arguments.
   A- Command numeric: which must be previously defined in CMDs config file.
   B- The second: A Call back function that will be called when the command 
      is requested. (A pointer to function).
   C- The third: is the module name.
3- When you add a new command. The call back function has to be in the following
   format. int example1(RCLIENT *, char *);
   First arugment is the socket struct. This can be used so that the module can
   call sendtoclient() to return its results. Also it can be used in many other
   ways. you can predict by looking at the declaration of struct sckstrct in 
   main.h. For example you can look at fd or buf to check for valid commands.
4- Finally when RMS unloads a module it will call unregister_mod() to clean up.
5- If you need an example to start with please take a look at mods/example/
   It is highly recommended that you take a look at the example along with its
   Makefile. example.c contains many info that is not discussed here.
6- Modules will have full access to other RMS functions or even other modules.
   if you need access to RMS robot struct then you need to use 
   remote_client->robotstk->
   if you want access to other modules then you should use runcmd()
7- After compiling your module, it is recommended to copy the result .so file
   to so/ directory.

-Faisal