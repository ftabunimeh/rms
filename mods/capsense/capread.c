/*******************************************************************************
Copyright (c) 2004, MSU ECE480 F04 Team01, Wissam Geadaa
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list
of conditions and the following disclaimer. Redistributions in binary form must
reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the
distribution. Neither the name of the Michigan State University nor the names
of its contributors may be used to endorse or promote products derived from this
software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************/

#include "capread.h"

/***** Load Module *****/
void register_mod(char *caps)
{
  int call;

  // Calls the main initializing function
  call = InitMain();

  // Registers the read capaciflector function to the RMS
  // upon successful initialization
  if (call == TRUE)
    addcmd(CAPSCMD, ReadCaps, caps);
  else
    logit("Caps Module ERROR: Main initializer failed see log file for details.");
}

/***** Unload Module *****/
void unregister_mod(char *module_name)
{
  // Frees all allocated memory and returns ports to initial position
  UnLoadCaps();
}

/*******************************************************************************
   Function: Initializes buffers and serial COM port
   Inputs: VOID
   Outputs: calls InitSens() and InitCOM()
   Returns: TRUE/FALSE
*******************************************************************************/
int InitMain()
{
  int init;

  init = InitSens();
  if (init == FALSE)
    {
      logit("Caps Module ERROR: There is not enough memory to allocate!");
      return FALSE;
    }

  init = InitCOM();
  if (init == FALSE)
    {
      logit("Caps Module ERROR: Cannot load %s",DEVICE);
      return FALSE;
    }
  return TRUE;
}

/*******************************************************************************
   Function: Initializes buffers
   Inputs: VOID
   Outputs: VOID
   Returns: TRUE/FALSE
*******************************************************************************/
int InitSens()
{
  int loop;

  // Allocate memory for buf
  buf = malloc(buf_size * sizeof(unsigned char *));
  if (buf == NULL)
    return FALSE;
  bzero(buf, strlen(buf));

  // Allocate memory for sensor
  sensor = malloc(num_sens * sizeof(float *));
  if (sensor == NULL)
    return FALSE;

  // Allocate memory for sensor_slow
  sensor_slow = malloc(num_sens * sizeof(float *));
  if (sensor_slow == NULL)
    return FALSE;

  // Allocate memory for sensor_fast
  sensor_fast = malloc(num_sens * sizeof(float *));
  if (sensor_fast == NULL)
    return FALSE;

  for (loop = 0; loop < 8; loop++)
  {
    sensor[loop] = 10000 + loop;
    sensor_slow[loop] = 0.0;
    sensor_fast[loop] = 1000.0 + loop;
  }
  return TRUE;
}

/*******************************************************************************
   Function: Initializes serial COM port
   Inputs: VOID
   Outputs: Opens COM port and sets signal handler
   Returns: TRUE/FALSE
*******************************************************************************/
int InitCOM()
{
  // Open the COM Port to be non-blocking (read will return immediately)
  fd = open(DEVICE, O_RDWR | O_NOCTTY | O_NONBLOCK);
  if (fd < 0)
    return FALSE;

  //install the serial handler before making the device asynchronous
  saio.sa_handler = signal_handler_IO;
  sigemptyset(&saio.sa_mask);
  saio.sa_flags = 0;
  saio.sa_restorer = NULL;
  sigaction(SIGIO,&saio,NULL);

  // allow the process to receive SIGIO
  fcntl(fd, F_SETOWN, getpid());
  // Make the file descriptor asynchronous (the manual page says only
  // O_APPEND and O_NONBLOCK, will work with F_SETFL...)
  fcntl(fd, F_SETFL, FASYNC);

  tcgetattr(fd,&oldtio); // save current port settings 
  // set new port settings for canonical input processing 
  newtio.c_cflag = BAUDRATE | CRTSCTS | DATABITS | STOPBITS | PARITYON | PARITY | CLOCAL | CREAD;
  newtio.c_iflag = IGNPAR;
  newtio.c_oflag = 0;
  newtio.c_lflag = 0;       //ICANON;
  newtio.c_cc[VMIN]=1;
  newtio.c_cc[VTIME]=0;
  tcflush(fd, TCIFLUSH);
  tcsetattr(fd,TCSANOW,&newtio);

  return TRUE;
}

/*******************************************************************************
   Function: Reads serial port and collects data from PIC and 
   Inputs: Called from client with one argument 'F' to return frequency readings
   Outputs: Calls ProcessPacket(), UpdateSensors(), and Linearize()
   Returns: Frequency or distances of the eight capaciflectors
*******************************************************************************/
int ReadCaps(RCLIENT *remote_client, char *args)
{
  char temp[1];
  int flag = 1;
  int res, chk;
  int i = 0;
  int index = 0;

  // Read serial port one byte at a time and store into buf
  // untill the correct readings have been taken
  flag = 1;
  while (flag)
  {
    index = 0;
    for (i = 0; i < buf_read; i++)
    {
      res = read(fd, temp, 1);
      if (res > 0)
      {
        // Checks for correct input sequence
        if ((index == 0 && temp[0] == 'U') ||
            (index == 1 && temp[0] == 'U') || index > 0)
        {
          if (index < buf_size)
          {
            // Stores byte into buffer array
            buf[index] = temp[0];
            index++;
          }
        }
        // discard until 'UU' found
        else
          index = 0;
      }
    }
    // Once packet is collected, exit while loop
    if (index == buf_size)
      flag = 0;
  }

  if (index >= buf_size)
  {
    chk = ProcessPacket();
    if (chk == TRUE)
    {
      if (args[0] == 'F')
        sendtoclient(remote_client, "%d A%f,B%f,C%f,D%f,E%f,F%f,G%f,H%f\n",CAPSCMD,
                     sensor[0],sensor[1],sensor[2],sensor[3],
                     sensor[4],sensor[5],sensor[6],sensor[7]);
      else
      {
        UpdateSensors();
        sendtoclient(remote_client, "%d A%f,B%f,C%f,D%f,E%f,F%f,G%f,H%f\n",CAPSCMD,
                     sensor[0],sensor[1],sensor[2],sensor[3],
                     sensor[4],sensor[5],sensor[6],sensor[7]);
      }
    }
    else
      sendtoclient(remote_client, "BADPACKET\n");
  }
  return 1;
}

/*******************************************************************************
   Function: Takes raw readings from buffer array and calculates frequencies
   Inputs: VOID
   Outputs: VOID
   Returns: TRUE/FALSE
   Source: Edward Cheung
*******************************************************************************/
int ProcessPacket()
{
  int loop;
  unsigned char checksum;
  float adjust;  // scales reading to cycles per second
  checksum = 0;
  adjust = 4E7 / (32 * 128.0 * (HALF_SECOND / 5));

  for (loop = 0; loop < (buf_size - 1); loop++)
    checksum += buf[loop];

  if (checksum == buf[buf_size - 1])
  {
    // Good packet
    sensor[0] = adjust * (buf[2]  << 8 | buf[3]);
    sensor[1] = adjust * (buf[4]  << 8 | buf[5]);
    sensor[2] = adjust * (buf[6]  << 8 | buf[7]);
    sensor[3] = adjust * (buf[8]  << 8 | buf[9]);
    sensor[4] = adjust * (buf[10] << 8 | buf[11]);
    sensor[5] = adjust * (buf[12] << 8 | buf[13]);
    sensor[6] = adjust * (buf[14] << 8 | buf[15]);
    sensor[7] = adjust * (buf[16] << 8 | buf[17]);
  }
  else
    return FALSE;

  return TRUE;
}

/*******************************************************************************
   Function: Updates distances of sensors from frequencies
   Inputs: VOID
   Outputs: VOID
   Returns: VOID
*******************************************************************************/
void UpdateSensors()
{
  int loop;

  for (loop = 0; loop < num_sens; loop++)
  {
    sensor_slow[loop] = filter_slow * sensor[loop] + (1.0 - filter_slow) * (sensor_slow[loop]);
    sensor_fast[loop] = filter_fast * sensor[loop] + (1.0 - filter_fast) * (sensor_fast[loop]);
    //sensor[loop] = Linearize((sensor_slow[loop] - sensor_fast[loop]) * sensor_gains[loop]);
    sensor[loop] = Linearize(sensor[loop] * sensor_gains[loop]);
  }
}

/*******************************************************************************
   Function: Linearizes each frequency reading into distances
   Inputs: Frequency reading
   Outputs: VOID
   Returns: Distance in centimeters
   Source: Edward Cheung
*******************************************************************************/
float Linearize(float reading)
// uses 'sensor_readings' to perform piecewise linearization on raw reading
// the higher the input reading, the closer the range
{
  float distance;
  // Piecewise linear segment
  int loop;
  float inchperhertz;
  if (reading <= sensor_readings[0][0])
    return 20.0;  // beyond range
  else
  {
    loop = 0;
    while (sensor_readings[loop][0] < reading && loop < (num_readings))
      loop++;
    // loop now points to entry in calibration array that is one higher in reading
    inchperhertz = (sensor_readings[loop][1] - sensor_readings[loop - 1][1]) /
                   (sensor_readings[loop][0] - sensor_readings[loop - 1][0]);
    distance = inchperhertz * (reading - sensor_readings[loop - 1][0]) + sensor_readings[loop - 1][1];
  }

  return distance;
}

/*******************************************************************************
   Function: Frees memory and restores COM port
   Inputs: VOID
   Outputs: VOID
   Returns: VOID
*******************************************************************************/
void UnLoadCaps()
{
  // Free all buffers
  free(buf);
  free(sensor);
  free(sensor_slow);
  free(sensor_fast);
  // Restore old port settings
  tcsetattr(fd,TCSANOW,&oldtio);
  // Close COM Port
  close(fd);
}
