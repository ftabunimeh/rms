/*******************************************************************/
/* Bibliotheque de manipulation d'image au format TrueColor 32bitd */
/* image.c                                                         */
/*                                                                 */
/* Ecrit par : Daniel Lacroix (all rights reserved)                */
/*                                                                 */
/*******************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "image.h"

/************************************/
/* renvoi les dimensions de l'image */
void image_size(image *pimage, int32 *width, int32 *height)
{
  *width  = pimage->width;
  *height = pimage->height;
}
/************************************/

/***************************/
/* cree une nouvelle image */
image* image_new(int32 width, int32 height)
{ image *vimage;

  if((vimage=(image *)malloc(sizeof(image))) == NULL)
  {
    perror("malloc failed ");
    exit(1);
  }
  vimage->width = width; vimage->height = height;
  
  if((vimage->buf=(pix *)malloc(width*height*sizeof(pix))) == NULL)
  {
    perror("malloc failed ");
    exit(1);
  }
  return(vimage);
}
/***************************/

/********************/
/* libere une image */
void image_free(image *pimage)
{
  /* libere la buffer qui contient les points de l'image */
  free(pimage->buf);
  /* libere la structure qui definie l'image */
  free(pimage);
}
/********************/

/*************************************************/
/* copy l'image psrc dans pdest, les deux images */
/* doivent avoir la meme taille                  */
void image_copy(image *pdest, image *psrc)
{
  memcpy(pdest->buf, psrc->buf, (psrc->width*psrc->height)<<2);
}
/*************************************************/

/****************************************************/
/* (x,y) = position dans pdest ou est colle psrc    */
void image_paste(image *pdest, image *psrc, int32 x, int32 y)
{
  pix    *src_ptr,*dst_ptr;
  int32  line_size, x1;
  int32  height_size;

  /* place le pointeur src_ptr a l'adresse du premier    */
  /* point qui sera recopie. le calcul prend en compte   */
  /* les debordements d'images.                          */
  src_ptr = psrc->buf + ((x>=0)?0:-x) + (((y>=0)?0:-y)*psrc->width);
  /* place le pointeur src_ptr a l'adresse du premier    */
  /* point qui sera ecrase. le calcul prend en compte    */
  /* les debordements d'images.                          */
  dst_ptr = pdest->buf + ((x>=0)?x:0) + (((y>=0)?y:0)*pdest->width);
  /* calcul du nombre de points qui seront effectivement */
  /* copie par ligne. prend en compte les debordements   */
  line_size = ((pdest->width-x<psrc->width)?pdest->width-x:psrc->width)
    - ((x>=0)?0:-x);
  /* calcul du nombre de lignes qui seront effectivement */
  /* copie. prend en compte les debordements             */  
  height_size = ((pdest->height-y<psrc->height)?pdest->height-y:psrc->height)
    + ((y>=0)?0:y);

  /* on parcours toutes les lignes a copier */
  for(;height_size>0; height_size--)
  {
    /* on parcours tous les points d'une ligne */
    for(x1=0;x1<line_size; x1++)
    {
      /* fait la copie du point en cours */
      *dst_ptr = *src_ptr;
      /* deplace les pointeurs pour      */
      /* passer au point suivant.        */
      src_ptr++; dst_ptr++;
    }
    /* deplace les pointeurs pour */
    /* passer a la ligne suivante */
    src_ptr += psrc->width  - line_size;
    dst_ptr += pdest->width - line_size;
  }
}
/****************************************************/

/****************************************************/
/* (x,y) = position dans pdest ou est incruste psrc */
/* et col la couleur d'incruste de psrc             */
void image_incruste(image *pdest, image *psrc, int32 x, int32 y, pix col)
{
  int32  x1,y1;
  pix    *src_ptr,*dst_ptr;
  uint32 line_size;

  src_ptr = psrc->buf + ((x>=0)?0:-x) + (((y>=0)?0:-y)*psrc->width);
  dst_ptr = pdest->buf + ((x>=0)?x:0) + (((y>=0)?y:0)*pdest->width);
  line_size = ((pdest->width-x<psrc->width)?pdest->width-x:psrc->width)
    - ((x>=0)?0:-x);

  for(y1=(y>=0)?0:-y;
    y1<((pdest->height-y<psrc->height)?pdest->height-y:psrc->height); y1++)
  {
    for(x1=(x>=0)?0:-x;
      x1<((pdest->width-x<psrc->width)?pdest->width-x:psrc->width); x1++)
    {
      /* si la couleur du point de l'image a incruster est          */
      /* differente de la couleur d'incruste alors on fait la copie */
      if(*src_ptr != col) *dst_ptr = *src_ptr;
      src_ptr++; dst_ptr++;
    }
    src_ptr += psrc->width  - line_size;
    dst_ptr += pdest->width - line_size;
  }
}
/****************************************************/

/****************************************************/
/* (x,y) = position dans pdest ou est incruste psrc */
/* et col la couleur d'incruste de psrc. rapport l' */
/* intensite de psrc sur 256 par rapport a pdest    */
void image_transparent(image *pdest, image *psrc, int32 x, int32 y, pix col,
  uint16 rapport)
{
  int32  x1,y1;
  pix    *src_ptr,*dst_ptr;
  uint32 line_size;

  src_ptr = psrc->buf + ((x>=0)?0:-x) + (((y>=0)?0:-y)*psrc->width);
  dst_ptr = pdest->buf + ((x>=0)?x:0) + (((y>=0)?y:0)*pdest->width);
  line_size = ((pdest->width-x<psrc->width)?pdest->width-x:psrc->width)
    - ((x>=0)?0:-x);
  
  for(y1=(y>=0)?0:-y;
    y1<((pdest->height-y<psrc->height)?pdest->height-y:psrc->height); y1++)
  {
    for(x1=(x>=0)?0:-x;
      x1<((pdest->width-x<psrc->width)?pdest->width-x:psrc->width); x1++)
    {
      if(*src_ptr != col)
      {
        *dst_ptr = COL(
          ((COL_RED(*dst_ptr)*rapport)+(COL_RED(*src_ptr)*(256-rapport)))>>8,
          ((COL_GREEN(*dst_ptr)*rapport)+(COL_GREEN(*src_ptr)*(256-rapport)))>>8,
          ((COL_BLUE(*dst_ptr)*rapport)+(COL_BLUE(*src_ptr)*(256-rapport)))>>8
        );
      }
      src_ptr++; dst_ptr++;
    }
    src_ptr += psrc->width  - line_size;
    dst_ptr += pdest->width - line_size;
  }
}
/****************************************************/

/*****************************************************/
/* converti l'image psrc en noir et blanc dans pdest */
void image_gris(image *pdest, image *psrc, int32 x, int32 y)
{ uint32 luminance;
  int32  x1,y1;
  pix    *src_ptr,*dst_ptr;
  uint32 line_size;

  src_ptr = psrc->buf + ((x>=0)?0:-x) + (((y>=0)?0:-y)*psrc->width);
  dst_ptr = pdest->buf + ((x>=0)?x:0) + (((y>=0)?y:0)*pdest->width);
  line_size = ((pdest->width-x<psrc->width)?pdest->width-x:psrc->width)
    - ((x>=0)?0:-x);
  
  for(y1=(y>=0)?0:-y;
    y1<((pdest->height-y<psrc->height)?pdest->height-y:psrc->height); y1++)
  {
    for(x1=(x>=0)?0:-x;
      x1<((pdest->width-x<psrc->width)?pdest->width-x:psrc->width); x1++)
    {
      luminance = (( 76*(uint16)COL_RED(*src_ptr))+
 	      (150*(uint16)COL_GREEN(*src_ptr))+
	      ( 30*(uint16)COL_BLUE(*src_ptr)))>>8;
		  
      *dst_ptr = COL((uint8)luminance,(uint8)luminance,(uint8)luminance);

      src_ptr++; dst_ptr++;
    }
    src_ptr += psrc->width  - line_size;
    dst_ptr += pdest->width - line_size;
  }
}
/*****************************************************/

/****************************************************/
/* (x,y) = position dans pdest ou est colle psrc    */
void image_paste_with_alpha(image *pdest, image *psrc, int32 x, int32 y)
{
  int32  x1;
  pix    *src_ptr,*dst_ptr;
  int32  line_size;
  int32  height_size;
  uint32 a, inv_a;

  src_ptr = psrc->buf + ((x>=0)?0:-x) + (((y>=0)?0:-y)*psrc->width);
  dst_ptr = pdest->buf + ((x>=0)?x:0) + (((y>=0)?y:0)*pdest->width);
  line_size = ((pdest->width-x<psrc->width)?pdest->width-x:psrc->width)
    + ((x>=0)?0:x);
  height_size = ((pdest->height-y<psrc->height)?pdest->height-y:psrc->height)
    + ((y>=0)?0:y);
  
  for(;height_size>0;height_size--)
  {
    for(x1=0; x1<line_size; x1++)
    {
      /* on recupere la valeur du canal alpha de l'image */
      /* source au point en cours de traitement          */
      a = COL_ALPHA(*src_ptr);
      if(a == 255) {
        /* on fait une copie integrale de la couleur source     */
        /* sur l'image destination mais en conservant la valeur */
        /* du canal alpha de l'image destination.               */
        *dst_ptr = (*dst_ptr & 0xFF) | (*src_ptr & 0xFFFFFF00);
      } else if(a != 0) {
        inv_a = 255 - a;
        /* la couleur du point resultat = ((dst*(255-a))+(src*a))/255     */
        /* avec a pouvant prendre les valeurs entre 0 et 255              */
        *dst_ptr = COL_FULL(
          ((COL_RED  (*dst_ptr) * inv_a) + (COL_RED  (*src_ptr) * a)) / 255,
          ((COL_GREEN(*dst_ptr) * inv_a) + (COL_GREEN(*src_ptr) * a)) / 255,
          ((COL_BLUE (*dst_ptr) * inv_a) + (COL_BLUE (*src_ptr) * a)) / 255,
          COL_ALPHA(*dst_ptr)
        );
      }
      src_ptr++; dst_ptr++;
    }
    src_ptr += psrc->width  - line_size;
    dst_ptr += pdest->width - line_size;
  }
}
/****************************************************/

/***************************************************************/
/* (x,y) = position dans pdest ou psrc est utilise comme alpha */
void image_set_alpha_with_grey(image *pdest, image *psrc, int32 x, int32 y)
{
  int32  x1,y1;
  pix    *src_ptr,*dst_ptr;
  uint32 line_size;

  src_ptr = psrc->buf + ((x>=0)?0:-x) + (((y>=0)?0:-y)*psrc->width);
  dst_ptr = pdest->buf + ((x>=0)?x:0) + (((y>=0)?y:0)*pdest->width);
  line_size = ((pdest->width-x<psrc->width)?pdest->width-x:psrc->width)
    - ((x>=0)?0:-x);
  
  for(y1=(y>=0)?0:-y;
    y1<((pdest->height-y<psrc->height)?pdest->height-y:psrc->height); y1++)
  {
    for(x1=(x>=0)?0:-x;
      x1<((pdest->width-x<psrc->width)?pdest->width-x:psrc->width); x1++)
    {
      /* on utilise le rouge de l'image source comme */
      /* canal alpha de l'image destination          */
      *dst_ptr = (*dst_ptr & 0xFFFFFF00) | (*src_ptr >> 24);
      src_ptr++; dst_ptr++;
    }
    src_ptr += psrc->width  - line_size;
    dst_ptr += pdest->width - line_size;
  }
}
/***************************************************************/
