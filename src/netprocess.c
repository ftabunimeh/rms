/*******************************************************************************
Copyright (c) 2004, MSU ECE480 F04 Team03, Steve Elgas
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list
of conditions and the following disclaimer. Redistributions in binary form must
reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the
distribution. Neither the name of the Michigan State University nor the names
of its contributors may be used to endorse or promote products derived from this
software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************/

/*
  Changes:
  12/06/04 [Faisal]: fixed args = malloc(sizeof(char*)*MIDBUF);
                     to malloc(sizeof(char)*MIDBUF);
  12/08/04 [Faisal]: Completely rewrote processinput();
*/

#include "main.h"
#include "external.h"

void processinput (RCLIENT*, char*);


/*******************************************************************************
Function: processes the received buffer for commands and vars,
attempts to run the command (if found)
Inputs: Remote client, received buffer
Outputs: -
Returns: VOID
*******************************************************************************/

void processinput (RCLIENT* client, char* buffer)
{
   int tmp;
   char *tok;
   
   rmchar(buffer,'\n'); rmchar(buffer,'\r'); /* clean out buffer */
	      
   tmp = strlen(buffer);

   if (tmp < 1)
   {
      sendtoclient(client, "%d\n",10);
      logit ("[receive] Invalid data format from remote client");
      return;
   }
   
   tmp = charcount(buffer,' ');
   
   /* received: command with arguments */
   if (tmp > 0)
   {
      tok = gettok(buffer, ' ', 1);
      tmp = (int)strtol(tok,NULL,10); /* base 10 */
      tok = gettok(buffer, ' ', 2);
   }
   
   /* received: command only */
   else
   {
      tmp = (int)strtol(buffer,NULL,10); /* base 10 */
      tok = "";
   }
     
   runcmd(client, tmp, tok); /* runcmd() will handle all errors */
}


