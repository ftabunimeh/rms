/*******************************************************************************
Copyright (c) 2004, MSU ECE480 F04 Team01, Faisal Abu-Nimeh
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list
of conditions and the following disclaimer. Redistributions in binary form must
reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the
distribution. Neither the name of the Michigan State University nor the names
of its contributors may be used to endorse or promote products derived from this
software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************/

/*
  Changes:
  10/28/04 [Faisal]: Added unloadallmods() and curloadedmods()
  10/31/04 [Faisal]: TODO: Optimize modules by using dlinfo() instead of pasing
                     modname to each function below. Also that might save us
                     from creating a linked list for loaded modules?
  10/31/04 [Faisal]: deleting commands associated with a module no longer depend
                     on unregister_mod(). Now delmodcmds() will be called when
                     unloading a module. This will force a clean module exit.
  11/14/04 [Faisal]: - Removed mod_head syncing with robot struct.
                     - Added more error handling to dlopen() and dlsym()
                       somtimes dlopen() passes an invalid pointer to dlsym()
                       which causes the program to seg faults
  11/15/04 [Faisal]: Added logging to unloadallmods().
*/

#include "main.h"
#include "external.h"

#define REGMOD "register_mod" /* name of function to call when starting a mod */
#define UREGMOD "unregister_mod" /* ... when stopping a mod */

int addmodlist(struct bot *,char *, void *);
void delmodlist(struct bot *,char *);
int loadmod(struct bot *,char *);
int unloadmod(struct bot *,char *);
MODULE *findmod(char *);
void unloadallmods(struct bot *);

/* we need to sync main struct with mod_head -- UGLY */
static MODULE *mod_head = NULL;
static MODULE *mod_tail = NULL;
static MODULE *mod_curr = NULL;

/* 
   List goes from top to buttom ( head to tail )
   next will be pointing downward
   prev will be pointing upward
   Head's prev pointer is always NULL
   Tails next pointer is always NULL
*/

/*******************************************************************************
   Function: addmodlist adds a module to the global link list
   Inputs: name of module and its handle
   Outputs: VOID
   Returns: -1 if failed, 1 if succeeded 
*******************************************************************************/
int addmodlist(struct bot *robo,char *name, void *handle)
{
   mod_curr = (MODULE *)malloc(sizeof(MODULE));
	if (mod_curr == NULL)
	{
		logit("ERROR: Not enough memory %s",name);
		return -1;
	}
	
	mod_curr->modname = name; /* This pointer should be previously allocated */
	mod_curr->handle = handle; /* Will be used later with sym and close */
	
	mod_curr->next = mod_head;
	mod_curr->prev = NULL;
	
	if (mod_tail == NULL)
	{ mod_tail = mod_curr; }
	
	else
	{ mod_curr->next->prev = mod_curr; }
	
	mod_head = mod_curr;
	return 1;
}

/*******************************************************************************
   Function: delmodlist deletes a module from the global linked list
   Inputs: name of module
   Outputs: VOID
   Returns: VOID
*******************************************************************************/
void delmodlist(struct bot *robo,char *name)
{
   MODULE *mod_tmp;
   mod_tmp = (MODULE *)findmod(name); /* casting to stop gcc warning */
   
   if(mod_tmp != NULL) /* found */
   {
      /* Make sure its not a head or a tail */
      if( (mod_tmp->next != NULL) && (mod_tmp->prev != NULL) )
      {
         mod_tmp->prev->next = mod_tmp->next;
         mod_tmp->next->prev = mod_tmp->prev;
      }
      
      /* either head or tail or both */
      else
      {
         /* Both head and tail == one element in list */
         
         if( (mod_tmp == mod_head) && (mod_tmp == mod_tail) )
         {
            mod_head = NULL;
            mod_tail = NULL;
         }
         
         /* either one */
         else
         {
            /* head only */
            if(mod_tmp == mod_head)
            {
               mod_tmp->next->prev = NULL;
               mod_head = mod_tmp->next;
            }
            
            /* tail only */
            else if(mod_tmp == mod_tail)
            {
               mod_tmp->prev->next = NULL;
               mod_tail = mod_tmp->prev;
            }
            
            /* else */
            /* WTF ? */
         }
      }
      free(mod_tmp);
   }
}

/*******************************************************************************
   Function: loadmod will load a shared object into memory using dynamic linker
   Inputs: name of module (path included)
   Outputs: calls the register function in module to initialize it
            calls addmodlist to manage modules in a linked list
   Returns: -2 if duplicated ,-1 if failed, 1 if succeeded 
*******************************************************************************/
int loadmod(struct bot *robo,char *modname)
{
   void *handle;
   int (*func)();
   int tmp;

   if(NULL == findmod(modname)) /* make sure its not duplicated */
   {
      logit("Trying to load [%s] Module.",modname);
      handle = dlopen(modname, RTLD_NOW);
      if (NULL == handle)
      {
         logit("ERROR: loading [%s] received %s.",modname,dlerror());
         return -1;
      }
      /* register module */
      func=dlsym(handle, REGMOD);
      if (NULL == func)
      {
         logit("ERROR: loading functions in [%s] failed!",modname);
         return -1;
      }
      
      tmp=func(modname); /* call register function */
      
      if(tmp < 0)
      {
         logit("ERROR: Unable to register [%s].",modname);
         return -1;
      }
      
      if( 1 == addmodlist(robo,modname,handle))
      {
         logit("[%s] Module is now registered.",modname);
      }
      
      else
      {
         logit("ERROR: Unable to add [%s] Module to list.",modname);
         return -1;
      }
      
      return 1; /* succeeded */
   }
   /* duplicated */
   logit("ERROR: [%s] Module is already registered.",modname);
   return -2;    
}

/*******************************************************************************
   Function: unloadmod will unload a shared object from memory
   Inputs: name of module
   Outputs: calls the unregister function in module to remove
            calls delmodlist to remove from linked list
   Returns: -2 if not found ,-1 if failed, 1 if succeeded
*******************************************************************************/
int unloadmod(struct bot *robo,char *modname)
{
   int (*func)();
   MODULE *mod_tmp;
   int tmp;
   
   mod_tmp = (MODULE *)findmod(modname);
   
   if(mod_tmp != NULL) /* make sure its loaded */
   {
      delmodcmds(modname); /* delete all commands linked with this module */
      
      func=dlsym(mod_tmp->handle, UREGMOD);
      tmp=func(modname); /* call unregister function */
      
      if(tmp < 0)
      {
         logit("ERROR: Unable to unregister [%s].",modname);
         return -1;
      }
      
      delmodlist(robo,modname);
      if(-1 == dlclose(mod_tmp->handle))
      {
         logit("ERROR: Unable to close [%s].",modname);
         return -1;
      }
      
      logit("Module [%s] is now unloaded.",modname);
      return 1;
   }
   
   logit("ERROR: [%s] Module is not loaded/registered",modname);
   return -2;
}

/*******************************************************************************
   Function: findmod will look up in linked list for modname
   Inputs: name of module
   Outputs: VOID
   Returns: pointer to element in list if found
            NULL pointer if not found
*******************************************************************************/
MODULE *findmod(char *modname)
{
   MODULE *mod_tmp = mod_head;
   
   while(mod_tmp != NULL)
   {
      if(0 == strncasecmp(mod_tmp->modname,modname,strlen(modname)))
      {
         return(mod_tmp);
      }
      mod_tmp = mod_tmp->next;
   }
   return(NULL);
}

/*******************************************************************************
   Function: unloadallmods removes delinks all modules from memroy
   Inputs: VOID
   Outputs: VOID
   Returns: VOID
*******************************************************************************/
void unloadallmods(struct bot *robo)
{
   logit("Unloading all modules!");
   
   while(mod_head != NULL)
   {
      unloadmod(robo,mod_head->modname); /* mod_head will be modified */
   }
}
