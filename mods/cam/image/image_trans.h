/**********************************************************************/
/* Bibliotheque de transformation d'images au format TrueColor 32bitd */
/* image_trans.h                                                      */
/*                                                                    */
/* Ecrit par : Daniel Lacroix (all rights reserved)                   */
/*                                                                    */
/**********************************************************************/

#ifndef __IMAGE_TRANS_H__
#define __IMAGE_TRANS_H__

#include "general.h"
#include "image.h"

/* Redimentionne l'image psrc et place le resultat */
/* a la postion (0,0) dans pdest en prenant        */
/* toujours le point le plus proche.               */
void image_resize_nearest(image *pdest, image *psrc, int32 width, int32 height);

/* Redimentionne l'image psrc et place le resultat */
/* a la postion (0,0) dans pdest. On determine les */
/* points par interpolation bilinaire.             */
void image_resize_bilinear(image *pdest, image *psrc, int32 width, int32 height);

/* Redimentionne l'image psrc et place le resultat */
/* a la postion (0,0) dans pdest. On determine les */
/* points par interpolation avec une DCT 2D des 64 */
/* points les plus pres.                           */
void image_resize_dct(image *pdest, image *psrc, int32 width, int32 height);

/* Redimentionne l'image psrc et place le resultat */
/* a la postion (0,0) dans pdest. On determine les */
/* points par interpolation avec une BSpline de    */
/* Catmull-Rom cubique.                            */
void image_resize_catmull_rom(image *pdest, image *psrc, int32 width, int32 height);

/* Redimentionne l'image psrc et place le resultat          */
/* dans pdest. pdest doit �tre 2 fois plus grande que psrc. */
void image_resize_tv(image *pdest, image *psrc);

/* Redimentionne l'image psrc et place le resultat          */
/* dans pdest. pdest doit �tre 2 fois plus grande que psrc. */
void image_resize_tv2(image *pdest, image *psrc);

/* Redimentionne l'image psrc et place le resultat          */
/* dans pdest. pdest doit �tre 2 fois plus grande que psrc. */
void image_resize_most(image *pdest, image *psrc);

/* Redimentionne l'image psrc et place le resultat */
/* a la postion (0,0) dans pdest en fusionnant les */
/* points. L'image r�sultat doit �tre plus petite  */
/* que l'image d'origine.                          */
void image_downsize(image *pdest, image *psrc,
  int32 width, int32 height);

/* Redimentionne l'image psrc et place le resultat */
/* a la postion (0,0) dans pdest en fusionnant les */
/* points. L'image r�sultat doit �tre plus petite  */
/* que l'image d'origine. L'algo est plus simple,  */
/* moins exact mais g�n�re moins de flou.          */
void image_downsize_rought(image *pdest, image *psrc,
  int32 width, int32 height);

#endif /* __IMAGE_TRANS_H__ */
