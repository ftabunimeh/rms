/****************************************************/
/* Pour cree une image a partir d'un fichier JPEG   */
/* image_from_jpeg.c                                */
/*                                                  */
/* Ecrit par : Daniel Lacroix (all rights reserved) */
/* adapte de imlib qui est sous LGPL                */
/* http://www.labs.redhat.com/imlib/                */
/****************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <sys/fcntl.h>
#include <jpeglib.h>
#include <setjmp.h>
#include "image_from_jpeg.h"

jmp_buf setjmp_buffer;

static void
g_JPEGFatalErrorHandler(j_common_ptr cinfo)
{
  cinfo->err->output_message(cinfo);
  longjmp(setjmp_buffer,1);
  return;
}

image *image_new_from_jpeg (char *file_name)
{ image *vimage;
  FILE  *file;
  unsigned char      *line[16], *ptr;
  int                 x, y, i, w, h, ofs;
  int maxbuf;
  struct jpeg_error_mgr pub;    
  struct jpeg_decompress_struct cinfo;

  if((file = fopen(file_name,"r")) == NULL) return NULL;

  cinfo.err = jpeg_std_error(&pub);
  jpeg_create_decompress(&cinfo);
  pub.error_exit =g_JPEGFatalErrorHandler;

  /* error handler to longjmp to, we want to preserve signals */
  if (setjmp(setjmp_buffer))
    {
      jpeg_destroy_decompress(&cinfo);
      fclose(file);
      return NULL;
    }


  jpeg_stdio_src(&cinfo, file);
  jpeg_read_header(&cinfo, TRUE);
  cinfo.do_fancy_upsampling = FALSE;
  cinfo.do_block_smoothing = FALSE;
  jpeg_start_decompress(&cinfo);
  w = cinfo.output_width;
  h = cinfo.output_height;
  vimage = image_new(w,h);
  if(!vimage)
  {
    jpeg_destroy_decompress(&cinfo);
    fclose(file);
    return NULL;
  }

  if(cinfo.rec_outbuf_height > 16)
  {
    fprintf(stderr, "ERROR image_from_jpeg : (image_from_jpeg.c) JPEG uses line buffers > 16. Cannot load.\n");
    image_free(vimage);
    fclose(file);
    return NULL;
  }
  maxbuf = vimage->width * vimage->height;
  if (cinfo.output_components == 3)
  { ofs = 0;
    if((ptr = (char *)malloc(w*3*cinfo.rec_outbuf_height)) == NULL)
    { perror("malloc failed "); exit(1); }
      
    for (y = 0; y < h; y += cinfo.rec_outbuf_height)
    {
	    for (i = 0; i < cinfo.rec_outbuf_height; i++)
	    {
	      line[i] = ptr + (w*3*i);
	    }
      jpeg_read_scanlines(&cinfo, line, cinfo.rec_outbuf_height);
      for(x=0;x<w*cinfo.rec_outbuf_height;x++)
      {
	if( ofs < maxbuf ) {
          vimage->buf[ofs] = COL(ptr[x+x+x], ptr[x+x+x+1], ptr[x+x+x+2]);
          ofs++;
        }
      }
    }
    free(ptr);
  }
  else if(cinfo.output_components == 1)
  { ofs = 0;
    for (i = 0; i < cinfo.rec_outbuf_height; i++)
    {
	    if((line[i] = (unsigned char *)malloc(w)) == NULL)
	    {
	      int t = 0;

	      for(t = 0; t < i; t++) free(line[t]);
	      jpeg_destroy_decompress(&cinfo);
        image_free(vimage);
        fclose(file);
	      return NULL;
	    }
    }
    for(y = 0; y < h; y += cinfo.rec_outbuf_height)
    {
      jpeg_read_scanlines(&cinfo, line, cinfo.rec_outbuf_height);
      for(i = 0; i < cinfo.rec_outbuf_height; i++)
	    {
	      for(x = 0; x < w; x++)
        {
          vimage->buf[ofs++] = COL(line[i][x], line[i][x], line[i][x]);
        }
	    }
    }
    for(i = 0; i < cinfo.rec_outbuf_height; i++) free(line[i]);
  }
  jpeg_finish_decompress(&cinfo);
  jpeg_destroy_decompress(&cinfo);

  fclose(file);
  return vimage;
}

image *image_new_from_jpeg_stream (FILE *file)
{ image *vimage;
  struct jpeg_decompress_struct cinfo;
  unsigned char      *line[16], *ptr;
  int                 x, y, i, w, h, ofs;
  int maxbuf;

  struct jpeg_error_mgr pub;

  cinfo.err = jpeg_std_error(&pub);
  jpeg_create_decompress(&cinfo);
  pub.error_exit = g_JPEGFatalErrorHandler;

  /* error handler to longjmp to, we want to preserve signals */
  if (setjmp(setjmp_buffer))
    {
      jpeg_abort_decompress(&cinfo);
      fclose(file);
      return NULL;
    }

 
  jpeg_stdio_src(&cinfo, file);
  jpeg_read_header(&cinfo, TRUE);
  cinfo.do_fancy_upsampling = FALSE;
  cinfo.do_block_smoothing = FALSE;
  jpeg_start_decompress(&cinfo);
  w = cinfo.output_width;
  h = cinfo.output_height;
  vimage = image_new(w,h);
  if(!vimage)
  {
    jpeg_destroy_decompress(&cinfo);
    return NULL;
  }

  if(cinfo.rec_outbuf_height > 16)
  {
    fprintf(stderr, "ERROR image_from_jpeg : (image_from_jpeg.c) JPEG uses line buffers > 16. Cannot load.\n");
    image_free(vimage);
    return NULL;
  }
  maxbuf = vimage->width * vimage->height;
  if (cinfo.output_components == 3)
  { ofs = 0;
    if((ptr = (char *)malloc(w*3*cinfo.rec_outbuf_height)) == NULL)
    { perror("malloc failed "); exit(1); }
      
    for (y = 0; y < h; y += cinfo.rec_outbuf_height)
    {
	    for (i = 0; i < cinfo.rec_outbuf_height; i++)
	    {
	      line[i] = ptr + (w*3*i);
	    }
      jpeg_read_scanlines(&cinfo, line, cinfo.rec_outbuf_height);
      for(x=0;x<w*cinfo.rec_outbuf_height;x++)
      {
	if( ofs < maxbuf ) {
          vimage->buf[ofs] = COL(ptr[x+x+x], ptr[x+x+x+1], ptr[x+x+x+2]);
          ofs++;
        }
      }
    }
    free(ptr);
  }
  else if(cinfo.output_components == 1)
  { ofs = 0;
    for (i = 0; i < cinfo.rec_outbuf_height; i++)
    {
	    if((line[i] = (unsigned char *)malloc(w)) == NULL)
	    {
	      int t = 0;

	      for(t = 0; t < i; t++) free(line[t]);
	      jpeg_destroy_decompress(&cinfo);
	      image_free(vimage);
	      return NULL;
	    }
    }
    for(y = 0; y < h; y += cinfo.rec_outbuf_height)
    {
      jpeg_read_scanlines(&cinfo, line, cinfo.rec_outbuf_height);
      for(i = 0; i < cinfo.rec_outbuf_height; i++)
      {
        for(x = 0; x < w; x++)
        {
          vimage->buf[ofs++] = COL(line[i][x], line[i][x], line[i][x]);
        }
      }
    }
    for(i = 0; i < cinfo.rec_outbuf_height; i++) free(line[i]);
  }
  jpeg_finish_decompress(&cinfo);
  jpeg_destroy_decompress(&cinfo);

  return vimage;
}

/*******************************************************/
/* Sauvegarde l'image pimage dans le fichier file      */
/* sous le format JPEG. Return -1 = si ERREUR, 0 sinon */
int image_save_to_jpeg(image *pimage, char *file,int jpeg_quality)
{
  struct jpeg_compress_struct cinfo;
  struct jpeg_error_mgr jerr;
  JSAMPROW row_pointer[1];
  int row_stride;
  FILE *f;
  char *data;
  int i,x;
	     
  f = fopen(file, "w");
  if (f)
	{
    cinfo.err = jpeg_std_error(&jerr);
    jpeg_create_compress(&cinfo);
    jerr.error_exit =g_JPEGFatalErrorHandler;  
     
     /* error handler to longjmp to, we want to preserve signals */
      if (setjmp(setjmp_buffer))
	{
        jpeg_destroy_compress(&cinfo);
	fclose(f);
        return -1;
	}
         
    jpeg_stdio_dest(&cinfo, f);
    cinfo.image_width = pimage->width;
    cinfo.image_height = pimage->height;
    cinfo.input_components = 3;
    cinfo.in_color_space = JCS_RGB;
    jpeg_set_defaults(&cinfo);
    jpeg_set_quality(&cinfo, (jpeg_quality * JPEG_QUALITY) >> 8, TRUE);
    jpeg_start_compress(&cinfo, TRUE);
    row_stride = cinfo.image_width * 3;
    if((data = (char *)malloc(row_stride)) == NULL)
    { perror("malloc failed "); exit(1); }
    i = 0;
    while(cinfo.next_scanline < cinfo.image_height)
    {
      for(x = 0; x < pimage->width; x++) {
        data[x+x+x]   = COL_RED(pimage->buf[i]);
        data[x+x+x+1] = COL_GREEN(pimage->buf[i]);
        data[x+x+x+2] = COL_BLUE(pimage->buf[i]);
        i++;
      }
      row_pointer[0] = data;
      /*im->rgb_data + (cinfo.next_scanline * row_stride);*/
      jpeg_write_scanlines(&cinfo, row_pointer, 1);
    }
    jpeg_finish_compress(&cinfo);
    free(data);
    fclose(f);
    return 0;
	}
  return -1;
}
/*******************************************************/

/*******************************************************/
/* Sauvegarde l'image pimage dans le flux stream       */
/* sous le format JPEG. Return -1 = si ERREUR, 0 sinon */
int image_save_to_jpeg_stream(image *pimage, FILE *stream,int jpeg_quality)
{
  struct jpeg_compress_struct cinfo;
  struct jpeg_error_mgr jerr;
  JSAMPROW row_pointer[1];
  int row_stride;
  char *data;
  int i,x;
	     
  cinfo.err = jpeg_std_error(&jerr);
  jpeg_create_compress(&cinfo);
  
  jerr.error_exit =g_JPEGFatalErrorHandler;  
     
     /* error handler to longjmp to, we want to preserve signals */
      if (setjmp(setjmp_buffer))
	{
        jpeg_destroy_compress(&cinfo);
        return -1;
	}
  
  
  jpeg_stdio_dest(&cinfo, stream);
  cinfo.image_width = pimage->width;
  cinfo.image_height = pimage->height;
  cinfo.input_components = 3;
  cinfo.in_color_space = JCS_RGB;
  jpeg_set_defaults(&cinfo);
  jpeg_set_quality(&cinfo, (jpeg_quality * JPEG_QUALITY) >> 8, TRUE);
  jpeg_start_compress(&cinfo, TRUE);
  row_stride = cinfo.image_width * 3;
  if((data = (char *)malloc(row_stride)) == NULL)
  { perror("malloc failed "); exit(1); }
  i = 0;
  while(cinfo.next_scanline < cinfo.image_height)
  {
    for(x = 0; x < pimage->width; x++) {
      data[x+x+x]   = COL_RED(pimage->buf[i]);
      data[x+x+x+1] = COL_GREEN(pimage->buf[i]);
      data[x+x+x+2] = COL_BLUE(pimage->buf[i]);
      i++;
    }
    row_pointer[0] = data;
    /*im->rgb_data + (cinfo.next_scanline * row_stride);*/
    jpeg_write_scanlines(&cinfo, row_pointer, 1);
  }
  jpeg_finish_compress(&cinfo);
  free(data);
  return 0;
}
/*******************************************************/
