/****************************************************/
/* Quelques primitives de dessin                    */
/* image_draw_func.c                                */
/*                                                  */
/* Ecrit par : Daniel Lacroix (all rights reserved) */
/*                                                  */
/****************************************************/

#include "image_draw_func.h"
#include "color.h"

/**********************************************************/
/* dessine un point aux coordonnees (x,y) de couleur col  */
void put_pix(image *pimage, int32 x, int32 y, pix col, AlphaType alpha_type)
{ uint32 a, inv_a;

  if((x >= 0) && (y >= 0) && (x < pimage->width) && (y < pimage->height))
  {
    switch(alpha_type)
    {
      case ALPHA_REPLACE :
        pimage->buf[(y * pimage->width) + x] = col;
        break;
      case ALPHA_KEEP    :
        pimage->buf[(y * pimage->width) + x] = 
          (col&(RED_MASK|GREEN_MASK|BLUE_MASK))|
          (pimage->buf[(y * pimage->width) + x]&ALPHA_MASK);
        break;
      case ALPHA_USE     :
        a = COL_ALPHA(col);
        inv_a = 255-a;
        pimage->buf[(y * pimage->width) + x] = COL_FULL(
          ((COL_RED  (pimage->buf[(y * pimage->width) + x]) * inv_a) +
            (COL_RED  (col) * a)) / 255,
          ((COL_GREEN(pimage->buf[(y * pimage->width) + x]) * inv_a) +
            (COL_GREEN(col) * a)) / 255,
          ((COL_BLUE (pimage->buf[(y * pimage->width) + x]) * inv_a) +
            (COL_BLUE (col) * a)) / 255,
          COL_ALPHA(pimage->buf[(y * pimage->width) + x]));
        break;
    }
  }
}
/**********************************************************/

/**********************************************************/
/* dessine un point aux coordonnees (x,y) de couleur col  */
/* la transparence resultante dans pimage de celle de col */
void put_pix_alpha_replace(image *pimage, int32 x, int32 y, pix col)
{
  if((x >= 0) && (y >= 0) && (x < pimage->width) && (y < pimage->height))
    pimage->buf[(y * pimage->width) + x] = col;
}
/**********************************************************/

/*************************************************************/
/* dessine un point aux coordonnees (x,y) de couleur col     */
/* la transparence resultante dans pimage de celle de pimage */
void put_pix_alpha_keep(image *pimage, int32 x, int32 y, pix col)
{
  if((x >= 0) && (y >= 0) && (x < pimage->width) && (y < pimage->height))
    pimage->buf[(y * pimage->width) + x] = 
      (pimage->buf[(y * pimage->width) + x] & ALPHA_MASK) |
      (col & (RED_MASK|GREEN_MASK|BLUE_MASK));
}
/*************************************************************/

/*************************************************************/
/* dessine un point aux coordonnees (x,y) de couleur col     */
/* la transparence resultante dans pimage de celle de pimage */
/* la transparence de col est utilise pour calculer le       */
/* nouveau point resultant de la superposition de col et du  */
/* point de pimage.                                          */
void put_pix_alpha_use(image *pimage, int32 x, int32 y, pix col)
{ uint32 a, inv_a;
  pix    col_dst;

  if((x >= 0) && (y >= 0) && (x < pimage->width) && (y < pimage->height))
  {
    col_dst = pimage->buf[(y * pimage->width) + x];
    a = COL_ALPHA(col);
    inv_a = 255-a;
    pimage->buf[(y * pimage->width) + x] = COL_FULL(
      ((COL_RED  (col_dst) * inv_a) + (COL_RED  (col) * a)) / 255,
      ((COL_GREEN(col_dst) * inv_a) + (COL_GREEN(col) * a)) / 255,
      ((COL_BLUE (col_dst) * inv_a) + (COL_BLUE (col) * a)) / 255,
      COL_ALPHA(col_dst));
  }
}
/*************************************************************/

/*******************************************************/
/* remplace la couleur src_col dans pimage par dst_col */
void image_replace_color(image *pimage, pix dst_col, pix src_col)
{ int32 x,y;

  for(y=0; y<pimage->height; y++)
    for(x=0; x<pimage->width; x++)
    {
      if(pimage->buf[(y*pimage->width)+x] == src_col)
        pimage->buf[(y*pimage->width)+x] = dst_col;
    }
}
/*******************************************************/

/*****************************************/
/* renvoi le point aux coordonnees (x,y) */
pix get_pix(image *pimage, int32 x, int32 y)
{
  if((x >= 0) && (y >= 0) && (x < pimage->width) && (y < pimage->height))
    return(pimage->buf[(y * pimage->width) + x]);
	else {
    pix vpix = BLACK;
	  return(vpix);
  }
}
/*****************************************/

/*************************************************************/
/* dessine une ligne entre (x1,y1) et (x2,y2) de couleur col */
void line(image *pimage, int32 x1, int32 y1, int32 x2, int32 y2, pix col,
  AlphaType alpha_type)
{ int32 x,y,d,deltax,deltay;
  pix   *sbuf, *ebuf;
  int32 ligne,yval,xval;
  uint32 a, inv_a;

  deltay = (y2 > y1)? y2 - y1 : y1 - y2;
  deltax = (x2 > x1)? x2 - x1 : x1 - x2;

  /* si deltax > deltay, on avance en x */
  if(deltax > deltay)
  {
    /* tri pour que x1 <= x2 */
    if(x2 < x1)
    {
      x = x1; x1 = x2; x2 = x;
      y = y1; y1 = y2; y2 = y;
    }

    ligne  = (y2 > y1)? pimage->width : -pimage->width;
    yval   = (y2 > y1)? 1 : -1;

    sbuf = &pimage->buf[y1*pimage->width+x1];
    ebuf = &pimage->buf[y2*pimage->width+x2];

    for(x=x1, y=y1, d=(deltay<<1)-deltax; x <= x2 - (deltax >> 1); x++)
    {
      /* Dessine un pixel si dans l'image */
      if((x >= 0) && (x < pimage->width) && (y >= 0) && (y < pimage->height))
      {
        switch(alpha_type)
        {
          case ALPHA_REPLACE :
            *sbuf = col;
            break;
          case ALPHA_KEEP    :
            *sbuf = (col&(RED_MASK|GREEN_MASK|BLUE_MASK))|(*sbuf&ALPHA_MASK);
            break;
          case ALPHA_USE     :
            a = COL_ALPHA(col);
            inv_a = 255-a;
            *sbuf = COL_FULL(
              ((COL_RED  (*sbuf) * inv_a) + (COL_RED  (col) * a)) / 255,
              ((COL_GREEN(*sbuf) * inv_a) + (COL_GREEN(col) * a)) / 255,
              ((COL_BLUE (*sbuf) * inv_a) + (COL_BLUE (col) * a)) / 255,
              COL_ALPHA(*sbuf));
            break;
        }
      }
      sbuf++;
      /* Dessine le pixel symetrique par rapport */
      /* au milieu si il est dans l'image        */
      if((x2-(x-x1) >= 0) && (x2-(x-x1) < pimage->width)
        && (y2-(y-y1) >= 0) && (y2-(y-y1) < pimage->height))
      {
        switch(alpha_type)
        {
          case ALPHA_REPLACE :
            *ebuf = col;
            break;
          case ALPHA_KEEP    :
            *ebuf = (col&(RED_MASK|GREEN_MASK|BLUE_MASK))|(*sbuf&ALPHA_MASK);
            break;
          case ALPHA_USE     :
            a = COL_ALPHA(col);
            inv_a = 255-a;
            *ebuf = COL_FULL(
              ((COL_RED  (*sbuf) * inv_a) + (COL_RED  (col) * a)) / 255,
              ((COL_GREEN(*sbuf) * inv_a) + (COL_GREEN(col) * a)) / 255,
              ((COL_BLUE (*sbuf) * inv_a) + (COL_BLUE (col) * a)) / 255,
              COL_ALPHA(*ebuf));
            break;
        }
      }
      ebuf--;
      
      if(d < 0) {
        d += deltay << 1;
      } else {
        d += (deltay - deltax) << 1;
        y += yval;
        sbuf += ligne;
        ebuf -= ligne;
      }
    }

  /* si deltay >= deltax, on avance de y */
  } else {

    /* tri pour que y1 <= y2 */
    if(y2 < y1)
    {
      x = x1; x1 = x2; x2 = x;
      y = y1; y1 = y2; y2 = y;
    }

    xval = (x2 > x1)? 1 : -1;

    sbuf = (uint32 *)(&pimage->buf[y1*pimage->width+x1]);
    ebuf = (uint32 *)(&pimage->buf[y2*pimage->width+x2]);

    for(x=x1, y=y1, d=(deltax<<1)-deltay; y <= y2 - (deltay >> 1); y++)
    {
      /* Dessine le pixel si il est dans l'image */
      if((x >= 0) && (x < pimage->width) && (y >= 0) && (y < pimage->height))
      {
        switch(alpha_type)
        {
          case ALPHA_REPLACE :
            *sbuf = col;
            break;
          case ALPHA_KEEP    :
            *sbuf = (col&(RED_MASK|GREEN_MASK|BLUE_MASK))|(*sbuf&ALPHA_MASK);
            break;
          case ALPHA_USE     :
            a = COL_ALPHA(col);
            inv_a = 255-a;
            *sbuf = COL_FULL(
              ((COL_RED  (*sbuf) * inv_a) + (COL_RED  (col) * a)) / 255,
              ((COL_GREEN(*sbuf) * inv_a) + (COL_GREEN(col) * a)) / 255,
              ((COL_BLUE (*sbuf) * inv_a) + (COL_BLUE (col) * a)) / 255,
              COL_ALPHA(*sbuf));
            break;
        }
      }
      sbuf += pimage->width;
      /* Dessine le pixel symetrique par rapport */
      /* au milieu si il est dans l'image        */
      if((x2-(x-x1) >= 0) && (x2-(x-x1) < pimage->width)
        && (y2-(y-y1) >= 0) && (y2-(y-y1) < pimage->height))
      {
        switch(alpha_type)
        {
          case ALPHA_REPLACE :
            *ebuf = col;
            break;
          case ALPHA_KEEP    :
            *ebuf = (col&(RED_MASK|GREEN_MASK|BLUE_MASK))|(*sbuf&ALPHA_MASK);
            break;
          case ALPHA_USE     :
            a = COL_ALPHA(col);
            inv_a = 255-a;
            *ebuf = COL_FULL(
              ((COL_RED  (*sbuf) * inv_a) + (COL_RED  (col) * a)) / 255,
              ((COL_GREEN(*sbuf) * inv_a) + (COL_GREEN(col) * a)) / 255,
              ((COL_BLUE (*sbuf) * inv_a) + (COL_BLUE (col) * a)) / 255,
              COL_ALPHA(*ebuf));
            break;
        }
      }
      ebuf -= pimage->width;
      
      if(d < 0) {
        d += deltax << 1;
      } else {
        d += (deltax - deltay) << 1;
        x += xval;
        sbuf += xval;
        ebuf -= xval;
      }
    }
  }

}
/*************************************************************/

/*********************************************************************/
/* dessine un rectangle vide entre (x1,y1) et (x2,y2) de couleur col */
void rectangle(image *pimage, int32 x1, int32 y1, int32 x2, int32 y2,pix col,
  AlphaType alpha_type)
{
  hline(pimage, x1, x2, y1, col, alpha_type);
  hline(pimage, x1, x2, y2, col, alpha_type);
  vline(pimage, x1, y1, y2, col, alpha_type);
  vline(pimage, x2, y1, y2, col, alpha_type);
}
/*********************************************************************/

/***********************************************************************/
/* dessine une ligne horizontale entre (x1,y) et (x2,y) de couleur col */
void hline(image *pimage, int32 x1, int32 x2, int32 y, pix col,
  AlphaType alpha_type)
{ int32  x;
  pix    *buf;
  uint32 a, inv_a;

  if(x1>x2){x = x1; x1 = x2; x2 = x;}

  /* pour le clipping avec l'image */
  if((x2<0) || (y<0) || (x1>=pimage->width) || (y>=pimage->height)) return;
  if(x1<0) x1 = 0;
  if(x2>=pimage->width)  x2 = pimage->width-1;
  
  buf = &pimage->buf[y*pimage->width+x1];

  for(x=x1;x<=x2;x++)
  {
    switch(alpha_type)
    {
      case ALPHA_REPLACE :
        *buf++ = col;
        break;
      case ALPHA_KEEP    :
        *buf = (col&(RED_MASK|GREEN_MASK|BLUE_MASK))|(*buf&ALPHA_MASK);
        buf++;
        break;
      case ALPHA_USE     :
        a = COL_ALPHA(col);
        inv_a = 255-a;
        *buf = COL_FULL(
          ((COL_RED  (*buf) * inv_a) + (COL_RED  (col) * a)) / 255,
          ((COL_GREEN(*buf) * inv_a) + (COL_GREEN(col) * a)) / 255,
          ((COL_BLUE (*buf) * inv_a) + (COL_BLUE (col) * a)) / 255,
          COL_ALPHA(*buf));
        buf++;
        break;
    }
  }
}
/***********************************************************************/

/*********************************************************************/
/* dessine une ligne verticale entre (x,y1) et (x,y2) de couleur col */
void vline(image *pimage, int32 x, int32 y1, int32 y2, pix col,
  AlphaType alpha_type)
{ int32  y;
  pix    *buf;
  uint32 a, inv_a;

  if(y1>y2){y = y1; y1 = y2; y2 = y;}

  /* pour le clipping avec l'image */
  if((x<0) || (y2<0) || (x>=pimage->width) || (y1>=pimage->height)) return;
  if(y1<0) y1 = 0;
  if(y2>=pimage->height)  y2 = pimage->height-1;
  
  buf = &pimage->buf[y1*pimage->width+x];

  for(y=y1;y<=y2;y++)
  {
    switch(alpha_type)
    {
      case ALPHA_REPLACE :
        *buf = col;
        break;
      case ALPHA_KEEP    :
        *buf = (col&(RED_MASK|GREEN_MASK|BLUE_MASK))|(*buf&ALPHA_MASK);
        break;
      case ALPHA_USE     :
        a = COL_ALPHA(col);
        inv_a = 255-a;
        *buf = COL_FULL(
          ((COL_RED  (*buf) * inv_a) + (COL_RED  (col) * a)) / 255,
          ((COL_GREEN(*buf) * inv_a) + (COL_GREEN(col) * a)) / 255,
          ((COL_BLUE (*buf) * inv_a) + (COL_BLUE (col) * a)) / 255,
          COL_ALPHA(*buf));
        break;
    }
    buf += pimage->width;
  }
}
/*********************************************************************/

/*******************************/
/* dessine un rectangle rempli */
void rectangle_fill(image *pimage, int32 x1, int32 y1,
  int32 x2, int32 y2,pix col, AlphaType alpha_type)
{ int32  x,y;
  pix    *buf;
  uint32 a, inv_a;
  
  if(x1>x2){x = x1; x1 = x2; x2 = x;}
  if(y1>y2){y = y1; y1 = y2; y2 = y;}
  
  if((x2<0) || (y2<0) || (x1>=pimage->width) || (y1>=pimage->height)) return;
  
  if(x1<0) x1 = 0; if(y1<0) y1 = 0;
  if(x2>=pimage->width)  x2 = pimage->width-1;
  if(y2>=pimage->height) y2 = pimage->height-1;

  buf = &pimage->buf[y1*pimage->width+x1];

  for(y=y1;y<=y2;y++)
  {
    for(x=x1;x<=x2;x++)
    {
      switch(alpha_type)
      {
        case ALPHA_REPLACE :
          *buf++ = col;
          break;
        case ALPHA_KEEP    :
          *buf = (col&(RED_MASK|GREEN_MASK|BLUE_MASK))|(*buf&ALPHA_MASK);
          buf++;
          break;
        case ALPHA_USE     :
          a = COL_ALPHA(col);
          inv_a = 255-a;
          *buf = COL_FULL(
            ((COL_RED  (*buf) * inv_a) + (COL_RED  (col) * a)) / 255,
            ((COL_GREEN(*buf) * inv_a) + (COL_GREEN(col) * a)) / 255,
            ((COL_BLUE (*buf) * inv_a) + (COL_BLUE (col) * a)) / 255,
            COL_ALPHA(*buf));
          buf++;
          break;
      }
    }
    buf += pimage->width - (x2-x1+1);
  }
}
/*******************************/

/*****************************/
/* repaint le fond avec col  */
void clear_img(image *pimage,pix col)
{ uint32 compt;
  
  /* place la couleur col dans tout le buffer image */
  for(compt=0;compt<pimage->width*pimage->height;compt++)
    pimage->buf[compt] = col;
}
/*****************************/

/******************************************/
/* Dessine une ellipse de centre (cx,cy)  */
/* et de rayon horizontal rx, vertical ry */
void ellipse(image *pimage, int32 cx, int32 cy, uint32 rx, uint32 ry, pix col,
  AlphaType alpha_type)
{
  /* intermediate terms to speed up loop */
  int32 t1 = rx*rx, t2 = t1<<1, t3 = t2<<1;
  int32 t4 = ry*ry, t5 = t4<<1, t6 = t5<<1;
  int32 t7 = rx*t5, t8 = t7<<1, t9 = 0L;
  int32 d1 = t2 - t7 + (t4>>1);    /* error terms */
  int32 d2 = (t1>>1) - t8 + t5;

  int32 x = rx, y = 0;     /* ellipse points */

  while (d2 < 0)                  /* til slope = -1 */
  {
    /* draw 4 points using symmetry */
    put_pix(pimage, cx + x, cy + y, col, alpha_type);
    put_pix(pimage, cx + x, cy - y, col, alpha_type);
    put_pix(pimage, cx - x, cy + y, col, alpha_type);
    put_pix(pimage, cx - x, cy - y, col, alpha_type);

    y++;            /* always move up here */
    t9 += t3;       
    if (d1 < 0)     /* move straight up */
    {
      d1 += t9 + t2;
      d2 += t9;
    } else {           /* move up and left */
      x--;
      t8 -= t6;
      d1 += t9 + t2 - t8;
      d2 += t9 + t5 - t8;
    }
  }

  do                              /* rest of top right quadrant */
  {
    /* draw 4 points using symmetry */
    put_pix(pimage, cx + x, cy + y, col, alpha_type);
    put_pix(pimage, cx + x, cy - y, col, alpha_type);
    put_pix(pimage, cx - x, cy + y, col, alpha_type);
    put_pix(pimage, cx - x, cy - y, col, alpha_type);

    x--;            /* always move left here */
    t8 -= t6;       
    if(d2 < 0)     /* move up and left */
    {
      y++;
      t9 += t3;
      d2 += t9 + t5 - t8;
    } else            /* move straight left */
      d2 += t5 - t8;
  } while(x >= 0);
}
/******************************************/

/******************************************************/
/* Dessine une ellipse rempli de centre (cx,cy) et de */
/* rayon horizontal rx, vertical ry                   */
void ellipse_fill(image *pimage, int32 cx, int32 cy,
  uint32 rx, uint32 ry, pix col, AlphaType alpha_type)
{

  int32 t1 = rx*rx, t2 = t1<<1, t3 = t2<<1;
  int32 t4 = ry*ry, t5 = t4<<1, t6 = t5<<1;
  int32 t7 = rx*t5, t8 = t7<<1, t9 = 0;
  int32 d1 = t2 - t7 + (t4>>1);    /* error terms */
  int32 d2 = (t1>>1) - t8 + t5;
  int32 x = rx, y = 0;     /* ellipse points */

  while (d2 < 0)                  /* til slope = -1 */
  {
    /* fill in leftward to inner ellipse */
    hline(pimage, cx+x, cx-x, cy-y, col, alpha_type);
    hline(pimage, cx+x, cx-x, cy+y, col, alpha_type);

    y++;            /* always move up here */
    t9 += t3;       
    if (d1 < 0)     /* move straight up */
    {
      d1 += t9 + t2;
      d2 += t9;
    } else {            /* move up and left */
      x--;
      t8 -= t6;
      d1 += t9 + t2 - t8;
      d2 += t9 + t5 - t8;
    }
  }

  do                              /* rest of top right quadrant */
  {
    /* fill in downward to inner ellipse */
    hline(pimage, cx+x, cx-x, cy-y, col, alpha_type);
    hline(pimage, cx+x, cx-x, cy+y, col, alpha_type);

    x--;            /* always move left here */
    t8 -= t6;       
    if (d2 < 0)     /* move up and left */
    {
      y++;
      t9 += t3;
      d2 += t9 + t5 - t8;
    } else {           /* move straight left */
      d2 += t5 - t8;
    }
  } while(x >= 0);
}
/******************************************************/

/*****************************************************************/
/* dessine une ligne horizontale entre (x1,y) de couleur col1    */
/* et (x2,y) de couleur col2 avec un degrade lineaire de couleur */
void hline_shade(image *pimage, int32 x1, int32 x2, int32 y,
  pix col1, pix col2, AlphaType alpha_type)
{ int32 x;
  pix col;
  pix *buf;
  uint32 a, inv_a;
  uint32 red, green, blue;

  if(x1>x2){x = x1; x1 = x2; x2 = x; col = col1; col1 = col2; col2 = col;}

  /* pour le clipping avec l'image */
  if((x2<0) || (y<0) || (x1>=pimage->width) || (y>=pimage->height)) return;
  
  buf = &pimage->buf[y*pimage->width+MAX(0,x1)];
  
  for(x = MAX(0,x1); x < MIN(pimage->width,x2+1); x++)
  {
    switch(alpha_type)
    {
      case ALPHA_REPLACE :
        *buf++ = COL_FULL(
          (COL_RED(col1) * (x2-x) / (x2-x1)) 
            + (COL_RED(col2) * (x-x1) / (x2-x1)),
          (COL_GREEN(col1) * (x2-x) / (x2-x1))
            + (COL_GREEN(col2) * (x-x1) / (x2-x1)),
          (COL_BLUE(col1) * (x2-x) / (x2-x1))
            + (COL_BLUE(col2) * (x-x1) / (x2-x1)),
          (COL_ALPHA(col1) * (x2-x) / (x2-x1))
            + (COL_ALPHA(col2) * (x-x1) / (x2-x1))
        );
        break;
      case ALPHA_KEEP    :
        *buf = COL_FULL(
          (COL_RED(col1) * (x2-x) / (x2-x1)) 
            + (COL_RED(col2) * (x-x1) / (x2-x1)),
          (COL_GREEN(col1) * (x2-x) / (x2-x1))
            + (COL_GREEN(col2) * (x-x1) / (x2-x1)),
          (COL_BLUE(col1) * (x2-x) / (x2-x1))
            + (COL_BLUE(col2) * (x-x1) / (x2-x1)),
          COL_ALPHA(*buf)
        );
        buf++;
        break;
      case ALPHA_USE     :
        a = (COL_ALPHA(col1) * (x2-x) / (x2-x1)) +
          (COL_ALPHA(col2) * (x-x1) / (x2-x1));
        inv_a = 255-a;
        red = (COL_RED(col1) * (x2-x) / (x2-x1)) 
          + (COL_RED(col2) * (x-x1) / (x2-x1));
        green = (COL_GREEN(col1) * (x2-x) / (x2-x1))
          + (COL_GREEN(col2) * (x-x1) / (x2-x1));
        blue = (COL_BLUE(col1) * (x2-x) / (x2-x1))
          + (COL_BLUE(col2) * (x-x1) / (x2-x1));
        *buf = COL_FULL(
          ((COL_RED  (*buf) * inv_a) + (red   * a)) / 255,
          ((COL_GREEN(*buf) * inv_a) + (green * a)) / 255,
          ((COL_BLUE (*buf) * inv_a) + (blue  * a)) / 255,
          COL_ALPHA(*buf));
        buf++;
        break;
    }
  }
}
/*****************************************************************/

/*****************************************************************/
/* dessine une ligne verticale entre (x,y1) de couleur col1      */
/* et (x,y2) de couleur col2 avec un degrade lineaire de couleur */
void vline_shade(image *pimage, int32 x, int32 y1, int32 y2,
  pix col1, pix col2, AlphaType alpha_type)
{ int32 y;
  pix col;
  pix *buf;
  uint32 a, inv_a;
  uint32 red, green, blue;

  if(y1>y2){y = y1; y1 = y2; y2 = y; col = col1; col1 = col2; col2 = col;}

  /* pour le clipping avec l'image */
  if((y2<0) || (x<0) || (y1>=pimage->height) || (x>=pimage->width)) return;
  
  buf = &pimage->buf[(MAX(y1,0)*pimage->width)+x];
  
  for(y = MAX(0,y1); y < MIN(pimage->height,y2+1); y++)
  {
    switch(alpha_type)
    {
      case ALPHA_REPLACE :
        *buf = COL_FULL(
          (COL_RED(col1) * (y2-y) / (y2-y1))
            + (COL_RED(col2) * (y-y1) / (y2-y1)),
          (COL_GREEN(col1) * (y2-y) / (y2-y1))
            + (COL_GREEN(col2) * (y-y1) / (y2-y1)),
          (COL_BLUE(col1) * (y2-y) / (y2-y1))
            + (COL_BLUE(col2) * (y-y1) / (y2-y1)),
          (COL_ALPHA(col1) * (y2-y) / (y2-y1))
            + (COL_ALPHA(col2) * (y-y1) / (y2-y1))
        );
        break;
      case ALPHA_KEEP    :
        *buf = COL_FULL(
          (COL_RED(col1) * (y2-y) / (y2-y1))
            + (COL_RED(col2) * (y-y1) / (y2-y1)),
          (COL_GREEN(col1) * (y2-y) / (y2-y1))
            + (COL_GREEN(col2) * (y-y1) / (y2-y1)),
          (COL_BLUE(col1) * (y2-y) / (y2-y1))
            + (COL_BLUE(col2) * (y-y1) / (y2-y1)),
          COL_ALPHA(*buf)
        );
        break;
      case ALPHA_USE     :
        a = (COL_ALPHA(col1) * (y2-y) / (y2-y1))
            + (COL_ALPHA(col2) * (y-y1) / (y2-y1));
        inv_a = 255-a;
        red = (COL_RED(col1) * (y2-y) / (y2-y1))
            + (COL_RED(col2) * (y-y1) / (y2-y1));
        green = (COL_GREEN(col1) * (y2-y) / (y2-y1))
            + (COL_GREEN(col2) * (y-y1) / (y2-y1));
        blue = (COL_BLUE(col1) * (y2-y) / (y2-y1))
            + (COL_BLUE(col2) * (y-y1) / (y2-y1));
        *buf = COL_FULL(
          ((COL_RED  (*buf) * inv_a) + (red   * a)) / 255,
          ((COL_GREEN(*buf) * inv_a) + (green * a)) / 255,
          ((COL_BLUE (*buf) * inv_a) + (blue  * a)) / 255,
          COL_ALPHA(*buf));
        break;
    }
    buf += pimage->width;
  }
}
/*****************************************************************/

/*******************************************************************/
/* Dessine un rectangle rempli entre les points (x1,y1) et (x2,y2) */
/* ce rectangle est rempli par un degrade lineaire de couleur      */
/*  definie comme sur le schema ci-dessous                         */
/*  col1 ---- col2                                                 */
/*   |          |                                                  */
/*  col3 ---- col4                                                 */
void rectangle_fill_shade(image *pimage, int32 x1, int32 y1,
  int32 x2, int32 y2, pix col1, pix col2, pix col3, pix col4,
  AlphaType alpha_type)
{ int32 y,x;
  pix col_beg, col_end;
  
  if(x1>x2){x = x1; x1 = x2; x2 = x;}
  if(y1>y2){y = y1; y1 = y2; y2 = y;}
  
  if((x2<0) || (y2<0) || (x1>=pimage->width) || (y1>=pimage->height)) return;

  if(x1 == x2)
  { /* si c'est une ligne verticale, on traite separement */
    /* pour ne pas avoir de divisions par 0.              */
    col_beg = COL_FULL(
      (COL_RED  (col1) + COL_RED  (col2))>>1,
      (COL_GREEN(col1) + COL_GREEN(col2))>>1,
      (COL_BLUE (col1) + COL_BLUE (col2))>>1,
      (COL_ALPHA(col1) + COL_ALPHA(col2))>>1
    );
    
    col_end = COL_FULL(
      (COL_RED  (col3) + COL_RED  (col4))>>1,
      (COL_GREEN(col3) + COL_GREEN(col4))>>1,
      (COL_BLUE (col3) + COL_BLUE (col4))>>1,
      (COL_ALPHA(col3) + COL_ALPHA(col4))>>1
    );

    vline_shade(pimage, x1, y1, y2, col_beg, col_end, alpha_type);
  } else {
    for(y = MAX(0,y1); y < MIN(pimage->height,y2+1); y++)
    {
      col_beg = COL_FULL(
        (COL_RED(col1) * (y2-y) / (y2-y1))
          + (COL_RED(col3) * (y-y1) / (y2-y1)),
        (COL_GREEN(col1) * (y2-y) / (y2-y1))
          + (COL_GREEN(col3) * (y-y1) / (y2-y1)),
        (COL_BLUE(col1) * (y2-y) / (y2-y1))
          + (COL_BLUE(col3) * (y-y1) / (y2-y1)),
        (COL_ALPHA(col1) * (y2-y) / (y2-y1))
          + (COL_ALPHA(col3) * (y-y1) / (y2-y1))
      );
      
      col_end = COL_FULL(
        (COL_RED(col2) * (y2-y) / (y2-y1))
          + (COL_RED(col4) * (y-y1) / (y2-y1)),
        (COL_GREEN(col2) * (y2-y) / (y2-y1))
          + (COL_GREEN(col4) * (y-y1) / (y2-y1)),
        (COL_BLUE(col2) * (y2-y) / (y2-y1))
          + (COL_BLUE(col4) * (y-y1) / (y2-y1)),
        (COL_ALPHA(col2) * (y2-y) / (y2-y1))
          + (COL_ALPHA(col4) * (y-y1) / (y2-y1))
      );
    
      hline_shade(pimage, x1, x2, y, col_beg, col_end, alpha_type);
    }
  }
}
/*******************************/

/*********************************************/
/* dessine une ligne avec antialiasime entre */
/* (x1,y1) et (x2,y2) de couleur col         */
void line_aliased(image *pimage, int32 x1, int32 y1,
  int32 x2, int32 y2, pix col)
{ int32 x,y,deltax,deltay;
  int32 yval,xval;
  float trans;
  float x_real,y_real;

  deltay = (y2 > y1)? y2 - y1 : y1 - y2;
  deltax = (x2 > x1)? x2 - x1 : x1 - x2;

  /* si deltax > deltay, on avance en x */
  if(deltax > deltay)
  {
    /* tri pour que x1 <= x2 */
    if(x2 < x1)
    {
      x = x1; x1 = x2; x2 = x;
      y = y1; y1 = y2; y2 = y;
    }

    yval   = (y2 > y1)? 1 : -1;

    for(x=x1; x <= x2 - (deltax >> 1); x++)
    {
      y_real = (((float)((x2 - x)*y1 +  (x - x1)*y2))/((float)deltax));
      y = (int32)y_real;
      trans = ((float)y) - y_real;
      trans = ABS(trans);
      trans = 1.0 - trans;
      
      /* Dessine un pixel si dans l'image */
      if((x >= 0) && (x < pimage->width) && (y >= 0) && (y < pimage->height))
      {
        pimage->buf[y*pimage->width+x] = COL(
          (uint8)(COL_RED(pimage->buf[y*pimage->width+x]) * (1.0 - trans)
            + COL_RED(col) * trans),
          (uint8)(COL_GREEN(pimage->buf[y*pimage->width+x]) * (1.0 - trans)
            + COL_GREEN(col) * trans),
          (uint8)(COL_BLUE(pimage->buf[y*pimage->width+x]) * (1.0 - trans)
            + COL_BLUE(col) * trans)
        );
      }
      if((x >= 0) && (x < pimage->width) && (y+yval >= 0)
        && (y+yval < pimage->height))
      {
        pimage->buf[(y+yval)*pimage->width+x] = COL(
          (uint8)(COL_RED(pimage->buf[(y+yval)*pimage->width+x]) * trans
            + COL_RED(col) * (1.0 - trans)),
          (uint8)(COL_GREEN(pimage->buf[(y+yval)*pimage->width+x]) * trans
            + COL_GREEN(col) * (1.0 - trans)),
          (uint8)(COL_BLUE(pimage->buf[(y+yval)*pimage->width+x]) * trans
            + COL_BLUE(col) * (1.0 - trans))
        );
      }

      if(x2-(x-x1) != x)
      {
        /* Dessine le pixel symetrique par rapport */
        /* au milieu si il est dans l'image        */
        if((x2-(x-x1) >= 0) && (x2-(x-x1) < pimage->width)
          && (y2-(y-y1) >= 0) && (y2-(y-y1) < pimage->height))
        {
          pimage->buf[(y2-(y-y1))*pimage->width+x2-(x-x1)] = COL(
            (uint8)(COL_RED(pimage->buf[(y2-(y-y1))*pimage->width+x2-(x-x1)])
              * (1.0 - trans) + COL_RED(col) * trans),
            (uint8)(COL_GREEN(pimage->buf[(y2-(y-y1))*pimage->width+x2-(x-x1)])
              * (1.0 - trans) + COL_GREEN(col) * trans),
            (uint8)(COL_BLUE(pimage->buf[(y2-(y-y1))*pimage->width+x2-(x-x1)])
              * (1.0 - trans) + COL_BLUE(col) * trans)
          );
        }
        if((x2-(x-x1) >= 0) && (x2-(x-x1) < pimage->width)
          && (y2-(y-y1)-yval >= 0) && (y2-(y-y1)-yval < pimage->height))
        {
          pimage->buf[(y2-(y-y1)-yval)*pimage->width+x2-(x-x1)] = COL(
            (uint8)(COL_RED(pimage->buf
              [(y2-(y-y1)-yval)*pimage->width+x2-(x-x1)]) * trans
              + COL_RED(col) * (1.0 - trans)),
            (uint8)(COL_GREEN(pimage->buf
              [(y2-(y-y1)-yval)*pimage->width+x2-(x-x1)]) * trans
              + COL_GREEN(col) * (1.0 - trans)),
            (uint8)(COL_BLUE(pimage->buf
              [(y2-(y-y1)-yval)*pimage->width+x2-(x-x1)]) * trans
              + COL_BLUE(col) * (1.0 - trans))
          );
        }
      }

    }

  /* si deltay >= deltax, on avance de y */
  } else {

    /* tri pour que y1 <= y2 */
    if(y2 < y1)
    {
      x = x1; x1 = x2; x2 = x;
      y = y1; y1 = y2; y2 = y;
    }

    xval = (x2 > x1)? 1 : -1;

    for(y=y1; y <= y2 - (deltay >> 1); y++)
    {
      x_real = (((float)((y2 - y)*x1 +  (y - y1)*x2))/((float)deltay));
      x = (int32)x_real;
      trans = ((float)x) - x_real;
      trans = ABS(trans);
      trans = 1.0 - trans;

      /* Dessine le pixel si il est dans l'image */
      if((x >= 0) && (x < pimage->width) && (y >= 0) && (y < pimage->height))
      {
        pimage->buf[y*pimage->width+x] = COL(
          (uint8)(COL_RED(pimage->buf[y*pimage->width+x]) * (1.0 - trans)
            + COL_RED(col) * trans),
          (uint8)(COL_GREEN(pimage->buf[y*pimage->width+x]) * (1.0 - trans)
            + COL_GREEN(col) * trans),
          (uint8)(COL_BLUE(pimage->buf[y*pimage->width+x]) * (1.0 - trans)
            + COL_BLUE(col) * trans)
        );
      }
      if((x+xval >= 0) && (x+xval < pimage->width) && (y >= 0)
        && (y < pimage->height))
      {
        pimage->buf[y*pimage->width+x+xval] = COL(
          (uint8)(COL_RED(pimage->buf[y*pimage->width+x+xval]) * trans
            + COL_RED(col) * (1.0 - trans)),
          (uint8)(COL_GREEN(pimage->buf[y*pimage->width+x+xval]) * trans
            + COL_GREEN(col) * (1.0 - trans)),
          (uint8)(COL_BLUE(pimage->buf[y*pimage->width+x+xval]) * trans
            + COL_BLUE(col) * (1.0 - trans))
        );
      }
      if(y2-(y-y1) != y)
      {
        /* Dessine le pixel symetrique par rapport */
        /* au milieu si il est dans l'image        */
        if((x2-(x-x1) >= 0) && (x2-(x-x1) < pimage->width)
          && (y2-(y-y1) >= 0) && (y2-(y-y1) < pimage->height))
        {
          pimage->buf[(y2-(y-y1))*pimage->width+x2-(x-x1)] = COL(
            (uint8)(COL_RED(pimage->buf
              [(y2-(y-y1))*pimage->width+x2-(x-x1)]) * (1.0 - trans)
              + COL_RED(col) * trans),
            (uint8)(COL_GREEN(pimage->buf
              [(y2-(y-y1))*pimage->width+x2-(x-x1)]) * (1.0 - trans)
              + COL_GREEN(col) * trans),
            (uint8)(COL_BLUE(pimage->buf
              [(y2-(y-y1))*pimage->width+x2-(x-x1)]) * (1.0 - trans)
              + COL_BLUE(col) * trans)
          );
        }
        if((x2-(x-x1)-xval >= 0) && (x2-(x-x1)-xval < pimage->width)
          && (y2-(y-y1) >= 0) && (y2-(y-y1) < pimage->height))
        {
          pimage->buf[(y2-(y-y1))*pimage->width+x2-(x-x1)-xval] = COL(
            (uint8)(COL_RED(pimage->buf
              [(y2-(y-y1))*pimage->width+x2-(x-x1)-xval]) * trans
              + COL_RED(col) * (1.0 - trans)),
            (uint8)(COL_GREEN(pimage->buf
              [(y2-(y-y1))*pimage->width+x2-(x-x1)-xval]) * trans
              + COL_GREEN(col) * (1.0 - trans)),
            (uint8)(COL_BLUE(pimage->buf
              [(y2-(y-y1))*pimage->width+x2-(x-x1)-xval]) * trans
              + COL_BLUE(col) * (1.0 - trans))
          );
        }
      }
    }
  }
}
/*********************************************/
