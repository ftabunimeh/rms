/***********************************************************/
/* Pour cree une image a partir d'un fichier PPM           */
/* image_from_ppm.c                                        */
/*                                                         */
/* Ecrit par : Daniel Lacroix (all rights reserved)        */
/*                                                         */
/***********************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include "image_from_ppm.h"

typedef enum {
  ASCII,
  BINARY
} ImgFormat;

/* Renvoi : 0 = Ok, != 0 = Erreur                                */
/* offset et positionne sur le premier caractere apres le nombre */
/* res contient la valeur lue.                                   */
static int give_uint32(char *buf, int *offset, int buf_size, uint32 *res)
{ uint32  last_res;
  boolean comment = FALSE;

  do {
    /* si on est a la fin c'est que le fichier est mal forme */
    if(*offset >= buf_size)
    { return(1); }
    
    if((buf[*offset] == '\n') && (comment))
    { /* fin d'un commentaire */
      comment = FALSE;
    } else if((buf[*offset] == '#') && (!comment))
    { /* debut d'un commentaire qui va jusqu'a la fin de la ligne */
      comment = TRUE;
    }
    /* si on est dans un commentaire ou qu'il ne s'agit pas */
    /* d'un nombre, alors on passe a caractere suivant.     */
    if(comment || (buf[*offset] < '0') || (buf[*offset] > '9'))
    {
      (*offset)++;
    } else {
      /* si on a trouve le debut du nombre, on sort de la boucle */
      break;
    }
  } while(1);
  last_res = *res = 0;
  do {
    *res = (*res)*10 + (buf[*offset]-'0');
    if(*res < last_res)
    { /* il y a debordement, le nombre est trop grand */
      return(1);
    }
    (*offset)++;
  } while((*offset < buf_size) && (buf[*offset] >= '0') && (buf[*offset] <= '9'));
  
  return(0);
}

/* Renvoi l'image ou NULL si impossible */
image *image_new_from_ppm(char *file_name)
{ int handle;
  int file_size;
  char *buf;
  image *vimage;
  int i,offset,x,y;
  int32 width,height;
  uint32 vuint32;
  uint32 vuint32_red, vuint32_green, vuint32_blue;
  uint32 level_max;
  ImgFormat mode;

  if((handle = open(file_name,O_RDONLY)) == -1) return(NULL);
  if((file_size = lseek(handle, 0, SEEK_END)) == -1)
  { close(handle); return(NULL); }
  if(lseek(handle, 0, SEEK_SET) == -1)
  { close(handle); return(NULL); }
  if((file_size > MAX_PPM_FILE_SIZE) || 
     (file_size < 2))
  { close(handle); return(NULL); }
  if((buf = (char *)malloc(file_size)) == NULL)
  { perror("malloc failed."); exit(1); }
  if(read(handle, buf, file_size) == -1)
  { free(buf); close(handle); return(NULL); }
  close(handle);

  if((buf[0] == 'P') && (buf[1] == '3'))
  { /* il s'agit du format pgm ASCI */
    mode = ASCII;
  } else if((buf[0] == 'P') && (buf[1] == '6'))
  { /* il s'agit du format pgm RAW */
    mode = BINARY;
  } else {
    free(buf); return(NULL);
  }

  offset = 2;
  /* on lit la largeur */
  if(give_uint32(buf, &offset, file_size, &vuint32))
  { free(buf); return(NULL); }
  width = (int32)vuint32;
  if(width < vuint32)
  { free(buf); return(NULL); }
  /* on lit la hauteur */
  if(give_uint32(buf, &offset, file_size, &vuint32))
  { free(buf); return(NULL); }
  height = (int32)vuint32;
  if(height < vuint32)
  { free(buf); return(NULL); }
  /* on lit le nombre de niveaux de gris */
  if(give_uint32(buf, &offset, file_size, &level_max))
  { free(buf); return(NULL); }

  /* on cree l'image qui contiendra le resultat */
  vimage = image_new(width,height);

  /* on lit le contenu de l'image */
  switch(mode)
  {
    case ASCII  :
      for(y=0;y<height;y++)
      {
        for(x=0;x<width;x++)
        {
          if(give_uint32(buf, &offset, file_size, &vuint32_red))
          { image_free(vimage); free(buf); return(NULL); }
          if(give_uint32(buf, &offset, file_size, &vuint32_green))
          { image_free(vimage); free(buf); return(NULL); }
          if(give_uint32(buf, &offset, file_size, &vuint32_blue))
          { image_free(vimage); free(buf); return(NULL); }

          if(255 > level_max) {
            vuint32_red   = (vuint32_red   * 255) / level_max;
            vuint32_green = (vuint32_green * 255) / level_max;
            vuint32_blue  = (vuint32_blue  * 255) / level_max;
          } else {
            vuint32_red   = vuint32_red   / (level_max / 255);
            vuint32_green = vuint32_green / (level_max / 255);
            vuint32_blue  = vuint32_blue  / (level_max / 255);
          }
          vimage->buf[x+(y*width)] = COL(vuint32_red,vuint32_green,vuint32_blue);
        }
      }
      break;
    case BINARY :
      offset++;
      /* si un pixel tient sur plus de 2 octets, on ne traite pas */
      if(level_max > 0xFFFF)
      { image_free(vimage); free(buf); return(NULL); }
      
      if(level_max > 0xFF)
      { /* cas des pixels sur 16 bits */
        if(width*height*2*3 != (file_size-offset))
        { image_free(vimage); free(buf); return(NULL); }
      } else {
        /* cas des pixels sur 8 bits */
        if(width*height*3 != (file_size-offset))
        { image_free(vimage); free(buf); return(NULL); }
      }

      for(i=0;i<height*width;i++)
      {
        if(level_max > 0xFF)
        { /* attention je ne suis pas sure de l'ordre des octets */
          vuint32_red   = (((uint8 *)buf)[offset]<<8) | ((uint8 *)buf)[offset+1];
          offset += 2;
          vuint32_green = (((uint8 *)buf)[offset]<<8) | ((uint8 *)buf)[offset+1];
          offset += 2;
          vuint32_blue  = (((uint8 *)buf)[offset]<<8) | ((uint8 *)buf)[offset+1];
          offset += 2;
        } else {
          vuint32_red   = ((uint8 *)buf)[offset];
          offset++;
          vuint32_green = ((uint8 *)buf)[offset];
          offset++;
          vuint32_blue  = ((uint8 *)buf)[offset];
          offset++;
        }
        /* on ne risque pas de debordement */
        vuint32_red   = (vuint32_red   * 255) / level_max;
        vuint32_green = (vuint32_green * 255) / level_max;
        vuint32_blue  = (vuint32_blue  * 255) / level_max;
        vimage->buf[i] = COL(vuint32_red,vuint32_green,vuint32_blue);
      }
      break;
  }  
  free(buf);
  return(vimage);
}

/*******************************************************/
/* Sauvegarde l'image pimage dans le fichier file_name */
/* dans le format PPM. Return -1 = si ERREUR, 0 sinon  */
int image_save_to_ppm(image *pimage, char *file_name)
{ int handle;
  char *str;
  int str_size;
  int size;
  uint8 *line_buf;
  int x,y;

  /* si l'image n'est pas correcte, on quitte */
  if((pimage == NULL) || (pimage->buf == NULL) ||
     (pimage->width == 0) || (pimage->height == 0)) return(-1);

  /* ouvre le fichier en ecriture et cr�ation si il n'existe pas              */
  /* dans ce cas, on donne les droits de lecture et �criture au propri�taire  */
  if((handle = open(file_name,O_WRONLY|O_CREAT,S_IRUSR|S_IWUSR)) == -1) return(-1);

  /* fabrique la chaine de caractere de l'entete */
  /* on choisi le format RAW (binaire) = P6      */
  str_size = 20;
  if((str = (char *)malloc(str_size)) == NULL)
  { perror("malloc failed "); exit(1); }

  do {  
    size = snprintf(str,str_size,"P6\n%d %d\n255\n",pimage->width,pimage->height);
    if(size == -1)
    { char *str_new;
    
      str_size *= 2;
      if((str_new = realloc(str, str_size)) == NULL)
      { perror("realloc failed "); exit(1); }
      str = str_new;
    }
  } while(size == -1);

  /* on ecrit l'entete */
  if(write(handle, str, strlen(str)) == -1)
  { free(str); close(handle); return(-1); }
  free(str);

  /* on alloue un buffer pour convertir une ligne */
  /* de notre image dans son format interne vers  */
  /* une ligne du format PPM (rouge vert bleu)    */
  if((line_buf = (uint8 *)malloc(pimage->width*3)) == NULL)
  { perror("malloc failed "); exit(1); }

  /* on parcourt toutes les lignes et on les ecrit une par une */
  for(y=0;y<pimage->height;y++)
  {
    for(x=0;x<pimage->width;x++)
    {
      line_buf[x*3+0] = COL_RED  (pimage->buf[(pimage->width*y)+x]);
      line_buf[x*3+1] = COL_GREEN(pimage->buf[(pimage->width*y)+x]);
      line_buf[x*3+2] = COL_BLUE (pimage->buf[(pimage->width*y)+x]);
    }
    if(write(handle, line_buf, pimage->width*3) == -1)
    { free(line_buf); close(handle); return(-1); }
  }

  /* on libere le buffer qui contient une ligne */
  free(line_buf);
  /* on ferme le fichier */
  close(handle);
  return(0);
}
/*******************************************************/
