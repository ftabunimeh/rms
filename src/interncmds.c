/*******************************************************************************
Copyright (c) 2004, MSU ECE480 F04 Team01, Faisal Abu-Nimeh
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list
of conditions and the following disclaimer. Redistributions in binary form must
reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the
distribution. Neither the name of the Michigan State University nor the names
of its contributors may be used to endorse or promote products derived from this
software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************/

/* This is an interface to allow runcmd() to execute internal functions */

/*
  Changes:
  12/05/04 [Faisal]: Added command feeback to client. either failed or success.
*/

#include "main.h"
#include "external.h"

/* CMD1 -- NULL :: intern_null does not do anything */
int intern_null(RCLIENT *client, char *arguments)
{
   /* arguments = N/A */
   sendtoclient(client,"%d %d\n",11,1);
   return 0;
}

/* CMD2 -- UPTIME :: intern_uptime returns uptime in seconds */
int intern_uptime(RCLIENT *client, char *arguments)
{
   time_t now;
   /* arguments = N/A */
   
   time(&now);
   now = now - client->robotstk->startupt;
   
   sendtoclient(client,"%d %d\n",2,now);
   return 0;
}  

/* CMD3 -- REHASH :: intern_rehash reloads RMS */
int intern_rehash(RCLIENT *client, char *arguments)
{
   /* arguments = N/A */
   rehashrobot();
   return 0;
}
   
/* CMD4 -- SETTINGS :: intern_settings returns current RMS settings */
int intern_settings(RCLIENT *client, char *arguments)
{
   /* arguments = N/A */
   return 0;
}
   
/* CMD5 -- EXIT :: intern_exit exits RMS with a tracking reason */
int intern_exit(RCLIENT *client, char *arguments)
{
   /* arguments = exit reason */
   exitrobot(arguments,1);
   return 0;
}
   
/* CMD6 -- LOADMOD :: intern_loadmod loads a module */
int intern_loadmod(RCLIENT *client, char *arguments)
{
   /* arguments = module name */
   if (loadmod(client->robotstk,arguments) < 0)
   {
      /* failed */
      sendtoclient(client,"%d %d\n",10,6);
   }
   
   else
   {
      sendtoclient(client,"%d %d\n",11,6);
   }
   return 0;
}
   
/* CMD7 -- ULOADMOD :: intern_uloadmod unloads a module */
int intern_uloadmod(RCLIENT *client, char *arguments)
{
   /* arguments = module name */
      
   if (unloadmod(client->robotstk,arguments) < 0)
   {
      /* failed */
      sendtoclient(client,"%d %d\n",10,7);
   }
   
   else
   {
      sendtoclient(client,"%d %d\n",11,7);
   }
   
   return 0;
}

/* CMD8 -- ULOADALLMODS :: intern_uloadallmod unloads all modules */
int intern_uloadallmods(RCLIENT *client, char *arguments)
{
   /* arguments = N/A */
   unloadallmods(client->robotstk);
   return 0;
}
