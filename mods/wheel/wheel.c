/*******************************************************************************
Copyright (c) 2004, MSU ECE480 F04 Team03, Steve Elgas, Mark Englund
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list
of conditions and the following disclaimer. Redistributions in binary form must
reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the
distribution. Neither the name of the Michigan State University nor the names
of its contributors may be used to endorse or promote products derived from this
software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************/

/* When creating a Module you always need to include these */
#include "main.h"
#include "external.h"
#include <stdio.h>
#include <sys/io.h>

#define WHEEL_TIMEOUT 100000
#define WHEELCMD 36

/* if you want to add a function to RMS. It has to have these two parameters */
/* the fist parameter is a structure to remote client look at main.h */
/* the second parameter is a string that contains some arguments if any */
/* if your module depends on remote arguments then you will need to parse this
   string to get the right values */
   
void receive (RCLIENT *, char *);

/* NOTE: internal commands are supported!  Add their prototypes here. */

void parse_input (char*, int*, int*);

/* This function will be called when the module is loaded */
void register_mod(char *module_name)
{ 
   addcmd (WHEELCMD, receive, module_name);
}

/* This function will be called when the module is unloaded */
void unregister_mod(char *module_name)
{
   /* say good bye */
   
   /* you might wanna add clean up functions for example restore
      arm position to a safe state
   */
}

/* receive the command from the network */
void receive (RCLIENT* remote_client, char* args)
{
  int dir=0, dist=0, n=0;

  /* parse argument string for values */
  parse_input (args, &dir, &dist);

  if(ioperm(0x7A,4,1)<0)
  {
	logit ("io space not addressable");
	sendtoclient(remote_client,"-1/n");
	return;
  }

  outb(0xFF,0x7A);
  outb(0xFF,0x7D);

  switch (dir)
  {
  case 0:				//foward
	  switch(dist)
	  {
	  case 0:
		  outb(0x01,0x7B);
		  outb(0x01,0x7E);
		  break;
	  case 1:
		  outb(0x05,0x7B);
		  outb(0x05,0x7E);
		  break;
	  case 2:
		  outb(0x11,0x7B);
		  outb(0x11,0x7E);
		  break;
	  case 3:
		  outb(0x15,0x7B);
		  outb(0x15,0x7E);
		  break;
	  case 4:
		  outb(0x21,0x7B);
		  outb(0x21,0x7E);
		  break;
	  case 5:
		  outb(0x25,0x7B);
		  outb(0x25,0x7E);
		  break;
	  case 6:
		  outb(0x31,0x7B);
		  outb(0x31,0x7E);
		  break;
	  case 7:
		  outb(0x35,0x7B);
		  outb(0x35,0x7E);
		  break;
	  case 8:
		  outb(0x41,0x7B);
		  outb(0x41,0x7E);
		  break;
	  case 9:
		  outb(0x45,0x7B);
		  outb(0x45,0x7E);
		  break;
	  case 10:
		  outb(0x51,0x7B);
		  outb(0x51,0x7E);
		  break;
	  case 11:
		  outb(0x55,0x7B);
		  outb(0x55,0x7E);
		  break;
	  case 12:
		  outb(0x61,0x7B);
		  outb(0x61,0x7E);
		  break;
	  case 13:
		  outb(0x65,0x7B);
		  outb(0x65,0x7E);
		  break;
	  case 14:
		  outb(0x71,0x7B);
		  outb(0x71,0x7E);
		  break;
	  case 15:
		  outb(0x75,0x7B);
		  outb(0x75,0x7E);
		  break;
	  }
	  break;

  case 1:				//backward
	  switch(dist)
	  {
	  case 0:
		  outb(0x81,0x7B);
		  outb(0x81,0x7E);
		  break;
	  case 1:
		  outb(0x85,0x7B);
		  outb(0x85,0x7E);
		  break;
	  case 2:
		  outb(0x91,0x7B);
		  outb(0x91,0x7E);
		  break;
	  case 3:
		  outb(0x95,0x7B);
		  outb(0x95,0x7E);
		  break;
	  case 4:
		  outb(0xA1,0x7B);
		  outb(0xA1,0x7E);
		  break;
	  case 5:
		  outb(0xA5,0x7B);
		  outb(0xA5,0x7E);
		  break;
	  case 6:
		  outb(0xB1,0x7B);
		  outb(0xB1,0x7E);
		  break;
	  case 7:
		  outb(0xB5,0x7B);
		  outb(0xB5,0x7E);
		  break;
	  case 8:
		  outb(0xC1,0x7B);
		  outb(0xC1,0x7E);
		  break;
	  case 9:
		  outb(0xC5,0x7B);
		  outb(0xC5,0x7E);
		  break;
	  case 10:
		  outb(0xD1,0x7B);
		  outb(0xD1,0x7E);
		  break;
	  case 11:
		  outb(0xD5,0x7B);
		  outb(0xD5,0x7E);
		  break;
	  case 12:
		  outb(0xE1,0x7B);
		  outb(0xE1,0x7E);
		  break;
	  case 13:
		  outb(0xE5,0x7B);
		  outb(0xE5,0x7E);
		  break;
	  case 14:
		  outb(0xF1,0x7B);
		  outb(0xF1,0x7E);
		  break;
	  case 15:
		  outb(0xF5,0x7B);
		  outb(0xF5,0x7E);
		  break;
	  }
	  break;
  case 2:				//left
	  switch(dist)
	  {
	  case 0:
		  outb(0x81,0x7B);
		  outb(0x01,0x7E);
		  break;
	  case 1:
		  outb(0x85,0x7B);
		  outb(0x05,0x7E);
		  break;
	  case 2:
		  outb(0x91,0x7B);
		  outb(0x11,0x7E);
		  break;
	  case 3:
		  outb(0x95,0x7B);
		  outb(0x15,0x7E);
		  break;
	  case 4:
		  outb(0xA1,0x7B);
		  outb(0x21,0x7E);
		  break;
	  case 5:
		  outb(0xA5,0x7B);
		  outb(0x25,0x7E);
		  break;
	  case 6:
		  outb(0xB1,0x7B);
		  outb(0x31,0x7E);
		  break;
	  case 7:
		  outb(0xB5,0x7B);
		  outb(0x35,0x7E);
		  break;
	  case 8:
		  outb(0xC1,0x7B);
		  outb(0x41,0x7E);
		  break;
	  case 9:
		  outb(0xC5,0x7B);
		  outb(0x45,0x7E);
		  break;
	  case 10:
		  outb(0xD1,0x7B);
		  outb(0x51,0x7E);
		  break;
	  case 11:
		  outb(0xD5,0x7B);
		  outb(0x55,0x7E);
		  break;
	  case 12:
		  outb(0xE1,0x7B);
		  outb(0x61,0x7E);
		  break;
	  case 13:
		  outb(0xE5,0x7B);
		  outb(0x65,0x7E);
		  break;
	  case 14:
		  outb(0xF1,0x7B);
		  outb(0x71,0x7E);
		  break;
	  case 15:
		  outb(0xF5,0x7B);
		  outb(0x75,0x7E);
		  break;
	  }
	  break;
  case 3:				//right
	  switch(dist)
	  {
	  case 0:
		  outb(0x01,0x7B);
		  outb(0x81,0x7E);
		  break;
	  case 1:
		  outb(0x05,0x7B);
		  outb(0x85,0x7E);
		  break;
	  case 2:
		  outb(0x11,0x7B);
		  outb(0x91,0x7E);
		  break;
	  case 3:
		  outb(0x15,0x7B);
		  outb(0x95,0x7E);
		  break;
	  case 4:
		  outb(0x21,0x7B);
		  outb(0xA1,0x7E);
		  break;
	  case 5:
		  outb(0x25,0x7B);
		  outb(0xA5,0x7E);
		  break;
	  case 6:
		  outb(0x31,0x7B);
		  outb(0xB1,0x7E);
		  break;
	  case 7:
		  outb(0x35,0x7B);
		  outb(0xB5,0x7E);
		  break;
	  case 8:
		  outb(0x41,0x7B);
		  outb(0xC1,0x7E);
		  break;
	  case 9:
		  outb(0x45,0x7B);
		  outb(0xC5,0x7E);
		  break;
	  case 10:
		  outb(0x51,0x7B);
		  outb(0xD1,0x7E);
		  break;
	  case 11:
		  outb(0x55,0x7B);
		  outb(0xD5,0x7E);
		  break;
	  case 12:
		  outb(0x61,0x7B);
		  outb(0xE1,0x7E);
		  break;
	  case 13:
		  outb(0x65,0x7B);
		  outb(0xE5,0x7E);
		  break;
	  case 14:
		  outb(0x71,0x7B);
		  outb(0xF1,0x7E);
		  break;
	  case 15:
		  outb(0x75,0x7B);
		  outb(0xF5,0x7E);
		  break;
	  }
	  break;
  }

  while (((inb(0x7C) & 0x01) != 1) && ((inb(0x7F) & 0x01) != 1))
  {
	 n++;
	 if(n == WHEEL_TIMEOUT)
	 {
		 logit("motion command failed");
		 sendtoclient(remote_client,"-2/n");
		 outb(0x00,0x7B);
		 outb(0x00,0x7E);
	     return;
	 }
  }

  /* send feedback string */
  sendtoclient (remote_client, "1/n");
  outb(0x00,0x7B);
  outb(0x00,0x7E);
}

/* parse input string for variables */
/* (this function will return zero  */
/* for all values in the event of   */
/* an error)                        */

void parse_input (char* input, int* A, int* B)
{
  char ch;
  int index1=1, index2=1;

  *A = *B = 0;

  /* check validity */
  if (charcount(input, ',') != 2)
    return;

  ch = input[1];    /* get 'A' */
  while (ch != ',')
    ch = input[index2++];
  *A = atoi(substr(input, index1, index2));

  index2++;          /* get 'B' */
  index1 = index2;
  ch = input[index1];
  while (ch != ',')
	ch = input[index2++];
  *B = atoi(substr(input, index1, index2));

}


