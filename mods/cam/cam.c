/* TODO:
   Remove static temp file names
   check multiple cameras
   
   This version of cam_mod is a stripped down version:
   it can only snap and grab pictures.
   
   the full version needs some fixing
*/
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <usb.h>
#include <jpeglib.h>
#include <string.h>

#include "main.h"
#include "external.h"

#include "image/image_from_ppm.h"
#include "image/image_from_jpeg.h"
#include "image/image_trans.h"
#include "image/general.h"
#include "image/image.h"

/* Maximum number of cameras on Bus */
#define MAXCAMS 2
/* Timeout in ms */
#define CAMTIMEOUT 10000
#define JPGTEMPFILE "temp.jpg"
#define PPMTEMPFILE "temp.ppm"

#define CAMERACMD 35

typedef struct Camera
{
   usb_dev_handle *camHandle;
   struct usb_device camDevice;
   struct usb_device camDevice1;
   struct usb_device camDevice2;
   struct usb_config_descriptor camConfig;
   struct usb_interface_descriptor camSetting;
   struct usb_interface camInterface;
   struct usb_endpoint_descriptor camEndpoint;
   struct usb_endpoint_descriptor camBulkInputEndpoint;
   struct usb_endpoint_descriptor camBulkOutputEndpoint;
   struct usb_endpoint_descriptor camInterruptOutputEndpoint;   
}CAMIT;


int findcams(CAMIT *);
usb_dev_handle *startcam(CAMIT *);

void camreset(usb_dev_handle *);
int camsnap(usb_dev_handle *);
int camdellast(usb_dev_handle *);
int camgetpiccount(usb_dev_handle *);
char *camgetpic(usb_dev_handle *, int , int , int , int );
char *fixjpgtoppm(char *, int , int , int , int );
char parseinput(char *);

int OperateCam(RCLIENT *, char *);

/***** Load Module *****/
void register_mod(char *cam)
{    
   addcmd(CAMERACMD, OperateCam, cam);  
}

/***** Unload Module *****/
void unregister_mod(char *module_name)
{
   
}

int OperateCam(RCLIENT *remote_client, char *args)
{
   int numofcams=0;
   CAMIT mycam;
   char *picture = NULL;

   usb_init();
   usb_find_busses();
   usb_find_devices();
   
   numofcams = findcams(&mycam);
   fprintf(stderr,"number of cameras found %d \n", numofcams);
   
   if (numofcams < 1)
   {
      sendtoclient(remote_client, "%d %d\n",10,CAMERACMD);
      logit("Camera Module ERROR: No Cameras Found!");

      return -1;
   }
   
   
   while(numofcams > 0)
   {
      if(numofcams == 2)
      {
         mycam.camDevice = mycam.camDevice2;
      }
      
      if(numofcams == 1)
      {
         mycam.camDevice = mycam.camDevice1;
      }
      
      mycam.camHandle = startcam(&mycam);
      if( mycam.camHandle != NULL)
      {
         logit("Camera Module: Camera [%d] is now ready\n",numofcams);
      }

      else
      {
         sendtoclient(remote_client, "%d %d\n",10,CAMERACMD);
         logit("Camera Module ERROR: Camera not ready!");
         return -1;
      }
           
         
      camsnap(mycam.camHandle);
      picture = camgetpic(mycam.camHandle,
         mycam.camBulkInputEndpoint.bEndpointAddress,160,120,25);
         
      camdellast(mycam.camHandle);

      if (picture != NULL)
      {
         sendtoclient(remote_client,"%d %d ",CAMERACMD,numofcams);
         send(remote_client->fd,picture,1024,0);
         sendtoclient(remote_client,"%c",'\n');
         fprintf(stderr,"got pic from cam[%d]\n",numofcams);
      }
      
      else
      {
         sendtoclient(remote_client,"%d %d\n",10,CAMERACMD);
      }
      
      usb_release_interface(mycam.camHandle,mycam.camSetting.bInterfaceNumber);
      numofcams--;
   }
   
   usb_close(mycam.camHandle);
   return 0;
}

/***** Find Cameras *****/
int findcams(CAMIT *camera)
{
   int camCount=0;
   
   struct usb_bus *bus;
   struct usb_device *dev;
   
   for (bus = usb_busses; bus; bus = bus->next)
   {
      for (dev = bus->devices; dev; dev = dev->next)
      {
         if (
         (dev->descriptor.idVendor == 0xc77) &&
         (dev->descriptor.idProduct == 0x1011) &&
         (camCount < MAXCAMS)
         )
         {
            camCount++;
            if (camCount == 1)
            {
               camera->camDevice1 = *dev;
            }
            if (camCount == 2)
            {
               camera->camDevice2 = *dev;
            }
         }
      }
   }   
   
   return(camCount);
}

/***** Start Cameras *****/
usb_dev_handle *startcam(CAMIT *camera)
{
   int i,j,k,l,urc;  

   for (i = 0; i < camera->camDevice.descriptor.bNumConfigurations; i++)
   {
      camera->camConfig = camera->camDevice.config[i];
      for (j=0;j<camera->camConfig.bNumInterfaces;j++)
      {
         camera->camInterface = camera->camConfig.interface[j];
         for (k=0;k<camera->camInterface.num_altsetting;k++)
         {
            camera->camSetting = camera->camInterface.altsetting[k];
            for (l=0;l<camera->camSetting.bNumEndpoints;l++)
            {
               camera->camEndpoint = camera->camSetting.endpoint[l];
            }
         }
      }
   }
   camera->camConfig = camera->camDevice.config[0];
   camera->camInterface = camera->camConfig.interface[0];
   camera->camSetting = camera->camInterface.altsetting[0];

   for (l=0;l<camera->camSetting.bNumEndpoints;l++)
   {
      camera->camEndpoint = camera->camSetting.endpoint[l];
      if ((camera->camEndpoint.bEndpointAddress & USB_ENDPOINT_DIR_MASK) == USB_ENDPOINT_OUT &&
      (camera->camEndpoint.bmAttributes & USB_ENDPOINT_TYPE_MASK) == USB_ENDPOINT_TYPE_BULK)
      camera->camBulkOutputEndpoint = camera->camEndpoint;
      if ((camera->camEndpoint.bEndpointAddress & USB_ENDPOINT_DIR_MASK) == USB_ENDPOINT_IN &&
      (camera->camEndpoint.bmAttributes & USB_ENDPOINT_TYPE_MASK) == USB_ENDPOINT_TYPE_BULK)
      camera->camBulkInputEndpoint = camera->camEndpoint;
      if ((camera->camEndpoint.bEndpointAddress & USB_ENDPOINT_DIR_MASK) == USB_ENDPOINT_IN &&
      (camera->camEndpoint.bmAttributes & USB_ENDPOINT_TYPE_MASK) == USB_ENDPOINT_TYPE_INTERRUPT)
      camera->camInterruptOutputEndpoint = camera->camEndpoint;
      if ((camera->camEndpoint.bEndpointAddress & USB_ENDPOINT_DIR_MASK) == USB_ENDPOINT_OUT &&
      (camera->camEndpoint.bmAttributes & USB_ENDPOINT_TYPE_MASK) == USB_ENDPOINT_TYPE_INTERRUPT)
      camera->camInterruptOutputEndpoint = camera->camEndpoint;
   }
   camera->camEndpoint = camera->camSetting.endpoint[0];
   camera->camHandle = usb_open(&camera->camDevice);
   
   urc = usb_claim_interface(camera->camHandle, camera->camSetting.bInterfaceNumber);
   if (urc < 0)
   {
      fprintf(stderr,"errno: %d (%s)\n", errno, strerror(errno));
      return(NULL);
   }
   return(camera->camHandle);
}

/***** Snap *****/
int camsnap(usb_dev_handle *camHandle)
{
   unsigned char *bytes=0;
   int urc;
   
   bytes = realloc(bytes, 8);
   urc = usb_control_msg(camHandle,
   USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_IN,
   0x0e, 0x3, 0x0,
   bytes, 8,
   1000);

   if (urc < 0)
   {
      fprintf(stderr,"Error snapping picture (rc:%d)."
      " errno: %d (%s)\n", urc, errno, strerror(errno));
      if(bytes != NULL) { free(bytes); }
      return -1;
   }

   /* sleep(2); */
   if(bytes != NULL) { free(bytes); }
   return 1;
}

/***** Pic Count *****/
int camgetpiccount(usb_dev_handle *camHandle)
{
   unsigned char *bytes=0;
   int urc;
   int pictureCount;

   bytes = realloc(bytes, 8);

   urc = usb_control_msg(camHandle,
   USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_IN,
   0x08, 0x3, 0x0,
   bytes, 8,
   CAMTIMEOUT);

   if (urc < 0)
   {
      fprintf(stderr,"Error counting pictures (rc:%d). "
      "errno: %d (%s)\n", urc, errno, strerror(errno));
      if(bytes != NULL) { free(bytes); }
      return -1;
   }

   pictureCount = bytes[0]*256 + bytes[1];
   fprintf(stderr,"Number of pictures on board is %d\n", pictureCount);

   if(bytes != NULL) { free(bytes); }
   return(pictureCount);
}

/***** Delete Last Pic *****/
int camdellast(usb_dev_handle *camHandle)
{
   unsigned char *bytes=0;
   int urc;
   int count;
   
   count = camgetpiccount(camHandle);
   if (count < 1)
   {
      fprintf(stderr,"ERROR: No pictures on cam\n");
      return -1;
      
   }
   
   bytes = realloc(bytes, 8);
   urc = usb_control_msg(camHandle,
   USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_IN,
   0x11, 0x3, 0x0,
   bytes, 8,
   CAMTIMEOUT);
   
   if (urc < 0)
   {
      fprintf(stderr,"Error deleting picture (rc:%d)."
      " errno: %d (%s)\n", urc, errno, strerror(errno));
      if(bytes != NULL) { free(bytes); }
      return -1;
   }

   if(bytes != NULL) { free(bytes); }
   return 1;
}

/***** Reset *****/
void camreset(usb_dev_handle *camHandle)
{

   unsigned char *bytes=0;
   int urc;

   bytes = realloc(bytes, 8);
   urc = usb_control_msg(camHandle,
   USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_IN,
   0x18, 0x300, 0x0,
   bytes, 6,
   10);

   urc = usb_control_msg(camHandle,
   USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_IN,
   0x04, 0x300, 0x0,
   bytes, 8,
   10);

   if(bytes != NULL) { free(bytes); }
}

/***** Get Pic *****/
char *camgetpic(usb_dev_handle *camHandle, int endpaddr, int width, int height, int quality)
{
   unsigned char *bytes=0;
   char *pic = NULL;
   char *result = NULL;
   int urc, i, type, end, start, length;
   int pictureCount;
   int directorySize;
   FILE *fp;

   pictureCount = camgetpiccount(camHandle);
   
   if (pictureCount < 1)
   {
      fprintf(stderr,"ERROR: No pictures on cam\n");
      return(NULL);
   }

   directorySize = 8 * pictureCount + 8;

   bytes = realloc(bytes, directorySize);
   urc = usb_control_msg(camHandle,
   USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_IN,
   0x0d, 0x3, 0x0,
   bytes, 8,
   CAMTIMEOUT);

   if (urc < 0)
   {
      fprintf(stderr,"Error commanding directory (%d)."
      " errno: %d (%s)\n", urc, errno, strerror(errno));
      if(bytes != NULL) { free(bytes); }
      return(NULL);
   }
   
   urc = usb_bulk_read(camHandle,
   endpaddr,
   bytes,
   directorySize,
   CAMTIMEOUT);

   if (urc < 0)
   {
      fprintf(stderr,"Error reading directory (%d). "
      "errno: %d (%s)\n", urc, errno, strerror(errno));

      if(bytes != NULL) { free(bytes); }
      return(NULL);
   }

   for (i=0; i<pictureCount+1; i++)
   {
      fprintf(stderr,"  %02x %02x %02x %02x  %02x %02x %02x %02x\n", bytes[i*8+0],
      bytes[i*8+1],
      bytes[i*8+2],
      bytes[i*8+3],
      bytes[i*8+4],
      bytes[i*8+5],
      bytes[i*8+6],
      bytes[i*8+7]);
   }

   for (i=0; i<pictureCount; i++)
   {
      type = bytes[8*i+8];
      start = (bytes[8*i+ 5] << 16) | (bytes[8*i+ 6] <<  8) | (bytes[8*i+ 7]);
      end =   (bytes[8*i+13] << 16) | (bytes[8*i+14] <<  8) | (bytes[8*i+15]);
      length = (end - start) / 4;
      fprintf(stderr,"  Entry %d: Type: %d  Start: %d  "
      "End: %d  Length: %d\n", i, type, start, end, length);
   }

   /* Done directory */

   /* Get last picture */
   i = pictureCount-1;
   type = bytes[8*i+8];
   start = (bytes[8*i+ 5] << 16) | (bytes[8*i+ 6] <<  8) | (bytes[8*i+ 7]);
   end =   (bytes[8*i+13] << 16) | (bytes[8*i+14] <<  8) | (bytes[8*i+15]);
   length = (end - start) / 4;

   fprintf(stderr,"  Entry %d: Type: %d  Start: %d  "
   "End: %d  Length: %d\n", i, type, start, end, length);

   fprintf(stderr,"Grabbing last image\n");

   bytes[0] = (start >> 24) & 0xff;
   bytes[1] = (start >> 16) & 0xff;
   bytes[2] = (start >>  8) & 0xff;
   bytes[3] =  start        & 0xff;
   bytes[4] = (length >> 24) & 0xff;
   bytes[5] = (length >> 16) & 0xff;
   bytes[6] = (length >>  8) & 0xff;
   bytes[7] =  length        & 0xff;


   urc = usb_control_msg(camHandle,
   USB_TYPE_VENDOR | USB_RECIP_DEVICE,
   0x0a, 0x3, 0x0,
   bytes, 8,
   CAMTIMEOUT);

   if (urc < 0)
   {
      fprintf(stderr,"Error commanding data dump (rc:%d)."
      " errno: %d (%s)\n", urc, errno, strerror(errno));
      if(bytes != NULL) { free(bytes); }
      return(NULL);
   }

   length *= 8;
   if(length < 0)
   {
      fprintf(stderr,"ERROR: Image size is negative!\n");
      if(bytes != NULL) { free(bytes); }
      return(NULL);
   }

   if ((pic = malloc(length)) == NULL)
   {
      fprintf(stderr,"ERROR: cannot allocate memory for pic\n");
      return(NULL);
   }
   
   urc = usb_bulk_read(camHandle,
   endpaddr,
   pic,
   length,
   CAMTIMEOUT);

   if (urc < 0)
   {
      fprintf(stderr,"Error reading data dump."
      " errno: %d (%s)\n", errno, strerror(errno));
      if(bytes != NULL) { free(bytes); }
      if(pic != NULL) { free(pic); }
      return(NULL);
   }

   fp = fopen("mosa.jpg", "wb");
   fwrite(pic, length, 1, fp);
   fclose(fp);
   
   result = fixjpgtoppm(pic, length, width,  height,  quality);
   if (result == NULL)
   {
      fprintf(stderr,"FUCKED fixjpgtoppm!!!\n");
   }
   else
   {
      fprintf(stderr,"not fucked fixjpgtoppm!!!\n");
   }
   
   return(result);
}

/* for the jpeg decompressor source manager. */
static void _jpeg_init_source(j_decompress_ptr cinfo) {
   fprintf(stderr,"Entering: _jpeg_init_source\n");
}
static boolean _jpeg_fill_input_buffer(j_decompress_ptr cinfo) {
   fprintf(stderr,"Entering: _jpeg_fill_input_buffer\n");
   fprintf(stderr,"(), should not get here.\n");
   return FALSE;
}
static void _jpeg_skip_input_data(j_decompress_ptr cinfo,long num_bytes) {
   fprintf(stderr,"Entering: _jpeg_skip_input_data\n");
   fprintf(stderr,"(%ld), should not get here.\n",num_bytes);
}
static boolean _jpeg_resync_to_restart(j_decompress_ptr cinfo, int desired) {
   fprintf(stderr,"Entering: _jpeg_resync_to_restart\n");
   fprintf(stderr,"(desired=%d), should not get here.\n",desired);
   return FALSE;
}
static void _jpeg_term_source(j_decompress_ptr cinfo) { }

/***** Resize Image *****/
char *fixjpgtoppm(char *ppmdata, int len, int width, int height, int quality)
{
   int ret;
   int rc;
   struct stat sBuff;
   char *convline,*convline2,*rawline;
   FILE *fppmtmp, *fjpgtmp;
   char *ppmtmp, *jpgtmp;
   char *pic;
   int j;
   unsigned int i, pitch;
   struct jpeg_decompress_struct   dinfo;
   struct jpeg_source_mgr          xjsm;
   struct jpeg_error_mgr           jerr;
   image *vimage = NULL, *vimage_resize = NULL;
   
   ppmtmp = PPMTEMPFILE;
   if(access(ppmtmp,F_OK)==0)
   {
      fprintf(stderr,"deleting old temp %s\n",ppmtmp);
      unlink(ppmtmp);
   }

   if ( (fppmtmp = fopen(ppmtmp,"wb")) == NULL)
   {
      fprintf(stderr,"ERROR fixjpgtoppm(): Cannot create tmp file\n");
      return(NULL);
   }

   memset( &dinfo, 0, sizeof(dinfo));
   xjsm.next_input_byte    = ppmdata;
   xjsm.bytes_in_buffer    = len;
   xjsm.init_source        = _jpeg_init_source;
   xjsm.fill_input_buffer  = _jpeg_fill_input_buffer;
   xjsm.skip_input_data    = _jpeg_skip_input_data;
   xjsm.resync_to_restart  = _jpeg_resync_to_restart;
   xjsm.term_source        = _jpeg_term_source;
   dinfo.err               = jpeg_std_error(&jerr);
   jpeg_create_decompress(&dinfo);
   dinfo.src               = &xjsm;
   ret = jpeg_read_header(&dinfo,TRUE);
   if (ret != JPEG_HEADER_OK) {fprintf(stderr,"Error Header\n"); return NULL;}
   dinfo.out_color_space = JCS_RGB;
   jpeg_start_decompress(&dinfo);

   pitch = (dinfo.output_width*dinfo.output_components+3)&~3;
   
   if ((rawline = malloc(pitch)) == NULL)
   {
      fprintf(stderr,"ERROR: cannot allocate memory for rawline\n");
      return(NULL);
   }
   
   if ((convline = malloc(dinfo.output_width*2*3)) == NULL)
   {
      fprintf(stderr,"ERROR: cannot allocate memory for convline\n");
      return(NULL);
   }
   
   if ((convline2 = malloc(dinfo.output_width*2*2*3)) == NULL)
   {
      fprintf(stderr,"ERROR: cannot allocate memory for convline\n");
      return(NULL);
   }
   

   fprintf(fppmtmp,"P6\n%d %d 255\n",dinfo.output_width, dinfo.output_height*2);
   for (i = 0; i < dinfo.output_height ; i++ )
   {
      jpeg_read_scanlines(&dinfo,(JSAMPARRAY)&rawline,1);

      memcpy(convline+((dinfo.output_width/16-1)*16+8)*3, rawline+((dinfo.output_width/16-1)*16+8)*3, 8*3);
      memcpy(convline+pitch/2, rawline, 8*3);
      for (j=0;j<(dinfo.output_width/16-1);j++) {
         if ((j&1) == 0)
         memcpy(convline+((j/2)*16)*3,rawline+(j*16+8)*3,16*3);
         else
         memcpy(convline+pitch/2+((j/2)*16+8)*3,rawline+(j*16+8)*3,16*3);
      }
      for (j=0;j<dinfo.output_width*2;j++) {
         memcpy(convline2+ j*2*3   , convline+j*3,3);
         memcpy(convline2+(j*2+1)*3, convline+j*3,3);
      }
      fwrite(convline2, pitch, 2, fppmtmp);
   }
   free(convline2);
   free(convline);
   free(rawline);
   jpeg_destroy_decompress(&dinfo);
   fflush(fppmtmp);
   fclose(fppmtmp);

   vimage = image_new_from_ppm(ppmtmp);
   if ( vimage == NULL)
   {
      fprintf(stderr,"ERROR loading ppm file!\n");
      return(NULL);
   }
   
   vimage_resize = image_new(width,height);

   if ( vimage_resize == NULL)
   {
      fprintf(stderr,"ERROR cannot create resized!\n");
      return(NULL);
   }
   
   image_resize_bilinear(vimage_resize,vimage,width,height);
   
   jpgtmp = JPGTEMPFILE;
   
   if(access(jpgtmp,F_OK)==0)
   {
      fprintf(stderr,"deleting old temp %s\n",jpgtmp);
      unlink(ppmtmp);
   }
   
   if(image_save_to_jpeg(vimage_resize, jpgtmp,quality) < 0)
   {
      fprintf(stderr,"ERROR creating jpg\n");
      return(NULL);
   }

   image_save_to_jpeg(vimage_resize, "after.jpg",quality);

   rc = stat(jpgtmp, &sBuff);
   len = sBuff.st_size;

   if ( (fjpgtmp = fopen(jpgtmp, "rb")) == NULL)
   {
      fprintf(stderr,"ERROR fixjpgtoppm(): Cannot read jpgtmp file\n");
      return(NULL);
   }

   pic = malloc(len);
   
   if ((pic = malloc(len)) == NULL)
   {
      fprintf(stderr,"ERROR: cannot allocate memory for pic\n");
      return(NULL);
   }
   
   fread(pic, len, 1, fjpgtmp);
   fclose(fjpgtmp);
   
   fprintf(stderr,"saved result!\n");
   
   unlink(jpgtmp);
   unlink(ppmtmp);
   return(pic);
}

