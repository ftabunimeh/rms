### Future Improvements (TODO)
1- Create a more robust RMS that can catch module's Seg faults
   using setjmp() longjmp() and sigaction()
2- Due to the fact that some modules need to do long processing
   and RMS is single process/single thread, RMS will need to add either
   A- spawning new new processing/threads for modules
   B- OR create a timer function that would allow a call-back functionality.
   This way RMS will not halt untill a module finishs its process. however
   It will continue accepting new commands.