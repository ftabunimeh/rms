/*******************************************************************/
/* Bibliotheque de manipulation d'image au format TrueColor 32bitd */
/* image.h                                                         */
/*                                                                 */
/* Ecrit par : Daniel Lacroix (all rights reserved)                */
/*                                                                 */
/*******************************************************************/

#ifndef __IMAGE_H__
#define __IMAGE_H__

#include "general.h"
#include "color.h"

/* definition d'un pixel d'une image */
typedef uint32 pix;
/* Les differentes couleurs RGB   */
/* sont positionnees comme suit : */
/* rouge 0xFF000000               */
/* vert  0x00FF0000               */
/* bleu  0x0000FF00               */
/* trans 0x000000FF               */

/* definition d'une image */
typedef struct {
  /* dimensions de l'image */
  int32 width,height;
  /* buffer en mode 32 bits (quelque soit la valeur reel de l'ecran) */
  pix    *buf;
} image;

/* renvoi les dimensions de l'image */
void image_size(image *pimage, int32 *width, int32 *height);

/* cree une nouvelle image */
image *image_new(int32 width, int32 height);

/* libere une image */
void image_free(image *pimage);

/* copy l'image psrc dans pdest, les deux images */
/* doivent avoir la meme taille                  */
void image_copy(image *pdest, image *psrc);

/* (x,y) = position dans pdest ou est colle psrc    */
void image_paste(image *pdest, image *psrc, int32 x, int32 y);

/* (x,y) = position dans pdest ou est incruste psrc */
/* et col la couleur d'incruste de psrc             */
void image_incruste(image *pdest, image *psrc, int32 x, int32 y, pix col);

/* (x,y) = position dans pdest ou est incruste psrc */
/* et col la couleur d'incruste de psrc. rapport l' */
/* intensite de psrc sur 256 par rapport a pdest    */
void image_transparent(image *pdest, image *psrc, int32 x, int32 y, pix col,
  uint16 rapport);

/* converti l'image psrc en noir et blanc dans pdest */
void image_gris(image *pdest, image *psrc, int32 x, int32 y);

/* (x,y) = position dans pdest ou est colle psrc    */
void image_paste_with_alpha(image *pdest, image *psrc, int32 x, int32 y);

/* (x,y) = position dans pdest ou psrc est utilise comme alpha */
void image_set_alpha_with_grey(image *pdest, image *psrc, int32 x, int32 y);

#endif /* __IMAGE_H__ */
