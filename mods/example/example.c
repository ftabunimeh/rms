/*******************************************************************************
Copyright (c) 2004, MSU ECE480 F04 Team01, Faisal Abu-Nimeh
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list
of conditions and the following disclaimer. Redistributions in binary form must
reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the
distribution. Neither the name of the Michigan State University nor the names
of its contributors may be used to endorse or promote products derived from this
software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************/

/* When creating a Module you always need to include these */
#include "main.h"
#include "external.h"

/* if you want to add a function to RMS. It has to have these two parameters */
/* the fist parameter is a structure to remote client look at main.h */
/* the second parameter is a string that contains some arguments if any */
/* if your module depends on remote arguments then you will need to parse this
   string to get the right values */
   
int example1(RCLIENT *, char *);
int example2(RCLIENT *, char *);

/* internal functions that are not registion with main program */
void internal(char *);
int cleanup();


/* NOTE: you can create internal functions with different parameters that won't
         be added to RMS.
*/

/* This function will be called when the module is loaded */
void register_mod(char *module_name)
{ 
   /* 
      addcmd() accepts three parameters:
      
      1- the numeric of the command: it has to be added to CMDS configuration
         file please check config/ directory.
      2- the name of the function that is associated with the command.
      3- the module name: this is always passed to you.
      
      EXAMPLE_MODULE_CMD1 = 33
      EXAMPLE_MODULE_CMD2 = 34
      
   */
   addcmd(33,example1, module_name);
   addcmd(34,example2, module_name);
}

/* This function will be called when the module is unloaded */
void unregister_mod(char *module_name)
{
   /* say good bye */
   
   /* you might wanna add clean up functions for example restore
      arm position to a safe state
   */
   cleanup();
}

/* example of how you can use sckstrct to reply to the client */
int example1(RCLIENT *remote_client, char *passed_arguments)
{
   int i = 20;
   char *test = "string test";
   
   /* 
     seperator between passed arguments. this can be anything like ',' ';' etc
   */
   char delimiter = '+';
   
   /* 
     total number of tokens pass in arguments. useful for parsing 
   */
   int num_of_toks; 
   
   /* example tokens */
   char *tok1;
   char *tok2;
   char *tok3;
   
   /* *** Sending data back to remote client
   
      sendtoclient is capable of accepting formatted arguments
      sendtoclient is responsible for all outgoing data
   */
   sendtoclient(remote_client,
                "this is an example. u can pass results like [i=%d] and "
                "[test=%s]\n",i,test);
   
   /* *** loggin activities such as cruicial errors
      you do not need to log everything this will result in a huge log file!
   */
   logit("Example Module: remote client [IP address %s] excuted example1()"
         " with arguments [%s]",remote_client->address,passed_arguments);
         
   /* parsing input
      a custom string library is provided so you can parse and get tokens out
      of the incoming string.
   */
   
   /* example string = "Hello+My+Friend" */
   num_of_toks = charcount(passed_arguments,delimiter) + 1;
   /* number of delimiters is 2. number of tokens are 3 which is (2+1) */
   
   /* you can use a while loop to go thru all of them. or manually like */
   tok1 = gettok(passed_arguments, delimiter, 1); /* Hello */
   tok2 = gettok(passed_arguments, delimiter, 2); /* My */
   tok3 = gettok(passed_arguments, delimiter, 3); /* Friend */
   
   /*
      NOTE: if the string had a \n at the end you need to trim before you 
            tokenize. this can be done by calling rmchar() for example:
            
              rmchar(passed_arguments,'\n');
              rmchar(passed_arguments,'\r');
   */
            
   return 0;
}

int example2(RCLIENT *remote_client, char *passed_arguments)
{
#ifdef DEBUG
   /* having printf's in modules is useless unless you are debugging! */
   printf("example2 was called. extra args are [%s]\n",passed_arguments);
#endif


   internal(passed_arguments);
   
   sendtoclient(remote_client,"Client [IP %s] called example2()\n",
                              remote_client->address);
                              
   return 0;
}

void internal(char *string)
{
   /* internal function that is not seen by outside */
   return;
}

int cleanup()
{
   /* delete files, stop hardware, etc */
   return 1;
}

