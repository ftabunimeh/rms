/*******************************************************************************
Copyright (c) 2004, MSU ECE480 F04 Team01, Faisal Abu-Nimeh
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list
of conditions and the following disclaimer. Redistributions in binary form must
reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the
distribution. Neither the name of the Michigan State University nor the names
of its contributors may be used to endorse or promote products derived from this
software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************/

/*
  Changes:
  10/27/04 [Faisal]: Deleted startlog() endlog() and global logptr
                     now logit() handles all the work.
  10/28/04 [Faisal]: RMS no longer exits if log file failed to open
                     all will be reported to stderr.
  10/29/04 [Faisal]: fixed not enough memory problem
  11/03/04 [Faisal]: rewrote logit() function.
  11/15/04 [Faisal]: fclose() is only called if fopen() succeeded.
  11/18/04 [Faisal]: Added checking to NULL input
  
*/

#include "main.h"
#include "external.h"

/*******************************************************************************
   Function: logit writes back parameters to defined log file
   Inputs: formatted text input
   Outputs: writes text to log file with time stamp and '\n' at the end
   Returns: VOID
*******************************************************************************/
void logit(char *format, ...)
{
   int len=0;
   char ftime[MIDBUF];
   char *buf = NULL;
   FILE *tmp;
   FILE *logfileptr = NULL;
   va_list args;
   time_t now;
   
   if (NULL == format)
   {
      return;
   }
   
   va_start(args,format);
   
   logfileptr = fopen(LOG, "a+");
	time(&now); /* get time */
	if(NULL == logfileptr) /* check if log file ptr exists */
	{
	   fprintf(stderr, "ERROR: log file [%s] could not be openned!\n", LOG);
	   vfprintf(stderr,format,args);
	}
	
	else
	{
	   if((tmp = tmpfile()) != NULL)
	   {
	      len = vfprintf(tmp, format, args) + 1;
	      fclose(tmp);
	      
	      if (len <= 1)
	      {
	         /* print log on stderr */
	         fprintf(stderr, "ERROR: writing to temp log file failed!\n");
	         vfprintf(stderr,format,args); fprintf(stderr, "\n");
	      }
	      
	      else
	      {
	         buf = malloc(len * sizeof(char)); /* allocate memory for buffer */
	         if (NULL == buf) /* check sufficient memory */
	         {
	            /* print log on stderr */
	            fprintf(stderr, "ERROR: Log file buffer failed!\n");
	            vfprintf(stderr,format,args); fprintf(stderr, "\n");
	         }
	         
	         else
	         {
	            vsprintf(buf, format, args);
	            /* create formatted time */
	            strftime(ftime, MIDBUF, "%a, %b %d, %Y at %I:%M:%S %p",
                  localtime(&now));
               /* write to log file */
               fprintf(logfileptr, "[%s]: %s\n", ftime, buf);
               free(buf); /* keep it clean */
            }
         }
      }
      
      else
      {
         /* print log on stderr */
         fprintf(stderr, "ERROR: Temp Log file failed!\n");
         vfprintf(stderr,format,args); fprintf(stderr, "\n");
      }
      fflush(logfileptr);
      fclose(logfileptr);
   }
   
   va_end(args);
}
