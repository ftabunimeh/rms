/*******************************************************************************
Copyright (c) 2004, MSU ECE480 F04 Team01, Faisal Abu-Nimeh
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list
of conditions and the following disclaimer. Redistributions in binary form must
reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the
distribution. Neither the name of the Michigan State University nor the names
of its contributors may be used to endorse or promote products derived from this
software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************/

/*
  Changes:
  10/31/04 [Faisal]: addcmd() now checks if command already exists.
  10/31/04 [Faisal]: added new function delmodcmds()
  11/03/04 [Faisal]: rewrote runcmd()
  11/13/04 [Faisal]: runcmd() now uses the new remote client linked list.
  11/14/04 [Faisal]: - new funcs readcmdconf(), addinternalcmd(), findcmdname()
                     - commands are not located in cmds.h anymore
                     - all commands will be fetched from config file now
                     - command comparisons is based on command numeric not name
                       this is faster because comparing ints is faster than
                       comparing strings.
  11/15/04 [Faisal]: - fixed a bug in delmodcmds() that caused an infinite loop.
                     - readcmdconf() now uses malloc instead of an array
  11/16/04 [Faisal]: - Removed cmdname from cmd struct. All commands are handled
                       by their numeric. This way less memory is used and faster
                       search results when looking for a command.
                     - Added va_end() to runcmd() before each return.
  11/18/04 [Faisal]: - runcmd()accepts NULL as formatted argument.
                     - delmodcmds() will not delete the command from cmd list
                       it will only unassociate the module linked to it.
  12/05/04 [Faisal]: Added new commands 9 10 and 11
*/

#include "main.h"
#include "external.h"

void assigncmd(char *);
void addinternalcmd();
int addcmd(int , int (*)() ,char *);
void delcmd(int );
CMD *findcmd(int );
int runcmd(RCLIENT *, int , char *, ...);
void delmodcmds(char *);

static CMD *cmd_head = NULL;
static CMD *cmd_tail = NULL;
static CMD *cmd_curr = NULL;

/*******************************************************************************
   Function: opens commands configuration file for reading and parses its tags
   Inputs: VOID
   Outputs: calls assigncmd() to assign commands to numerics 
   Returns: VOID
*******************************************************************************/
void readcmdconf(char *cmdsf)
{
   char *buf;
   char *cmd;
   FILE *cmdsfile = NULL;
   
   logit("Reading Commands list configuration file [%s]",cmdsf);
   
   cmdsfile = fopen(cmdsf, "r");

   if (NULL == cmdsfile)
   {
		exitrobot("ERROR: CMDs conf file could not be openned!", -1);
	}
	
	buf = malloc(MIDBUF * sizeof(char)); /* allocate memory for buffer */
	if (NULL == buf) /* check sufficient memory */
	{
	   exitrobot("ERROR: Unable to readcmdconf() Not enough memory!",-1);
	}
	
	while ( (!feof(cmdsfile)) && (NULL != (fgets(buf, MIDBUF, cmdsfile))) )
	{
	   if(isdigit(buf[0]))
	   {
	      /* trim fgets left overs */
	      rmchar(buf,'\n');
	      rmchar(buf,'\r');
	      
	      cmd = gettok(buf, CONFDEL, 1);
	      assigncmd(cmd);
	   }

	}
	fclose(cmdsfile);
	logit("Done reading [%s]",cmdsf);
	free(buf);
}

/*******************************************************************************
   Function: assigncmd assigns a command to a numeric
   Inputs: cmdnum and cmdname
   Outputs: VOID
   Returns: VOID
*******************************************************************************/
void assigncmd(char *cmdnum)
{
   int num;
   CMD *cmd_tmp;
   
   /* convert string to int */
   num = (int)strtol(cmdnum,NULL,10); /* base 10 */
   if (num <= 0)
   {
      logit("ERROR: Ignored invalid command [%s]",cmdnum);
      return;
   }
   
   /* check if command is duplicated */
   cmd_tmp = (CMD *)findcmd(num);
   if(cmd_tmp != NULL)
   {
      logit("ERROR: Ignored Command. already exits [%d]",num);
      return;
   }
   /* assign memory */
   cmd_curr = (CMD *)malloc(sizeof(CMD));
	if (cmd_curr == NULL)
	{
		logit("ERROR: Cannot add command [%d] Not enough memory",num);
		return;
	}
	
	cmd_curr->cmd = num;
	
	cmd_curr->func = NULL; /* will be assigned later */
	cmd_curr->module = NULL;
	
	cmd_curr->next = cmd_head;
	cmd_curr->prev = NULL;
	
	if (cmd_tail == NULL)
	{ cmd_tail = cmd_curr; }
	
	else
	{ cmd_curr->next->prev = cmd_curr; }
	
	cmd_head = cmd_curr;
	
	/* Add internal commands now */
	if ( (num > 0 ) && (num < 33) )
	{
	   addinternalcmd();
	}
}

/*******************************************************************************
   Function: addinternalcmd adds a internal RMS command to list
   Inputs: VOID -- this function will always handle the head of the list
                   because it is called after a successful assignment
   Outputs: VOID
*******************************************************************************/
void addinternalcmd()
{
   /* NULL */
   if (1 == cmd_head->cmd)
   {
      cmd_head->func = intern_null;
      cmd_head->module = NULL;
   }
   
   /* UPTIME */
   else if (2 == cmd_head->cmd)
   {
      cmd_head->func = intern_uptime;
      cmd_head->module = NULL;
   }
   
   /* REHASH */
   else if (3 == cmd_head->cmd)
   {
      cmd_head->func = intern_rehash;
      cmd_head->module = NULL;
   }
   
   /* SETTINGS */
   else if (4 == cmd_head->cmd)
   {
      cmd_head->func = intern_settings;
      cmd_head->module = NULL;
   }
   
   /* EXIT */
   else if (5 == cmd_head->cmd)
   {
      cmd_head->func = intern_exit;
      cmd_head->module = NULL;
   }
   
   /* LOADMOD */
   else if (6 == cmd_head->cmd)
   {
      cmd_head->func = intern_loadmod;
      cmd_head->module = NULL;
   }
   
   /* ULOADMOD */
   else if (7 == cmd_head->cmd)
   {
      cmd_head->func = intern_uloadmod;
      cmd_head->module = NULL;
   }
   
   /* ULOADALLMODS */
   else if (8 == cmd_head->cmd)
   {
      cmd_head->func = intern_uloadallmods;
      cmd_head->module = NULL;
   }
   
   /* Login */
   else if (9 == cmd_head->cmd)
   {
      /* Handled in network.c -- high priority */ 
   }
   
   /* CMDFAILD */
   else if (10 == cmd_head->cmd)
   {
      /* Feedback Command */
   }
   
   /* CMDSUCCS */
   else if (11 == cmd_head->cmd)
   {
      /* Feedback Command */
   }
   
   
   /* Unknown */
   else
   {
      logit("ERROR: Unknown internal command [%d]",
            cmd_head->cmd);
      
      delcmd(cmd_head->cmd); /* why keep it in memory? */
   }
}

/*******************************************************************************
   Function: addcmd adds a CMD to the command link list
   Inputs: cmd, call back function, module if any
   Outputs: VOID
   Returns: -2 command already in use,-1 if failed, 1 if succeeded.
*******************************************************************************/
int addcmd(int cmd, int (*func)(), char *module)
{
   /* 
      The reason why addmod now accepts cmdname instead of cmd numeric is:
      
      - It is more user friend to pass a name of a command. It makes it easier
        for people who are writing modules.
      - Passing cmd numeric is way faster to search and less bytes passed in 
        memory. I would rather pass numerics! -- Faisal
    */
      
   CMD *cmd_tmp;
   cmd_tmp = (CMD *)findcmd(cmd);
   
   if (NULL == cmd_tmp)
   {
      logit("ERROR: Module [%s] is trying to add a command [%d] that does not"
            " exists in CMDs configuration file!",module,cmd);
      return -1;
   }
   
   /* check if command is already in use */
   if (cmd_tmp->func != NULL)
   {
      logit("ERROR: Module [%s] is trying to add a command [%d] that is "
            "already in use!",module,cmd);
      return -2;
   }
   
   /* assign command to module */
   cmd_tmp->func = func;
   cmd_tmp->module = module;
   
   logit("Module [%s] has added command [%d] to list",module,cmd);
   return 1;
}

/*******************************************************************************
   Function: delcmd deletes a CMD from the global linked list
   Inputs: cmd
   Outputs: VOID
   Returns: VOID
*******************************************************************************/
void delcmd(int cmd)
{
   CMD *cmd_tmp;
   
   cmd_tmp = (CMD *)findcmd(cmd);
   
   if(cmd_tmp != NULL) /* found */
   {
      /* Make sure its not a head or a tail */
      if( (cmd_tmp->next != NULL) && (cmd_tmp->prev != NULL) )
      {
         cmd_tmp->prev->next = cmd_tmp->next;
         cmd_tmp->next->prev = cmd_tmp->prev;
      }
      
      /* either head or tail or both */
      else
      {
         /* Both head and tail == one element in list */
         
         if( (cmd_tmp == cmd_head) && (cmd_tmp == cmd_tail) )
         {
            cmd_head = NULL;
            cmd_tail = NULL;
         }
         
         /* either one */
         else
         {
            /* head only */
            if(cmd_tmp == cmd_head)
            {
               cmd_tmp->next->prev = NULL;
               cmd_head = cmd_tmp->next;
            }
            
            /* tail only */
            else if(cmd_tmp == cmd_tail)
            {
               cmd_tmp->prev->next = NULL;
               cmd_tail = cmd_tmp->prev;
            }
            
            /* else */
            /* WTF ? */
         }
      }
      free(cmd_tmp);
   }
}

/*******************************************************************************
   Function: findcmd will look up in linked list for cmd
   Inputs: cmd
   Outputs: VOID
   Returns: pointer to element in list if found
            NULL pointer if not found
*******************************************************************************/
CMD *findcmd(int cmd)
{
   CMD *cmd_tmp = cmd_head;
   
   while(cmd_tmp != NULL)
   {
      if(cmd_tmp->cmd == cmd)
      {
         return(cmd_tmp);
      }
      cmd_tmp = cmd_tmp->next;
   }
   return(NULL);
}

/*******************************************************************************
   Function: runcmd will look up in linked list for cmd then run it
   Inputs: cmd, formatted string to be passed to cmd
   Outputs: VOID
   Returns: -2 if failed, -1 if not found, 1 otherwise
*******************************************************************************/
int runcmd(RCLIENT *client, int cmd, char *format, ...)
{
   int len;
   int tmp;
   char *buf;
   FILE *tempf;
   CMD *cmd_tmp;
   va_list args;
   
   /* check if passed arguments are NULL */
   if (NULL == format)
   {
      format = "";
   }
   
   cmd_tmp = (CMD *)findcmd(cmd); /* check if command exists */
   if (NULL == cmd_tmp)
   {
      logit("ERROR: Client [%s] is trying to run an unknown command [%d]!",
            client->address,cmd);
      return -1;
   }
   
   if (cmd_tmp->func == NULL)
   {
      logit("ERROR: Client [%s] is trying to run a command [%d] that is not"
            " associated with any module!",client->address,cmd);
      return -2;
   }
   
   
	va_start(args, format);
	if((tempf = tmpfile()) != NULL)
	{
	   len = vfprintf(tempf, format, args) + 1;
	   fclose(tempf);
	   if (len < 1)
	   {
	      logit("ERROR: writing to cmd temp file failed!");
	      va_end(args);
	      return -2;
	   }
	   
	   buf = malloc(len * sizeof(char)); /* allocate memory for buffer */
	   
	   if (NULL == buf) /* check sufficient memory */
	   {
	      logit("ERROR: Not enough memory! cannot run cmd");
	      
	      va_end(args);
	      return -2;
	   }
	   
	   vsprintf(buf, format, args);
	   tmp = cmd_tmp->func(client,buf);
	   /* if (tmp) TODO: check return values for logging */
	   
	   free(buf);
	   va_end(args);
	   return 1;
	}
	
	logit("ERROR: cmd temp file failed!");
	
	va_end(args);
	return -2;
}

/*******************************************************************************
   Function: delmodcmds will only unassociate a command with a module
             it will not delete the actually command from list.
   Inputs: module name
   Outputs: VOID
   Returns: VOID
*******************************************************************************/
void delmodcmds(char *modname)
{
   CMD *cmd_tmp = cmd_head;
   
   /* some commands might be internal -- check for module name as well */
   while( (cmd_tmp != NULL) && (cmd_tmp->module != NULL) )
   {
      if(0 == strncmp(modname,cmd_tmp->module,strlen(cmd_tmp->module)))
      {
         	cmd_tmp->func = NULL;
         	cmd_tmp->module = NULL;
      }
      cmd_tmp = cmd_tmp->next;
   }
}
