/**********************************************************************/
/* Bibliotheque de transformation d'images au format TrueColor 32bitd */
/* image_trans.c                                                      */
/*                                                                    */
/* Ecrit par : Daniel Lacroix (all rights reserved)                   */
/*                                                                    */
/**********************************************************************/


#include <math.h>
#include <stdio.h>
#include "image_trans.h"
#include "image_draw_func.h"

/***************************************************/
/* Redimentionne l'image psrc et place le resultat */
/* a la postion (0,0) dans pdest en prenant        */
/* toujours le point le plus proche.               */
void image_resize_nearest(image *pdest, image *psrc,
  int32 width, int32 height)
{ int32 vx,vy;
  pix   vcol;
  float rx,ry;
  float width_scale, height_scale;

  width_scale  = (float)psrc->width  / (float)width;
  height_scale = (float)psrc->height / (float)height;

  for(vy = 0;vy < height; vy++)  
  {
    for(vx = 0;vx < width; vx++)
    {
      rx = vx * width_scale;
      ry = vy * height_scale;
      vcol = get_pix(psrc, (int32)rx, (int32)ry);
      put_pix_alpha_replace(pdest, vx, vy, vcol);
    }
  }
}
/***************************************************/

/***************************************************/
/* Redimentionne l'image psrc et place le resultat */
/* a la postion (0,0) dans pdest en fusionnant les */
/* points. L'image r�sultat doit �tre plus petite  */
/* que l'image d'origine.                          */
void image_downsize(image *pdest, image *psrc,
  int32 width, int32 height)
{ int32 vx,vy;
  pix   vcol;
  float rx,ry;
  float width_scale, height_scale;
  float red, green, blue, alpha;
  int   i,j;
  int32 half_square_width, half_square_height;
  float round_width, round_height;

  if((pdest == NULL) || (psrc == NULL) || (psrc->width < width) ||
     (psrc->height < height))
    return;

  width_scale  = (float)psrc->width  / (float)width;
  height_scale = (float)psrc->height / (float)height;

  half_square_width  = (int32)(width_scale  / 2.0);
  half_square_height = (int32)(height_scale / 2.0);
  round_width  = (width_scale  / 2.0) - (float)half_square_width;
  round_height = (height_scale / 2.0) - (float)half_square_height;
  if(round_width  > 0.0)
    half_square_width++;
  else
    round_width = 1.0;
  if(round_height > 0.0)
    half_square_height++;
  else
    round_height = 1.0;

  for(vy = 0;vy < height; vy++)  
  {
    for(vx = 0;vx < width; vx++)
    {
      

      rx = vx * width_scale;
      ry = vy * height_scale;
      vcol = get_pix(psrc, (int32)rx, (int32)ry);

      red = green = blue = alpha = 0.0;

      for(j=0;j<half_square_height*2;j++)
      {
        for(i=0;i<half_square_width*2;i++)
        {
          vcol = get_pix(psrc, ((int32)rx)-half_square_width+i,
            ((int32)ry)-half_square_height+j);
          
          if(((j == 0) || (j == (half_square_height*2)-1)) && 
             ((i == 0) || (i == (half_square_width*2)-1))) {
            red   += round_width*round_height*(float)COL_RED  (vcol);
            green += round_width*round_height*(float)COL_GREEN(vcol);
            blue  += round_width*round_height*(float)COL_BLUE (vcol);
            alpha += round_width*round_height*(float)COL_ALPHA(vcol);
          } else if((j == 0) || (j == (half_square_height*2)-1)) {
            red   += round_height*(float)COL_RED  (vcol);
            green += round_height*(float)COL_GREEN(vcol);
            blue  += round_height*(float)COL_BLUE (vcol);
            alpha += round_height*(float)COL_ALPHA(vcol);
          } else if((i == 0) || (i == (half_square_width*2)-1)) {
            red   += round_width*(float)COL_RED  (vcol);
            green += round_width*(float)COL_GREEN(vcol);
            blue  += round_width*(float)COL_BLUE (vcol);
            alpha += round_width*(float)COL_ALPHA(vcol);
          } else {
            red   += (float)COL_RED  (vcol);
            green += (float)COL_GREEN(vcol);
            blue  += (float)COL_BLUE (vcol);
            alpha += (float)COL_ALPHA(vcol);
          }
        }
      }
      
      red   /= width_scale*height_scale;
      green /= width_scale*height_scale;
      blue  /= width_scale*height_scale;
      alpha /= width_scale*height_scale;
      
      /* on sature les valeurs */
      red   = (red   > 255.0)? 255.0 : ((red   < 0.0)? 0.0:red  );
      green = (green > 255.0)? 255.0 : ((green < 0.0)? 0.0:green);
      blue  = (blue  > 255.0)? 255.0 : ((blue  < 0.0)? 0.0:blue );
      alpha = (alpha > 255.0)? 255.0 : ((alpha < 0.0)? 0.0:alpha);
      
      put_pix_alpha_replace(pdest, vx, vy,
        COL_FULL((uint8)red,(uint8)green,(uint8)blue,(uint8)alpha));
    }
  }
}
/***************************************************/

/***************************************************/
/* Redimentionne l'image psrc et place le resultat */
/* a la postion (0,0) dans pdest. On determine les */
/* points par interpolation bilinaire.             */
void image_resize_bilinear(image *pdest, image *psrc,
  int32 width, int32 height)
{ int32 vx,vy;
  pix   vcol,vcol1,vcol2,vcol3,vcol4;
  float rx,ry;
  float width_scale, height_scale;
  float x_dist, y_dist;

  width_scale  = (float)psrc->width  / (float)width;
  height_scale = (float)psrc->height / (float)height;

  for(vy = 0;vy < height; vy++)  
  {
    for(vx = 0;vx < width; vx++)
    {
      rx = vx * width_scale;
      ry = vy * height_scale;
      vcol1 = get_pix(psrc, (int32)rx, (int32)ry);
      vcol2 = get_pix(psrc, ((int32)rx)+1, (int32)ry);

      vcol3 = get_pix(psrc, (int32)rx, ((int32)ry)+1);
      vcol4 = get_pix(psrc, ((int32)rx)+1, ((int32)ry)+1);

      x_dist = rx - ((float)((int32)rx));
      y_dist = ry - ((float)((int32)ry));
      vcol = COL_FULL(
        (uint8)((COL_RED(vcol1)*(1.0-x_dist)
          + COL_RED(vcol2)*(x_dist))*(1.0-y_dist)
          + (COL_RED(vcol3)*(1.0-x_dist)
          + COL_RED(vcol4)*(x_dist))*(y_dist)),
        (uint8)((COL_GREEN(vcol1)*(1.0-x_dist)
          + COL_GREEN(vcol2)*(x_dist))*(1.0-y_dist)
          + (COL_GREEN(vcol3)*(1.0-x_dist)
          + COL_GREEN(vcol4)*(x_dist))*(y_dist)),
        (uint8)((COL_BLUE(vcol1)*(1.0-x_dist)
          + COL_BLUE(vcol2)*(x_dist))*(1.0-y_dist)
          + (COL_BLUE(vcol3)*(1.0-x_dist)
          + COL_BLUE(vcol4)*(x_dist))*(y_dist)),
        (uint8)((COL_ALPHA(vcol1)*(1.0-x_dist)
          + COL_ALPHA(vcol2)*(x_dist))*(1.0-y_dist)
          + (COL_ALPHA(vcol3)*(1.0-x_dist)
          + COL_ALPHA(vcol4)*(x_dist))*(y_dist))
      );
      put_pix_alpha_replace(pdest, vx, vy, vcol);
    }
  }
}
/***************************************************/

/***************************************************/
/* Redimentionne l'image psrc et place le resultat */
/* a la postion (0,0) dans pdest. On determine les */
/* points par interpolation avec une DCT 2D des 64 */
/* points les plus pres.                           */
void image_resize_dct(image *pdest, image *psrc, int32 width, int32 height)
{
  float dct_coeff[8][8];  /* 8 fonctions de bases definie par 8 valeurs */
  uint8 val[8][8];
  float dct_res[8][8];    /* coefficient 3d qui correspondent a nos pixels */
  float dc_coeff;
  
  /* initialise le tableau des fonctions de bases */
  double f1,f2,tmp;
  int x,y,i1,i2;

  boolean do_dct = TRUE;
  int32 vx,vy;
  float rx,ry,last_rx,last_ry;
  float width_scale, height_scale;
  float x_dist, y_dist;
  pix   vcol = BLACK;
  
  uint32 nb_pix;
  float  percent_done,percent_tmp;

  void init_dct(void)
  {
    f1 = 2.0*atan(1.0)/8.0;
    for(y=0;y<8;y++){         /* de la fonction 0 a la 7              */
      f2=(double)y*f1;
      for(x=0;x<8;x++){       /* du point 0 au point 7 de la fonction */
        tmp=cos((double)(2*x+1)*f2);
        if(y==0) dct_coeff[y][x]=(float)(1.0/sqrt(2.0));
        else     dct_coeff[y][x]=(float)tmp;
      }
    }
    dc_coeff = 1.0/sqrt(2.0);
  }

  void calcul_dct(void)
  {    
    for(i2 = 0; i2 < 8; i2++)
    {
      for(i1 = 0; i1 < 8; i1++)
      {
        dct_res[i2][i1] = 0.0;

        for(y = 0; y < 8; y++)
          for(x = 0; x < 8; x++)
            dct_res[i2][i1] += dct_coeff[i1][x] * dct_coeff[i2][y]
              * (float)val[y][x];
        dct_res[i2][i1] /= 16.0;
      }
    }
  } /* fin de calcul_dct() */

  uint8 calcul_pix(float px, float py)
  { uint8 res;
  
    float temp;
    float dct_coeff_hor[8],dct_coeff_ver[8];

    for(y=0;y<8;y++)
    {
      f2=(double)y*f1;
      if(y==0) {
        dct_coeff_hor[y]=dc_coeff;
      } else {
        tmp=cos((double)(2.0*(4.0-px)+1.0)*f2);
        dct_coeff_hor[y]=(float)tmp;
      }
      if(y==0) {
        dct_coeff_ver[y]=dc_coeff;
      } else {
        tmp=cos((double)(2.0*(4.0-py)+1.0)*f2);
        dct_coeff_ver[y]=(float)tmp;
      }
    }
    
    temp = 0.0;
    for(y=0;y<8;y++)
      for(x=0;x<8;x++)
        temp += dct_coeff_hor[x] * dct_coeff_ver[y] * dct_res[y][x];

    if(temp > 255.0) res = 255;
    else if(temp < 0.0) res = 0;
    else res = (uint8)temp;
    return(res);
  }
  
  /* corp de la procedure */
  init_dct();
  
  width_scale  = (float)psrc->width  / (float)width;
  height_scale = (float)psrc->height / (float)height;

  nb_pix = 0;
  percent_done = 0.0;

  last_rx = last_ry = -1.0;
  
  for(vy = 0; vy < height; vy++)
  {
    for(vx = 0; vx < width; vx++)
    {
      rx = vx * width_scale;
      ry = vy * height_scale;

      x_dist = rx - ((float)((int32)rx));
      y_dist = ry - ((float)((int32)ry));
      
      if((((int32)last_rx) != ((int32)rx))
        || (((int32)last_ry) != ((int32)ry)))
        do_dct = TRUE;
      else
        do_dct = FALSE;
      last_rx = rx;
      last_ry = ry;
      
      /* pour le rouge */
      for(y=0;y<8;y++)
        for(x=0;x<8;x++)
          val[y][x] = COL_RED(get_pix(psrc,((int32)rx)-x+4,((int32)ry)-y+4));
      calcul_dct();
      SET_COL_RED(vcol,calcul_pix(x_dist, y_dist));
      /* pour le vert */
      for(y=0;y<8;y++)
        for(x=0;x<8;x++)
          val[y][x] = COL_GREEN(get_pix(psrc,((int32)rx)-x+4,((int32)ry)-y+4));
      calcul_dct();
      SET_COL_GREEN(vcol,calcul_pix(x_dist, y_dist));
      /* pour le bleu */
      for(y=0;y<8;y++)
        for(x=0;x<8;x++)
          val[y][x] = COL_BLUE(get_pix(psrc,((int32)rx)-x+4,((int32)ry)-y+4));
      calcul_dct();
      SET_COL_BLUE(vcol,calcul_pix(x_dist, y_dist));
      /* on place le pixel calcule */
      put_pix_alpha_replace(pdest, vx, vy, vcol);
      
      nb_pix++;
      percent_tmp = (float)nb_pix / (float)(width*height);
      if(percent_tmp - percent_done >= 0.01)
      {
        percent_done = (float)(((uint32)(percent_tmp*100.0))/100.0);
        fprintf(stdout,"\rpercent done = %3.0f%c",percent_done*100.0,'%');
        fflush(stdout);
      }
    }
  }
  fprintf(stdout,"\n");
}
/***************************************************/


/***************************************************/
/* Redimentionne l'image psrc et place le resultat */
/* a la postion (0,0) dans pdest. On determine les */
/* points par interpolation avec une BSpline de    */
/* Catmull-Rom cubique.                            */
void image_resize_catmull_rom(image *pdest, image *psrc, int32 width, int32 height)
{
  /* definie la matrice des BSplines de Catmull-Rom */
  float mat[16] = {-1.0,  3.0, -3.0,  1.0,
                    2.0, -5.0,  4.0, -1.0,
                   -1.0,  0.0,  1.0,  0.0,
                    0.0,  2.0,  0.0,  0.0};

  /* tableau des points de contrainte */
  float dot[12];
  
  float x,y,z;
  pix   vcol = BLACK;
  pix   vcol1,vcol2,vcol3,vcol4;
  int   vy,vx;
  float rx = 0.0,ry;
  float width_scale, height_scale;
  image *img_width;

  /* calcul la valeur du point a la position t */
  void calcul_dot(float t)
  { float t2,t3;
    float xt,yt,zt,wt;

    /* calcul t au carre */
    t2 = t*t;
    /* calcul t au cube */
    t3 = t2*t;

    /* on multiplie le vecteur (t3,t2,t,1) par la matrice mat */
    xt = t3*mat[0] + t2*mat[4] + t*mat[ 8] + mat[12];
    yt = t3*mat[1] + t2*mat[5] + t*mat[ 9] + mat[13];
    zt = t3*mat[2] + t2*mat[6] + t*mat[10] + mat[14];
    wt = t3*mat[3] + t2*mat[7] + t*mat[11] + mat[15];
    /* on multiplie le resultat par la matrice des contraintes */
    x = xt*dot[0] + yt*dot[3] + zt*dot[6] + wt*dot[9];
    y = xt*dot[1] + yt*dot[4] + zt*dot[7] + wt*dot[10];
    z = xt*dot[2] + yt*dot[5] + zt*dot[8] + wt*dot[11];
  }

  width_scale  = (float)psrc->width  / (float)width;
  height_scale = (float)psrc->height / (float)height;

  img_width = image_new(width,psrc->height);

  /* pour le moment, on redimentionne seulement en x */
  for(vy = 0;vy < psrc->height; vy++)
  {
    for(vx = 0;vx < width; vx++)
    {

      rx = vx * width_scale;
      ry = vy * height_scale;

      vcol1 = get_pix(psrc, ((int32)rx)-1, vy);
      vcol2 = get_pix(psrc, ((int32)rx)  , vy);
      vcol3 = get_pix(psrc, ((int32)rx)+1, vy);
      vcol4 = get_pix(psrc, ((int32)rx)+2, vy);

      /* on fait le calcul pour le rouge */
      dot[0] = 1.0; dot[ 1] = (float)COL_RED(vcol1); dot[ 2] = 1.0;
      dot[3] = 2.0; dot[ 4] = (float)COL_RED(vcol2); dot[ 5] = 1.0;
      dot[6] = 3.0; dot[ 7] = (float)COL_RED(vcol3); dot[ 8] = 1.0;
      dot[9] = 4.0; dot[10] = (float)COL_RED(vcol4); dot[11] = 1.0;

      /* calcul le point */
      calcul_dot(rx-((float)((int32)rx)));

      y /= 2.0;
      y = MIN(y,255);
      y = MAX(y,0);
      SET_COL_RED(vcol,(uint8)y);

      /* on fait le calcul pour le vert */
      dot[0] = 1.0; dot[ 1] = (float)COL_GREEN(vcol1); dot[ 2] = 1.0;
      dot[3] = 2.0; dot[ 4] = (float)COL_GREEN(vcol2); dot[ 5] = 1.0;
      dot[6] = 3.0; dot[ 7] = (float)COL_GREEN(vcol3); dot[ 8] = 1.0;
      dot[9] = 4.0; dot[10] = (float)COL_GREEN(vcol4); dot[11] = 1.0;

      /* calcul le point */
      calcul_dot(rx-((float)((int32)rx)));

      y /= 2.0;
      y = MIN(y,255);
      y = MAX(y,0);
      SET_COL_GREEN(vcol,(uint8)y);
      
      /* on fait le calcul pour le bleu */
      dot[0] = 1.0; dot[ 1] = (float)COL_BLUE(vcol1); dot[ 2] = 1.0;
      dot[3] = 2.0; dot[ 4] = (float)COL_BLUE(vcol2); dot[ 5] = 1.0;
      dot[6] = 3.0; dot[ 7] = (float)COL_BLUE(vcol3); dot[ 8] = 1.0;
      dot[9] = 4.0; dot[10] = (float)COL_BLUE(vcol4); dot[11] = 1.0;

      /* calcul le point */
      calcul_dot(rx-((float)((int32)rx)));

      y /= 2.0;
      y = MIN(y,255);
      y = MAX(y,0);
      SET_COL_BLUE(vcol,(uint8)y);

      SET_COL_ALPHA(vcol,0xFF);
      put_pix_alpha_replace(img_width, vx, vy, vcol);
    }
  }

  /* on redimentionne seulement en y */
  for(vx = 0;vx < width; vx++)
  {
    for(vy = 0;vy < height; vy++)
    {
      ry = vy * height_scale;

      vcol1 = get_pix(img_width, vx, ((int32)ry)-1);
      vcol2 = get_pix(img_width, vx, ((int32)ry)  );
      vcol3 = get_pix(img_width, vx, ((int32)ry)+1);
      vcol4 = get_pix(img_width, vx, ((int32)ry)+2);

      /* on fait le calcul pour le rouge */
      dot[0] = 1.0; dot[ 1] = (float)COL_RED(vcol1); dot[ 2] = 1.0;
      dot[3] = 2.0; dot[ 4] = (float)COL_RED(vcol2); dot[ 5] = 1.0;
      dot[6] = 3.0; dot[ 7] = (float)COL_RED(vcol3); dot[ 8] = 1.0;
      dot[9] = 4.0; dot[10] = (float)COL_RED(vcol4); dot[11] = 1.0;

      /* calcul le point */
      calcul_dot(ry-((float)((int32)ry)));

      y /= 2.0;
      y = MIN(y,255);
      y = MAX(y,0);
      SET_COL_RED(vcol,(uint8)y);

      /* on fait le calcul pour le vert */
      dot[0] = 1.0; dot[ 1] = (float)COL_GREEN(vcol1); dot[ 2] = 1.0;
      dot[3] = 2.0; dot[ 4] = (float)COL_GREEN(vcol2); dot[ 5] = 1.0;
      dot[6] = 3.0; dot[ 7] = (float)COL_GREEN(vcol3); dot[ 8] = 1.0;
      dot[9] = 4.0; dot[10] = (float)COL_GREEN(vcol4); dot[11] = 1.0;

      /* calcul le point */
      calcul_dot(rx-((float)((int32)rx)));

      y /= 2.0;
      y = MIN(y,255);
      y = MAX(y,0);
      SET_COL_GREEN(vcol,(uint8)y);
      
      /* on fait le calcul pour le bleu */
      dot[0] = 1.0; dot[ 1] = (float)COL_BLUE(vcol1); dot[ 2] = 1.0;
      dot[3] = 2.0; dot[ 4] = (float)COL_BLUE(vcol2); dot[ 5] = 1.0;
      dot[6] = 3.0; dot[ 7] = (float)COL_BLUE(vcol3); dot[ 8] = 1.0;
      dot[9] = 4.0; dot[10] = (float)COL_BLUE(vcol4); dot[11] = 1.0;

      /* calcul le point */
      calcul_dot(rx-((float)((int32)rx)));

      y /= 2.0;
      y = MIN(y,255);
      y = MAX(y,0);
      SET_COL_BLUE(vcol,(uint8)y);

      SET_COL_ALPHA(vcol,0xFF);
      put_pix_alpha_replace(pdest, vx, vy, vcol);
    }
  }
  image_free(img_width);
}
/***************************************************/

/************************************************************/
/* Redimentionne l'image psrc et place le resultat          */
/* dans pdest. pdest doit �tre 2 fois plus grande que psrc. */
void image_resize_tv(image *pdest, image *psrc)
{ pix vcol,vcol1,vcol2;
  int32 vx,vy;

  /* si les pr�conditions ne sont pas remplies, on quitte */
  if((pdest == NULL) || (psrc == NULL) ||
     (pdest->width != psrc->width*2) || (pdest->height != psrc->height*2))
    return;

  for(vy = 0;vy < psrc->height; vy++)
  {
    for(vx = 0;vx < psrc->width; vx++)
    {
      vcol  = get_pix(psrc, vx, vy);
      vcol2 = get_pix(psrc, vx+1, vy);

      put_pix_alpha_replace(pdest, (vx<<1), vy<<1, vcol);
      vcol = COL(
        (uint8)((((float)COL_RED(vcol))*0.5)+(((float)COL_RED(vcol2))*0.25)),
        (uint8)((((float)COL_GREEN(vcol))*0.5)+(((float)COL_GREEN(vcol2))*0.25)),
        (uint8)((((float)COL_BLUE(vcol))*0.5)+(((float)COL_BLUE(vcol2))*0.25)));
      put_pix_alpha_replace(pdest, (vx<<1)+1, vy<<1, vcol);
    }
  }

  for(vy = 0;vy < psrc->height; vy++)
  {
    for(vx = 0;vx < psrc->width; vx++)
    {
      vcol1 = get_pix(psrc, vx, vy);
      vcol2 = get_pix(psrc, vx, vy+1);

      vcol = COL(
        (uint8)((((float)COL_RED(vcol1))*0.5)+(((float)COL_RED(vcol2))*0.25)),
        (uint8)((((float)COL_GREEN(vcol1))*0.5)+(((float)COL_GREEN(vcol2))*0.25)),
        (uint8)((((float)COL_BLUE(vcol1))*0.5)+(((float)COL_BLUE(vcol2))*0.25)));

      put_pix_alpha_replace(pdest, (vx<<1), (vy<<1)+1, vcol);
      put_pix_alpha_replace(pdest, (vx<<1)+1, (vy<<1)+1, vcol);
    }
  }
}
/************************************************************/

/************************************************************/
/* Redimentionne l'image psrc et place le resultat          */
/* dans pdest. pdest doit �tre 2 fois plus grande que psrc. */
void image_resize_tv2(image *pdest, image *psrc)
{ pix vcol,vcol1,vcol2,vcol3,vcol4;
  int32 vx,vy;

  void redim_col(float dim)
  {
    vcol = COL(
      (uint8)MIN(255,MAX(0,(((float)COL_RED(vcol))*dim))),
      (uint8)MIN(255,MAX(0,(((float)COL_GREEN(vcol))*dim))),
      (uint8)MIN(255,MAX(0,(((float)COL_BLUE(vcol))*dim))));
  }

  void redim_lum(float dim_y,float dim_cr,float dim_cb)
  { float cr,cb,y;
    float r,g,b;
  
    y = 0.299*(float)COL_RED(vcol)+
        0.587*(float)COL_GREEN(vcol)+
        0.114*(float)COL_BLUE(vcol);
    cr = (float)COL_RED(vcol)  - y;
    cb = (float)COL_BLUE(vcol) - y;

/*    y = (1.0+cos(M_PI+((y/255.0)*M_PI)))*128.0;*/

    y *= dim_y;
    cr *= dim_cr;
    cb *= dim_cb;

    r = cr + y; 
    b = cb + y; 
    g = y*1.7 - r*0.509 - b*0.194;
    
    r = MIN(255.0,MAX(0,r));
    g = MIN(255.0,MAX(0,g));
    b = MIN(255.0,MAX(0,b));
    
    vcol = COL((uint8)r,(uint8)g,(uint8)b);
  }

  /* si les pr�conditions ne sont pas remplies, on quitte */
  if((pdest == NULL) || (psrc == NULL) ||
     (pdest->width != psrc->width*2) || (pdest->height != psrc->height*2))
    return;

  for(vy = 0;vy < psrc->height; vy++)
  {
    for(vx = 0;vx < psrc->width; vx++)
    {
      vcol1 = get_pix(psrc,   vx,   vy);
      vcol2 = get_pix(psrc, vx+1,   vy);
      vcol3 = get_pix(psrc,   vx, vy+1);
      vcol4 = get_pix(psrc, vx+1, vy+1);
      
      vcol = vcol1;
      redim_lum(1.5,1.7,1.7);
      put_pix_alpha_replace(pdest, (vx<<1), (vy<<1), vcol);
      vcol = COL(
        (uint8)((((float)COL_RED(vcol1))*0.3)+(((float)COL_RED(vcol2))*0.6)),
        (uint8)((((float)COL_GREEN(vcol1))*0.3)+(((float)COL_GREEN(vcol2))*0.6)),
        (uint8)((((float)COL_BLUE(vcol1))*0.3)+(((float)COL_BLUE(vcol2))*0.6)));
/*      redim_col(0.95);*/
      redim_lum(1.3,1.7,1.7);
      put_pix_alpha_replace(pdest, (vx<<1)+1, (vy<<1), vcol);
      vcol = COL(
        (uint8)((((float)COL_RED(vcol1))*0.3)+(((float)COL_RED(vcol3))*0.6)),
        (uint8)((((float)COL_GREEN(vcol1))*0.3)+(((float)COL_GREEN(vcol3))*0.6)),
        (uint8)((((float)COL_BLUE(vcol1))*0.3)+(((float)COL_BLUE(vcol3))*0.6)));
/*      redim_col(0.8);*/
      redim_lum(1.0,1.7,1.7);
      put_pix_alpha_replace(pdest, (vx<<1), (vy<<1)+1, vcol);
      vcol = COL(
        (uint8)((((float)COL_RED(vcol1))*0.11)+(((float)COL_RED(vcol2))*0.2)+(((float)COL_RED(vcol3))*0.2)+(((float)COL_RED(vcol4))*0.44)),
        (uint8)((((float)COL_GREEN(vcol1))*0.11)+(((float)COL_GREEN(vcol2))*0.2)+(((float)COL_GREEN(vcol3))*0.2)+(((float)COL_GREEN(vcol4))*0.44)),
        (uint8)((((float)COL_BLUE(vcol1))*0.11)+(((float)COL_BLUE(vcol2))*0.2)+(((float)COL_BLUE(vcol3))*0.2)+(((float)COL_BLUE(vcol4))*0.44)));
      redim_col(0.7);
      put_pix_alpha_replace(pdest, (vx<<1)+1, (vy<<1)+1, vcol);
    }
  }
}
/************************************************************/

/************************************************************/
/* Redimentionne l'image psrc et place le resultat          */
/* dans pdest. pdest doit �tre 2 fois plus grande que psrc. */
void image_resize_most(image *pdest, image *psrc)
{ pix vcol1,vcol2,vcol3,vcol4,vcolm;
  int32 vx,vy;

  pix find_most_middle(void)
  { float y[4];
    int   pos[4];
    int   min_pos, i1, i2;
    float swap_float;
    int   swap_int;

    y[0] = (0.299*(float)COL_RED(vcol1)+
           0.587*(float)COL_GREEN(vcol1)+
           0.114*(float)COL_BLUE(vcol1))*1.2;
    y[1] = 0.299*(float)COL_RED(vcol2)+
           0.587*(float)COL_GREEN(vcol2)+
           0.114*(float)COL_BLUE(vcol2);
    y[2] = 0.299*(float)COL_RED(vcol3)+
           0.587*(float)COL_GREEN(vcol3)+
           0.114*(float)COL_BLUE(vcol3);
    y[3] = 0.299*(float)COL_RED(vcol4)+
           0.587*(float)COL_GREEN(vcol4)+
           0.114*(float)COL_BLUE(vcol4);
    pos[0] = 0; pos[1] = 1; pos[2] = 2; pos[3] = 3;

    for(i1=0;i1<4-1;i1++)
    {
      min_pos = i1;
      for(i2=i1;i2<4;i2++)
      {
        if(y[i2] < y[min_pos]) min_pos = i2;
      }
      swap_float = y[i1];
      swap_int   = pos[i1];
      y[i1]      = y[min_pos];
      pos[i1]    = pos[min_pos];
      y[min_pos] = swap_float;
      pos[min_pos] = swap_int;
    }
    switch(pos[1])
    {
      case 0 : return(vcol1);
      case 1 : return(vcol2);
      case 2 : return(vcol3);
      case 3 : return(vcol4);
    }
    return(BLACK);
  }

  pix find_most_upper(void)
  { float y[3];
    int   pos[3];
    int   min_pos, i1, i2;
    float swap_float;
    int   swap_int;

    y[0] = 0.299*(float)COL_RED(vcol1)+
           0.587*(float)COL_GREEN(vcol1)+
           0.114*(float)COL_BLUE(vcol1);
    y[1] = 0.299*(float)COL_RED(vcol2)+
           0.587*(float)COL_GREEN(vcol2)+
           0.114*(float)COL_BLUE(vcol2);
    y[2] = (0.299*(float)COL_RED(vcolm)+
           0.587*(float)COL_GREEN(vcolm)+
           0.114*(float)COL_BLUE(vcolm))*1.2;
    pos[0] = 0; pos[1] = 1; pos[2] = 2;

    for(i1=0;i1<3-1;i1++)
    {
      min_pos = i1;
      for(i2=i1;i2<3;i2++)
      {
        if(y[i2] < y[min_pos]) min_pos = i2;
      }
      swap_float = y[i1];
      swap_int   = pos[i1];
      y[i1]      = y[min_pos];
      pos[i1]    = pos[min_pos];
      y[min_pos] = swap_float;
      pos[min_pos] = swap_int;
    }
    switch(pos[1])
    {
      case 0 : return(vcol1);
      case 1 : return(vcol2);
      case 2 : return(vcolm);
    }
    return(vcolm);
  }

  pix find_most_left(void)
  { float y[3];
    int   pos[3];
    int   min_pos, i1, i2;
    float swap_float;
    int   swap_int;

    y[0] = 0.299*(float)COL_RED(vcol1)+
           0.587*(float)COL_GREEN(vcol1)+
           0.114*(float)COL_BLUE(vcol1);
    y[1] = 0.299*(float)COL_RED(vcol3)+
           0.587*(float)COL_GREEN(vcol3)+
           0.114*(float)COL_BLUE(vcol3);
    y[2] = (0.299*(float)COL_RED(vcolm)+
           0.587*(float)COL_GREEN(vcolm)+
           0.114*(float)COL_BLUE(vcolm))*1.2;
    pos[0] = 0; pos[1] = 1; pos[2] = 2;

    for(i1=0;i1<3-1;i1++)
    {
      min_pos = i1;
      for(i2=i1;i2<3;i2++)
      {
        if(y[i2] < y[min_pos]) min_pos = i2;
      }
      swap_float = y[i1];
      swap_int   = pos[i1];
      y[i1]      = y[min_pos];
      pos[i1]    = pos[min_pos];
      y[min_pos] = swap_float;
      pos[min_pos] = swap_int;
    }
    switch(pos[1])
    {
      case 0 : return(vcol1);
      case 1 : return(vcol3);
      case 2 : return(vcolm);
    }
    return(vcolm);
  }

  /* si les pr�conditions ne sont pas remplies, on quitte */
  if((pdest == NULL) || (psrc == NULL) ||
     (pdest->width != psrc->width*2) || (pdest->height != psrc->height*2))
    return;

  for(vy = 0;vy < psrc->height; vy++)
  {
    for(vx = 0;vx < psrc->width; vx++)
    {
      vcol1 = get_pix(psrc,   vx,   vy);
      vcol2 = get_pix(psrc, vx+1,   vy);
      vcol3 = get_pix(psrc,   vx, vy+1);
      vcol4 = get_pix(psrc, vx+1, vy+1);
      
      put_pix_alpha_replace(pdest, (vx<<1), (vy<<1), vcol1);
      vcolm = find_most_middle();
      put_pix_alpha_replace(pdest, (vx<<1)+1, (vy<<1)+1, vcolm);
      put_pix_alpha_replace(pdest, (vx<<1)+1, (vy<<1), find_most_upper());
      put_pix_alpha_replace(pdest, (vx<<1), (vy<<1)+1, find_most_left());
    }
  }
}
/************************************************************/


/***************************************************/
/* Redimentionne l'image psrc et place le resultat */
/* a la postion (0,0) dans pdest en fusionnant les */
/* points. L'image r�sultat doit �tre plus petite  */
/* que l'image d'origine. L'algo est plus simple,  */
/* moins exact mais g�n�re moins de flou.          */
void image_downsize_rought(image *pdest,
                    image *psrc,
                    int32 width,
                    int32 height)
{
  int32 vx,vy;
  pix   vcol;
  float rx,ry,rx_next,ry_next;
  float width_scale, height_scale;
  float red, green, blue, alpha;
  float factor;
  int32   i,j;

  if((pdest == NULL) || (psrc == NULL)) {
    return;
  }
  if ((psrc->width < width) || (psrc->height < height)) {
      image_resize_bilinear(pdest, psrc, width, height);
      return;
  }
  width_scale  = (float)psrc->width  / (float)width;
  height_scale = (float)psrc->height / (float)height;

  for(vy = 0;vy < height; vy++) {
    for(vx = 0;vx < width; vx++) {

      rx = vx * width_scale;
      ry = vy * height_scale;

      red = green = blue = alpha = 0.0;

      rx_next = rx + width_scale;
      ry_next = ry + width_scale;
      factor = 0;

      for( j = (int32)rx; (float)j < rx_next; j++) {
        for( i = (int32)ry; (float)i < ry_next; i++) {
          factor += 1;
          vcol = get_pix(psrc, j, i);

          red   += (float)COL_RED  (vcol);
          green += (float)COL_GREEN(vcol);
          blue  += (float)COL_BLUE (vcol);
          alpha += (float)COL_ALPHA(vcol);
        }
      }

      red   /= factor;
      green /= factor;
      blue  /= factor;
      alpha /= factor;

      /* on sature les valeurs */
      red   = (red   > 255.0)? 255.0 : ((red   < 0.0)? 0.0:red  );
      green = (green > 255.0)? 255.0 : ((green < 0.0)? 0.0:green);
      blue  = (blue  > 255.0)? 255.0 : ((blue  < 0.0)? 0.0:blue );
      alpha = (alpha > 255.0)? 255.0 : ((alpha < 0.0)? 0.0:alpha);

      put_pix_alpha_replace(pdest, vx, vy,
        COL_FULL((uint8)red,(uint8)green,(uint8)blue,(uint8)alpha));
    }
  }
}
/************************************************************/
