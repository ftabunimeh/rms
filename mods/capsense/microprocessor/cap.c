/*
Wireless Capaciflector Microprocessor Code
Author:        Edward Cheung
Modified by:   Wissam Geadaa
Date modified: 11/11/2004
Removed battery and temperature readings
*/

#include <18F258.h>
#DEVICE ADC=10

// GENERAL DEFS
#fuses HS,NOWDT,NOPROTECT,PUT,NOLVP
#use delay(clock=40 000 000)
#USE FAST_IO(A)
#USE FAST_IO(B)
#USE FAST_IO(C)
#BYTE PORTA = 0xF80
#BYTE PORTB = 0XF81
#BYTE PORTC = 0XF82
#BYTE DIRA  = 0XF92
#BYTE DIRB  = 0XF93
#BYTE DIRC  = 0XF94
#use rs232(baud=9600, bits=8, parity = N, xmit=PIN_C6, rcv=PIN_C7)

// Oscillator outputs
#define SEN0 PIN_B0
#define SEN1 PIN_B1
#define SEN2 PIN_B2
#define SEN3 PIN_B3
#define SEN4 PIN_B4
#define SEN5 PIN_B5
#define SEN6 PIN_B6
#define SEN7 PIN_B7

// Enables for oscillators.  Set to Hi-Z to enable oscillator, ground to disable
#define SEN0_ENB PIN_A4
#define SEN1_ENB PIN_A5
#define SEN2_ENB PIN_C0
#define SEN3_ENB PIN_C1
#define SEN4_ENB PIN_C2
#define SEN5_ENB PIN_C3
#define SEN6_ENB PIN_C4
#define SEN7_ENB PIN_C5

// global vars
#define VALS 18
int sen_latch;  // status of oscillators
unsigned long sen0_counter;
unsigned long sen1_counter;
unsigned long sen2_counter;
unsigned long sen3_counter;
unsigned long sen4_counter;
unsigned long sen5_counter;
unsigned long sen6_counter;
unsigned long sen7_counter;
short timer_flag;

void init()
{
  // set data direction
  DIRA = 0xFF;  // input
  DIRB = 0xFF;  // input
  DIRC = 0xBF;  // only serial TX is output
  setup_port_a(RA0_RA1_RA3_ANALOG);  // (ALL_ANALOG);
  setup_adc( ADC_CLOCK_INTERNAL );
  setup_timer_0(RTCC_DIV_128);

  // initialize port outputs
  PORTA = 0;
  PORTC = 0;
}

void set_timer(long value)
{
  // 'prescaler' is in 'setup_timer_0' in 'init()'
  //     'value' is in 'set_timer' call
  // time delay (sec) = value * prescaler * 4 / clock_frequency
  timer_flag = TRUE;
  set_timer0(-value);
}

#INT_TIMER0
void fire_timer(void)
{
  // close counter's gate
  timer_flag = FALSE;
}

#define HALF_SECOND 0x9896  // calculated for divider of 128
void poll()
{
  // with PIC18 @40Mhz can count frequency up to 125kHz
  int diff,levels;

  // set timer for closing of gate
  set_timer(HALF_SECOND/5);   // was 0x7A33);

  while (timer_flag)
  {
    // latch inputs
    levels = PORTB;
    // get difference between latched and current levels
    diff = sen_latch ^ levels;
    // latch current levels
    sen_latch = levels;
    if (bit_test(diff,0))
      sen0_counter++;
    if (bit_test(diff,1))
      sen1_counter++;
    if (bit_test(diff,2))
      sen2_counter++;
    if (bit_test(diff,3))
      sen3_counter++;
    if (bit_test(diff,4))
      sen4_counter++;
    if (bit_test(diff,5))
      sen5_counter++;
    if (bit_test(diff,6))
      sen6_counter++;
    if (bit_test(diff,7))
      sen7_counter++;
  }
}

void send_data()
{
  // normal binary data
  char loop;
  char sum = 0;
  unsigned char buffer[VALS];
  buffer[0]  = 'U';  // framing bytes
  buffer[1]  = 'U';
  buffer[2]  = (sen0_counter>>8);
  buffer[3]  =  sen0_counter;
  buffer[4]  = (sen1_counter>>8);
  buffer[5]  =  sen1_counter;
  buffer[6]  = (sen2_counter>>8);
  buffer[7]  =  sen2_counter;
  buffer[8]  = (sen3_counter>>8);
  buffer[9]  =  sen3_counter;
  buffer[10] = (sen4_counter>>8);
  buffer[11] =  sen4_counter;
  buffer[12] = (sen5_counter>>8);
  buffer[13] =  sen5_counter;
  buffer[14] = (sen6_counter>>8);
  buffer[15] =  sen6_counter;
  buffer[16] = (sen7_counter>>8);
  buffer[17] =  sen7_counter;

  // calc checksum and send bytes to host
  for (loop=0;loop<VALS;loop++)
  {
    sum += buffer[loop];
    putc(buffer[loop]);
  }

  // send checksum
  putc(sum);
}

void main()
{
  enable_interrupts(GLOBAL);
  enable_interrupts(INT_TIMER0);

  init();

  #USE STANDARD_IO(A)    // allow CCS to set TRIS from here
  #USE STANDARD_IO(C)
  output_low(SEN0_ENB);  // disable this oscillator
  output_low(SEN1_ENB);  // disable this oscillator
  output_low(SEN2_ENB);  // disable this oscillator
  output_low(SEN3_ENB);  // disable this oscillator
  output_low(SEN4_ENB);  // disable this oscillator
  output_low(SEN5_ENB);  // disable this oscillator
  output_low(SEN6_ENB);  // disable this oscillator
  output_low(SEN7_ENB);  // disable this oscillator

  while (1)
  {
    // zero counters
    sen0_counter = 0;
    sen1_counter = 0;
    sen2_counter = 0;
    sen3_counter = 0;
    sen4_counter = 0;
    sen5_counter = 0;
    sen6_counter = 0;
    sen7_counter = 0;

    // count transitions
    output_float(SEN0_ENB);  // enable  oscillator 0
    output_float(SEN4_ENB);  // enable  oscillator 4
    poll();
    output_low(SEN0_ENB);    // disable oscillator 0
    output_low(SEN4_ENB);    // disable oscillator 4

    // count transitions
    output_float(SEN1_ENB);  // enable  oscillator 1
    output_float(SEN5_ENB);  // enable  oscillator 5
    poll();
    output_low(SEN1_ENB);    // disable oscillator 1
    output_low(SEN5_ENB);    // disable oscillator 5

    // count transitions
    output_float(SEN2_ENB);  // enable  oscillator 2
    output_float(SEN6_ENB);  // enable  oscillator 6
    poll();
    output_low(SEN2_ENB);    // disable oscillator 2
    output_low(SEN6_ENB);    // disable oscillator 6

    // count transitions
    output_float(SEN3_ENB);  // enable  oscillator 3
    output_float(SEN7_ENB);  // enable  oscillator 7
    poll();
    output_low(SEN3_ENB);    // disable oscillator 3
    output_low(SEN7_ENB);    // disable oscillator 7

    send_data();
  }
}
