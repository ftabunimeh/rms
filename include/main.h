/*******************************************************************************
Copyright (c) 2004, MSU ECE480 F04 Team01, Faisal Abu-Nimeh
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list
of conditions and the following disclaimer. Redistributions in binary form must 
reproduce the above copyright notice, this list of conditions and the following 
disclaimer in the documentation and/or other materials provided with the 
distribution. Neither the name of the Michigan State University nor the names 
of its contributors may be used to endorse or promote products derived from this 
software without specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR 
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>
#include <sys/select.h>
#include <time.h>
#include <signal.h>
#include <limits.h>
#include <errno.h>
#include <dlfcn.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "strlib.h" /* Custom String Library */

#define MAXBUF 256
#define MIDBUF 128
#define MINBUF 64

#define MAXNETFD 128 /* TODO: make rcvsock dynamic */ 

#define CONFDEL ':' /* config file delimiter char */

#define VERSION "RMS v0.1"
#define CONF "../config/RMS.conf" /* relative path to RMS bin */
#define CMDS "../config/CMDS.conf"
#define LOG "../log/RMS.log"
#define RPID "../log/RMS.pid"

/* Modules List */
typedef struct mod
{
   char *modname;
   void *handle;
   
	struct mod *next;
	struct mod *prev;
}MODULE;

/* Commands List */
typedef struct command
{
	int cmd; /* command numeric */
	/* char *cmdname; command name */
	int (*func)(); /* pointer to command function */
	char *module; /* module associated with command if any */
	
	struct command *next;
	struct command *prev;
}CMD;

/* Server Net Socket */
typedef struct robonet
{
   int lsnsock; 
   int fdmax;
   int users; /* number of users currently connected */
   fd_set master; 
   fd_set read_fds; 
}RONET;

/* Remote Client Net Socket */
typedef struct remoteclient
{
   int fd;
   int flags;
   char *address; /* IP address in xxx.xxx.xxx.xxx format -- TODO IPv6 */
   int auth; /* authorized ? */
   time_t lastrecv; /* Last Command received */
   struct bot *robotstk; /* Give remote client access to robot struct */
   
   struct remoteclient *next;
   struct remoteclient *prev;
}RCLIENT;

/* Robot Structure */
struct bot
{
   char *login;
   char *pass;
   int port; /* RMS listening port */
   int timeout; /* connection timeout in seconds */
   int maxcon; /* maximum number of connections allowed concurrently */
   
   char *arm_com;
   char *motor_com;
   char *sense_com;
   char *teles_com;
   
   time_t startupt; /* time since RMS first ran */
};
