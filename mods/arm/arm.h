/***************************************************************************************************
Name:  ArmModule.h
Description:  This is the header file to control the robotic arm.  It contains all of the defined constants, global variables, struct declarations, and function declarations.  Please refer to the individual comments for a detailed description of the purpose and functionality.
**************************************************************************************************/

#include <stdio.h>
#include <math.h>

#include <stdlib.h> /* needed for abs() */
#include <fcntl.h> /* needed for fcntl() */
#include <unistd.h> /* needed for close() write() */

#include "main.h"
#include "external.h"

#define ARMCMD 37

//only when NOT on SBC
//#define COMPRT "/dev/ttyS1"

//debug=1 then print debug statements, 0 no debug
#define DEBUG 1 


/* 
degrees     -- 00deg -- 90deg -- 180deg 
pulse width -- 1.0ms -- 1.5ms -- 2.0ms 
servo input -- 2000  -- 3000  -- 4000 
*/
#define EQCONST 1360/180.0 

/* offset is 3000 for center*/ 
#define EQOFFSET 3000.0 
#define REVERSE_SERVO -1

/*********this defines how many steps each arm movement should be broken up into, so to complete one motion to another position it will 	break it up into x steps to help the arm move in a safer manner   
	Set to 1 if you do not want any resolution!************************************/
//#define num_steps 1
int num_steps=1;
//Constants for the Robotic Arm

//These constants were taken from the sample from Lynxmotion
#define BoxLength 10.0//the lenght of the box
#define baseheight 7.50
#define Link1 15.5
#define Link2 15.5
#define Link3 (8+BoxLength)
#define deg (180/3.1415926535) //convert from degrees to radians
#define rad (3.1415926535/180) //convert from degrees to radians



//maximum/minimum angles for the servos
#define servo_max_range0 100
#define servo_min_range0 -100
#define servo_max_range1 65.0
#define servo_min_range1 -80.0
#define servo_max_range2 90.0
#define servo_min_range2 -75.0
#define servo_max_range3 90.0
#define servo_min_range3 -90
#define servo_max_range4 45.0
#define servo_min_range4 -90.0 
#define servo_max_range5 90.0 
#define servo_min_range5 -90.0

/*servo offsets due to the slight error of the mounting, 8.5 7.25 2*/
#define servo_offset1 0
#define servo_offset2 -5
#define servo_offset3 0

struct ArmPos//Global struct that stores the coordinate position of the arm
{
	float Pos_x;
	float Pos_y;
	float Pos_z;
	float Pos_pitch;//in degrees
	float Pos_yaw;//in degrees
	float Pos_roll; //in degrees
};

struct Angles//Struct Stores the Angles of the Servos
{	
	
	float servo0;//Base Servo
	float servo1;//Shoulder Servo
	float servo2;//Elbow servo
	float servo3;//Pitch Servo
	float servo4;//Yaw Servo
	float servo5;//Roll Servo
};
struct Sensor//struct to define the capaciflector values for the autodocking procedure
{
	//from the perspective of the box
	float cap_0;//front top-left sensor
	float cap_1;//front top-right sensor
	float cap_2;//front bottom-left sensor
	float cap_3;//front bottom-right sensor
	float cap_4;//top sensor
	float cap_5;//right sensor
	float cap_6;//bottom sensor
	float cap_7;//left sensor
};

//Global structs that hold the current position/angles of the arm
struct ArmPos CurrentPos;
struct Angles CurrentAngles;
struct Sensor CurrentSensor;
int errorcode=1; //this is the errorcode/success code of the operation  0 is a failure(servorange), 1 is a success, 2 capaciflectors out of range
int Load_Unload=0; //this is used to determine if the if you are loading the module for the first time so that the arm is put in it's default position
/****need to change, for design day only****/
char *COMPRT; //used to define which serial port to use
RCLIENT * MyClient;//used as a global RCLIENT

//Define Functions Here

int InitializeMain(RCLIENT *, char *);
int NewPos(struct ArmPos *InputRef,struct Angles *AngleRef,struct ArmPos *ArmRef, int execute );//This Function Takes in a New Set of Coordinates and Makes the Arm Take the New Position
int ValidPos(float servo0, float servo1, float servo2, float servo3, float servo4, float servo5, float Xb, float Yb);//This Function calculates whether the new angles put the arm in a safe state
void SendOutput(struct Angles *AngleRef, struct ArmPos *ArmRef);//this function instructs the arm to go to the new position.
void InitAngles(struct Angles *,struct ArmPos *,int);//This Initializes the Angles for the Servos
int GetInput(struct ArmPos *); //This function Gets the new coordinates from the user and puts them into the struct by ref
int ParseInput(char *, struct ArmPos *,struct Angles *, struct ArmPos *); //This function takes in the String input from the GUI which defines the positions to be taken and parses it to create the true input
float NumVal(char * buf, int *pos); //function used by ParseInput
void IncPos(struct ArmPos *, struct ArmPos *,  float);//This function increments the box's position in the direction of it's orientation
int resolution(struct ArmPos *, struct Angles *, struct ArmPos *);  //This function divides the arms movement into x steps to go from current point to the next desired location
int autodock();
void GetSensorValues(struct Sensor * SensorInput);
void returnstring(int , struct ArmPos *); //returns the string to be passed back to the GUI indicating the current position and error/success code
